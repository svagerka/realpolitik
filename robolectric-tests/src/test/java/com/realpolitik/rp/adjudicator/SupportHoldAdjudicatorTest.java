package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.reader.ReadException;
import com.realpolitik.rp.reader.orders.MovementOrderParser;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.SeasonBuilder;
import com.realpolitik.test.AdjudicatorTest;
import com.realpolitik.test.RobolectricGradleTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.realpolitik.rp.common.Order.Resolution.CUT;
import static com.realpolitik.rp.common.Order.Resolution.DISLODGED;
import static com.realpolitik.rp.common.Order.Resolution.FAILS;
import static com.realpolitik.rp.common.Order.Resolution.SUCCESS;
import static com.realpolitik.rp.common.Order.Resolution.VOID;

@RunWith(RobolectricGradleTestRunner.class)
public class SupportHoldAdjudicatorTest extends AdjudicatorTest {

    public SupportHoldAdjudicatorTest() throws ReadException {
    }

    @Test
    public void testSupportAddsDefensiveStrength() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("One: A axb Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        addOrder("Two: A bxa Supports A bxb");
        season.resolve();
        assertOrderResolution("axa", FAILS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", SUCCESS);
        assertOrderResolution("bxa", SUCCESS);
    }

    @Test
    public void testMoveSupportDoesNotAddDefensiveStrength() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("One: A axb Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        addOrder("Two: A bxa Supports A bxb - axa");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);
        assertOrderResolution("bxa", VOID);
    }

    @Test
    public void testInsufficientSupportDoesNotPreventDislodge() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "cxa", 0)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("One: A axb Supports A axa - bxb");
        addOrder("One: A cxa Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        addOrder("Two: A bxa Supports A bxb");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);
        assertOrderResolution("bxa", SUCCESS);
        assertOrderResolution("cxa", SUCCESS);
    }

    @Test
    public void testMoveCutsSupport() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "cxa", 0)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("One: A axb Supports A axa - bxb");
        addOrder("One: A cxa - bxa");
        addOrder("Two: A bxb Hold");
        addOrder("Two: A bxa Supports A bxb");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);
        assertOrderResolution("bxa", CUT);
        assertOrderResolution("cxa", FAILS);
    }

    @Test
    public void testSupportOnUnreachableCoastAddsDefensiveStrength() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.FLEET, "dxc/wc", 0)
                .addUnit(Unit.Type.FLEET, "exc", 0)
                .addUnit(Unit.Type.ARMY, "dxb", 1)
                .addUnit(Unit.Type.ARMY, "cxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: F dxc(wc) Hold");
        addOrder("One: F exc Supports F dxc(wc)");
        addOrder("Two: A dxb - dxc");
        addOrder("Two: A cxb Supports A dxb - dxc");
        season.resolve();
        assertOrderResolution("dxc", SUCCESS);
        assertOrderResolution("exc", SUCCESS);
        assertOrderResolution("dxb", FAILS);
        assertOrderResolution("cxb", SUCCESS);
    }


}
