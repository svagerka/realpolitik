package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.season.SeasonBuilder;

public class RetreatAdjudicator extends Adjudicator {
    public RetreatAdjudicator(Variant variant, Season season) {
        super(variant, season);
    }

    @Override
    protected void perform(SeasonBuilder seasonBuilder) {
        seasonBuilder.addUnits(season.getAllUnits());
        seasonBuilder.setSupplyOwnership(season.getSupplyCenters());

        // count retreats per space
        int[] retreatingUnitCount = new int[variant.getSectors().size()];
        for (Dislodge dislodge : season.getAllRetreats()) {
            Order order = season.getOrder(dislodge.unit.location.sector);
            if (order.type == Order.Type.RETREAT) {
                ++retreatingUnitCount[order.targetLocation.sector.id];
            }
        }

        // adjudicate
        int[] remainingAdjustments = getRemainingAdjustments();
        for (Dislodge dislodge : season.getAllRetreats()) {
            Order order = season.getOrder(dislodge.unit.location.sector);
            switch (order.type) {
                case NO_MOVE_RECEIVED:
                    order.setResolution(Order.Resolution.DISBANDED);
                    ++remainingAdjustments[order.unit.country.id];
                    break;
                case RETREAT:
                    if (retreatingUnitCount[order.targetLocation.sector.id] > 1) {
                        order.setResolution(Order.Resolution.BOUNCE);
                        ++remainingAdjustments[order.unit.country.id];
                    } else {
                        seasonBuilder.addUnit(order.unit.type, order.targetLocation,
                                order.unit.country);
                    }
                    break;
                case DISBAND:
                    ++remainingAdjustments[order.unit.country.id];
                    break;
                default:
                    throw new IllegalStateException();
            }
        }


        // change SC ownership
        if (seasonBuilder.getSeasonType() == Phase.PhaseType.ADJUSTMENT) {
            for (Unit unit : seasonBuilder.getAllUnits()) {
                if (!unit.location.sector.isSupply()) {
                    continue;
                }
                Country previousOwner = season.getSupplyCenterOwner(unit.location.sector);
                Country newOwner = unit.country;
                if (previousOwner != newOwner) {
                    seasonBuilder.setSupplyOwnership(unit.location.sector, newOwner);
                    ++remainingAdjustments[newOwner.id];
                    if (previousOwner != null) {
                        --remainingAdjustments[previousOwner.id];
                    }
                }
            }
        }

        seasonBuilder.setAdjustmentCount(remainingAdjustments);
    }
}
