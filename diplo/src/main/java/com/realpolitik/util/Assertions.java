package com.realpolitik.util;

public class Assertions {
    public static <T> T notNull(T object) {
        if (object == null) {
            throw new NullPointerException();
        } else {
            return object;
        }
    }

    public static void checkArgument(boolean argument) {
        if (!argument) {
            throw new IllegalArgumentException();
        }
    }

    public static void checkState(boolean argument) {
        if (!argument) {
            throw new IllegalStateException();
        }
    }
}
