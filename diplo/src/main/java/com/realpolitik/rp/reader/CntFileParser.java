package com.realpolitik.rp.reader;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Variant;

import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;

class CntFileParser {
    static void parse(Variant variant, LineNumberReader reader) throws IOException, ParseException {
        int id = 0;
        try {
            reader.readLine(); // ignore version
            int countryCount = Integer.parseInt(reader.readLine().trim()); // read country count
            for (int i = 0; i < countryCount; ++i) {
                String line = reader.readLine();
                String[] tokens = line.split(" ");
                if (tokens.length != 5) {
                    throw new ParseException(String.format("Malformed country line %s", line),
                            reader.getLineNumber());
                }

                variant.addCountry(new Country(
                        id++, tokens[0], tokens[1], tokens[2].charAt(0),
                        Country.getColor(tokens[4])));
            }
        } catch (RuntimeException e) {
            ParseException ex = new ParseException("Failed to parse Cnt: " + e.getMessage(),
                    reader.getLineNumber());
            ex.initCause(e);
            throw ex;
        }
    }
}
