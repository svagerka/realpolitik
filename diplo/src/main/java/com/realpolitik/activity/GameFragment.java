package com.realpolitik.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.realpolitik.controller.UiHelper;
import com.realpolitik.database.DatabaseController;
import com.realpolitik.rp.common.Game;

abstract class GameFragment extends Fragment {

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        update();
    }

    public final void update() {
        if (getGame() != null) {
            performUpdate();
        }
    }

    protected abstract void performUpdate();

    Game getGame() {
        return ((GameActivity) getActivity()).getGame();
    }

    UiHelper getUiHelper() {
        return ((GameActivity) getActivity()).getUiHelper();
    }

    DatabaseController getDatabaseController() {
        return ((GameActivity) getActivity()).getDatabaseController();
    }

    void performFullUpdate() {
        ((GameActivity) getActivity()).performFullUpdate();
    }
}