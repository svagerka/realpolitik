package com.realpolitik.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.realpolitik.R;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.writer.OrderWriter;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Arrays;

public class OrdersFragment extends GameFragment {
    private TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.orders_fragment, container, false);
        textView = (TextView) view.findViewById(R.id.orders_text_view);
        return view;
    }

    @Override
    public void performUpdate() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream);

        // in hotseat, on last season, print only the current country
        boolean[] countriesToShare = null;
        if (getGame().gameInfo.mode == GameInfo.MODE_HOT_SEAT && getGame().isCurrentSeasonLast()) {
            countriesToShare = new boolean[getGame().variant.getCountries().size()];
            Arrays.fill(countriesToShare, false);
            if (getGame().gameInfo.hotSeatCountry != null) {
                countriesToShare[getGame().gameInfo.hotSeatCountry.id] = true;
            }
        }

        OrderWriter.writeOrders(writer, getGame().getCurrentSeason(), getGame().variant,
                countriesToShare, OrderWriter.FLAG_USE_HTML);
        writer.flush();
        textView.setText(Html.fromHtml(stream.toString()));
        writer.close();
    }
}
