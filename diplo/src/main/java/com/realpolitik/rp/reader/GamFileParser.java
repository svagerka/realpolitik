package com.realpolitik.rp.reader;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.season.SeasonBuilder;

import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

class GamFileParser {

    private static final Pattern COUNTRY_LIST_PATTERN = Pattern.compile(",?[ .]");
    private static final Pattern UNIT_LIST_PATTERN = Pattern.compile(",? ");

    static void parse(Variant variant, LineNumberReader reader) throws IOException, ParseException {
        try {
            variant.gameInfo = parseGameInfo(reader);
            variant.startingSeason = parseSeasonInfo(reader, variant, true);
        } catch (RuntimeException | ReadException e) {
            ParseException ex = new ParseException("Failed to parse Gam: " + e.getMessage(),
                    reader.getLineNumber());
            ex.initCause(e);
            throw ex;
        }
    }

    static GameInfo parseGameInfo(LineNumberReader reader) throws IOException {
        GameInfo game = new GameInfo();
        reader.readLine(); // ignore version
        game.gameName = reader.readLine().trim();
        game.variantName = reader.readLine().trim();
        return game;
    }

    static Season parseSeasonInfo(LineNumberReader reader, Variant variant,
                                  boolean setCentersAsHome) throws IOException, ParseException, ReadException {
        String line = reader.readLine();
        String[] tokens = line.split(" ");
        if (tokens.length != 2) {
            throw new ParseException(String.format("Malformed season %s", line),
                    reader.getLineNumber());
        }
        SeasonBuilder season = new SeasonBuilder(
                variant, Phase.fromString(tokens[0]), Integer.parseInt(tokens[1]));
        reader.readLine(); // ignore 0
        reader.readLine(); // ignore country count

        for (Country country : variant.getCountries()) {
            season.setAdjustmentCount(country, Integer.parseInt(reader.readLine()));
            parseCurrentCenters(reader, variant, season, country, setCentersAsHome);
            parseCurrentUnits(reader, variant, season, country);
        }

        parseRetreats(reader, variant, season);
        return season.build();
    }

    public static void parseRetreats(LineNumberReader reader, Variant variant,
                                     SeasonBuilder seasonBuilder) throws IOException, ParseException {
        int retreatCount = Integer.parseInt(reader.readLine());

        try {
            for (; retreatCount > 0; --retreatCount) {
                String[] tokens = reader.readLine().split(" ");
                Unit.Type type = TokenParser.parseUnitType(tokens[1]);
                Location location = TokenParser.parseLocation(variant, tokens[2],
                        type.getDefaultCoast());
                Country country = variant.getCountryByAdjective(tokens[0]);
                List<Location> locations = new ArrayList<>();
                for (int i = 3; i < tokens.length; ++i) {
                    locations.add(TokenParser.parseLocation(variant, tokens[i], type.getDefaultCoast()));
                }
                seasonBuilder.addRetreat(new Dislodge(new Unit(type, location, country), locations));
            }
        } catch (ReadException e) {
            ParseException ex = new ParseException("Failed to parse Gam: " + e.getMessage(),
                    reader.getLineNumber());
            ex.initCause(e);
            throw ex;
        }
    }

    static private void parseCurrentCenters(LineNumberReader reader, Variant variant,
                                            SeasonBuilder seasonBuilder, Country country,
                                            boolean setCentersAsHome) throws IOException {
        for (String abbrev : COUNTRY_LIST_PATTERN.split(reader.readLine())) {
            if (!"".equals(abbrev)) {
                Sector sector = variant.getSectorByAbbrev(abbrev);
                seasonBuilder.setSupplyOwnership(sector, country);
                if (setCentersAsHome) {
                    country.addHomeCenter(sector);
                }
            }
        }
    }

    static private void parseCurrentUnits(LineNumberReader reader, Variant variant, SeasonBuilder seasonBuilder,
                                          Country country) throws IOException, ParseException, ReadException {
        String line = reader.readLine();
        if ("".equals(line)) {
            return;
        }
        String[] tokens = UNIT_LIST_PATTERN.split(line);
        if ((tokens.length & 1) == 1) {
            throw new ParseException(String.format("Malformed country state: %s", line),
                    reader.getLineNumber());
        }

        for (int i = 0; i < tokens.length - 1; i += 2) {
            Unit.Type type = TokenParser.parseUnitType(tokens[i]);
            Location location = TokenParser.parseLocation(variant, tokens[i + 1],
                    type.getDefaultCoast());
            seasonBuilder.addUnit(type, location, country);
        }
    }

}
