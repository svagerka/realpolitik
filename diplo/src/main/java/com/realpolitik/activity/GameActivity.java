package com.realpolitik.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.realpolitik.R;
import com.realpolitik.controller.GameOpener;
import com.realpolitik.controller.UiHelper;
import com.realpolitik.database.DatabaseController;
import com.realpolitik.database.DatabaseHelper;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.reader.FileParser;
import com.realpolitik.rp.reader.orders.OrderParser;
import com.realpolitik.util.Assertions;
import com.realpolitik.util.Pref;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.text.ParseException;

public class GameActivity extends ActionBarActivity implements GameOpener.Listener {

    private UiHelper uiHelper;
    private FragmentManager fragmentManager;
    private GameOpener gameOpener;
    private DatabaseController databaseController;

    private Game game;

    public static final String EXTRA_VARIANT_NAME = "variant_name";
    public static final String EXTRA_MODE = "mode";
    public static final String EXTRA_SAVED_GAME_ID = "game_id";
    private String[] seasonsArray;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabbed_activity);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        databaseController = new DatabaseController(databaseHelper.getReadableDatabase());
        gameOpener = new GameOpener(this, new FileParser(getAssets(), getResources()),
                getContentResolver(), databaseController);
        uiHelper = new UiHelper(this);
        fragmentManager = getSupportFragmentManager();
        preferences = getPreferences(Context.MODE_PRIVATE);

        seasonsArray = getResources().getStringArray(R.array.phases);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        addTab(R.string.tab_map, "MAP", MapFragment.class);
        addTab(R.string.tab_orders, "ORDER", OrdersFragment.class);
        addTab(R.string.tab_status, "STATUS", StatusFragment.class);
        addTab(R.string.tab_share, "SHARE", ShareFragment.class);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        uiHelper.setLoading(true);
        if (Intent.ACTION_VIEW.equals(action) && type != null) {
            gameOpener.openGame(intent.getData());
        } else if (intent.hasExtra(EXTRA_VARIANT_NAME)) {
            int mode = intent.getIntExtra(EXTRA_MODE, GameInfo.MODE_LOCAL);
            String variantName = intent.getStringExtra(EXTRA_VARIANT_NAME);
            if (mode == GameInfo.MODE_HOT_SEAT
                    && !preferences.getBoolean(Pref.ACCEPTED_HOT_SEAT_INTRODUCTION, false)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.hot_seat_introduction_title)
                        .setMessage(R.string.hot_seat_introduction_text)
                        .setPositiveButton(R.string.hot_seat_introduction_accept,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        preferences.edit()
                                                .putBoolean(Pref.ACCEPTED_HOT_SEAT_INTRODUCTION, true)
                                                .apply();
                                    }
                                })
                        .show();
            }
            gameOpener.openVariant(variantName, mode);
        } else if (intent.hasExtra(EXTRA_SAVED_GAME_ID)) {
            gameOpener.openGame(intent.getLongExtra(EXTRA_SAVED_GAME_ID, -1));
        } else {
            throw new IllegalStateException();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameOpener.cancel();
        if (game != null) {
            game.persistStatus();
        }
    }

    private void addTab(int labelId, String tag, Class<? extends GameFragment> clz) {
        ActionBar.Tab tab = getSupportActionBar().newTab()
                .setText(labelId).setTabListener(new TabListener(this, tag, clz));
        getSupportActionBar().addTab(tab);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_activity, menu);
        uiHelper.prepareMenu(menu, game);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
            case R.id.action_previous:
                try {
                    Assertions.checkState(!game.isCurrentSeasonFirst());
                    game.moveToPrevious();
                    performFullUpdate();
                    return true;
                } catch (ParseException e) {
                    uiHelper.displayError(R.string.error_cannot_parse_season, e, null);
                    return false;
                }
            case R.id.action_next:
                try {
                    Assertions.checkState(!game.isCurrentSeasonLast());
                    game.moveToNext();
                    performFullUpdate();
                    return true;
                } catch (ParseException e) {
                    uiHelper.displayError(R.string.error_cannot_parse_season, e, null);
                    return false;
                }
            case R.id.action_paste:
                boolean success = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB
                        && performPaste();
                if (success) {
                    performFullUpdate();
                }
                return success;
            case R.id.action_resolve:
                game.resolve();
                performFullUpdate();
                return true;
            case R.id.action_advance_to_next_country:
                game.advanceToNextCountry();
                performFullUpdate();
                return true;
            case R.id.action_edit:
                game.performEdit();
                performFullUpdate();
                return true;
            case R.id.action_variant_info:
                new AlertDialog.Builder(this).setMessage(game.variant.info).show();
                return true;
            case R.id.action_prefs:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_help:
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.help_url))));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateAllVisibleFragments();
    }

    public Game getGame() {
        return game;
    }

    public UiHelper getUiHelper() {
        return uiHelper;
    }

    public DatabaseController getDatabaseController() {
        return databaseController;
    }

    @TargetApi(11)
    private boolean performPaste() {
        // TODO: better error reporting

        ClipboardManager clipboardManager =
                (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        Assertions.checkState(clipboardManager.hasPrimaryClip());
        ClipData.Item item = clipboardManager.getPrimaryClip().getItemAt(0);
        CharSequence orders = item.coerceToText(this);
        if (orders == null) {
            uiHelper.displayError(R.string.error_clipboard_empty);
            return false;
        }

        LineNumberReader reader = new LineNumberReader(new StringReader(orders.toString()));
        OrderParser parser = game.getCurrentSeason().getOrderParser();
        boolean hadOrders = false;
        try {
            String line = reader.readLine();
            while (line != null) {
                Order order = null;
                try {
                    order = parser.parseOrder(line);
                } catch (NullPointerException e) {
                    Log.e("PASTE_ERROR", "Failed to parse");
                    hadOrders = true;
                }

                if (order != null) {
                    game.getCurrentSeason().addOrder(order);
                    hadOrders = true;
                }

                line = reader.readLine();
            }
        } catch (IOException e) {
            Log.e("GAMEACTIVITY", e.getMessage());
            return false;
        }

        if (!hadOrders) {
            uiHelper.displayError(R.string.error_clipboard_empty);
        }

        return hadOrders;
    }

    private void updateTitle() {
        if (game == null) {
            setTitle(getString(R.string.app_name));
        } else {
            setTitle(getString(R.string.map_activity_title,
                    seasonsArray[game.getCurrentSeason().phase.id - 1],
                    game.getCurrentSeason().year));
        }
    }

    private void updateAllVisibleFragments() {
        ((GameFragment) (fragmentManager.findFragmentById(R.id.content))).update();
    }

    public void performFullUpdate() {
        supportInvalidateOptionsMenu();
        updateTitle();
        updateAllVisibleFragments();
    }

    // GameOpener.Listener
    @Override
    public void onOpen(Game game) {
        this.game = game;
        game.setSeasonStorage(databaseController);
        performFullUpdate();
        uiHelper.setLoading(false);
    }

    @Override
    public void onOpenError(Exception exception) {
        exception.printStackTrace();
        uiHelper.setLoading(false);
        uiHelper.displayError(R.string.error_loading_failed, exception,
                new UiHelper.OnDialogResult() {
                    @Override
                    public void cancelled() {
                        finish();
                    }

                    @Override
                    public void itemClicked(int which) {
                        finish();
                    }
                }
        );
    }

}
