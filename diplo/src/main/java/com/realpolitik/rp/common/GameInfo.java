package com.realpolitik.rp.common;

import java.util.HashSet;
import java.util.Set;

public class GameInfo {
    public static final int MODE_LOCAL = 0;
    public static final int MODE_HOT_SEAT = 1;

    public GameInfo() {
        hotSeatCountry = null;
        missingCountries = new HashSet<>();
        mode = MODE_LOCAL;
    }

    public String variantName;
    public String gameName;
    public long gameId; ///< game id in database
    public int seasons; ///< number of season in the game

    public int mode;

    /**
     * For hot-seat mode, this is the current player's country.
     */
    public Country hotSeatCountry;

    /**
     * For hot-seat mode, those are the countries that ave orders yet to be input.
     */
    public final Set<Country> missingCountries;
}
