package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.reader.ReadException;
import com.realpolitik.rp.reader.orders.MovementOrderParser;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.SeasonBuilder;
import com.realpolitik.test.AdjudicatorTest;
import com.realpolitik.test.RobolectricGradleTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.realpolitik.rp.common.Order.Resolution.BOUNCE;
import static com.realpolitik.rp.common.Order.Resolution.CUT;
import static com.realpolitik.rp.common.Order.Resolution.DISLODGED;
import static com.realpolitik.rp.common.Order.Resolution.FAILS;
import static com.realpolitik.rp.common.Order.Resolution.ORDERED_TO_MOVE;
import static com.realpolitik.rp.common.Order.Resolution.SUCCESS;
import static com.realpolitik.rp.common.Order.Resolution.VOID;

@RunWith(RobolectricGradleTestRunner.class)
public class SupportMoveAdjudicatorTest extends AdjudicatorTest {

    public SupportMoveAdjudicatorTest() throws ReadException {
    }

    @Test
    public void testSupportedAttackDislodgesUnit() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("A axb Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);
    }

    @Test
    public void testMoveDoesNotCutSupportAgainstSelf() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("A axb Supports A axa - bxb");
        addOrder("Two: A bxb - axb");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);

        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - axb");
        addOrder("Two: A axb Supports A bxb - axa");
        addOrder("Two: A bxb - axa");
        season.resolve();
        assertOrderResolution("bxb", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("axa", DISLODGED);
    }

    @Test
    public void testIncorrectSupportDoesNotAddStrength() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("A axb Supports A axa - bxa");
        addOrder("Two: A bxb Hold");
        season.resolve();
        assertOrderResolution("axa", FAILS);
        assertOrderResolution("axb", VOID);
        assertOrderResolution("bxb", SUCCESS);
    }

    @Test
    public void testHoldSupportDoesNotAddOffensiveStrength() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("A axb Supports A axa");
        addOrder("Two: A bxb Hold");
        season.resolve();
        assertOrderResolution("axa", FAILS);
        assertOrderResolution("axb", ORDERED_TO_MOVE);
        assertOrderResolution("bxb", SUCCESS);
    }

    @Test
    public void testMoveCutsSupport() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1)
                .addUnit(Unit.Type.ARMY, "bxc", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("A axb Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        addOrder("A bxc - axb");
        season.resolve();
        assertOrderResolution("axa", FAILS);
        assertOrderResolution("axb", CUT);
        assertOrderResolution("bxb", SUCCESS);
        assertOrderResolution("bxc", FAILS);
    }

    @Test
    public void testUnitsCannotSwapSpacesWithEqualSupport() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1)
                .addUnit(Unit.Type.ARMY, "bxa", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("A axb Supports A axa - bxb");
        addOrder("Two: A bxb - axa");
        addOrder("A bxa Supports A bxb - axa");
        season.resolve();
        assertOrderResolution("axa", FAILS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", FAILS);
        assertOrderResolution("bxa", SUCCESS);
    }

    @Test
    public void testStrongerCounterAttackAdvances() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxc", 0).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("A axb Supports A axa - bxb");
        addOrder("A bxc Supports A axa - bxb");
        addOrder("Two: A bxb - axa");
        addOrder("A bxa Supports A bxb - axa");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);
        assertOrderResolution("bxa", SUCCESS);
        assertOrderResolution("bxc", SUCCESS);
    }

    @Test
    public void testFriendlySupportDoesNotCauseDislodge() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("Two: A axb Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        season.resolve();
        assertOrderResolution("axa", FAILS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", SUCCESS);
    }

    @Test
    public void testFriendlySupportDoesNotPreventDislodge() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("One: A axb Supports A axa - bxb");
        addOrder("Two: A bxa Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxa", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);
    }

    @Test
    public void testDislodgedUnitDoesNotBounceOnCounterAttack() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("One: A axb Supports A axa - bxb");
        addOrder("Two: A bxa - axa");
        addOrder("Two: A bxb - axa");
        season.resolve();
        assertOrderResolution("axa", SUCCESS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxa", SUCCESS);
        assertOrderResolution("bxb", DISLODGED);
    }

    @Test
    public void testBeleagueredGarrisonIsNotDislodged() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1)
                .addUnit(Unit.Type.ARMY, "cxb", 2)
                .addUnit(Unit.Type.ARMY, "cxa", 2).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - bxb");
        addOrder("One: A axb Supports A axa - bxb");
        addOrder("Two: A bxb Hold");
        addOrder("Three: A cxb - bxb");
        addOrder("Three: A cxa Supports A cxb - bxb");
        season.resolve();
        assertOrderResolution("axa", BOUNCE);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", SUCCESS);
        assertOrderResolution("cxa", SUCCESS);
        assertOrderResolution("cxb", BOUNCE);
    }

    @Test
    public void testCannotDislodgeItself() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa - axb");
        addOrder("One: A axb Hold");
        addOrder("Two: A bxb Supports A axa - axb");
        season.resolve();
        assertOrderResolution("axa", FAILS);
        assertOrderResolution("axb", SUCCESS);
        assertOrderResolution("bxb", SUCCESS);
    }

    @Test
    public void testMutualAttackCutsSupports() throws ReadException {
        season = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "axa", 0)
                .addUnit(Unit.Type.ARMY, "axb", 0)
                .addUnit(Unit.Type.ARMY, "bxa", 1)
                .addUnit(Unit.Type.ARMY, "bxb", 1).build();
        parser = new MovementOrderParser(variant);
        addOrder("One: A axa Supports A axb - bxb");
        addOrder("One: A axb - bxb");
        addOrder("Two: A bxb Supports A bxa - axa");
        addOrder("Two: A bxa - axa");
        season.resolve();
        assertOrderResolution("axa", CUT);
        assertOrderResolution("axb", FAILS);
        assertOrderResolution("bxa", FAILS);
        assertOrderResolution("bxb", CUT);
    }
}
