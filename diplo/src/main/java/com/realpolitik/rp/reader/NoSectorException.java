package com.realpolitik.rp.reader;

class NoSectorException extends ReadException {
    private final String location;

    public NoSectorException(String location) {
        this.location = location;
    }

    @Override
    public String getMessage() {
        return "Location " + location + " not found";
    }
}
