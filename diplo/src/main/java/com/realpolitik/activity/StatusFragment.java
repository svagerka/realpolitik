package com.realpolitik.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.realpolitik.R;
import com.realpolitik.rp.writer.StatusWriter;

public class StatusFragment extends GameFragment {
    private StatusWriter statusWriter;
    private TextView statusTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statusWriter = new StatusWriter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_fragment, container, false);
        statusTextView = (TextView) view.findViewById(R.id.status_text_view);
        return view;
    }

    @Override
    protected void performUpdate() {
        String statusString = statusWriter.write(getGame(), getGame().getCurrentSeason());
        statusTextView.setText(statusString);
    }
}
