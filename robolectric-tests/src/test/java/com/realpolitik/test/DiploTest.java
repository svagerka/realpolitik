package com.realpolitik.test;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.ReadException;
import com.realpolitik.rp.reader.TokenParser;

import org.junit.Ignore;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricGradleTestRunner.class)
@Ignore
abstract public class DiploTest {

    protected final Variant variant;
    private int sectorId;

    public DiploTest() throws ReadException {
        sectorId = 0;
        variant = new Variant("TEST", "Standard");
        addSector("AA", "axa", Sector.TYPE_LAND, null);
        addSector("AB", "axb", Sector.TYPE_SUPPLY, null);
        addSector("AC", "axc", Sector.TYPE_LAND, null);
        addSector("AD", "axd", Sector.TYPE_LAND, null);
        addSector("AE home", "axe", Sector.TYPE_SUPPLY, null);

        addSector("BA", "bxa", Sector.TYPE_LAND, null);
        addSector("BB home", "bxb", Sector.TYPE_SUPPLY, null);
        addSector("BC", "bxc", Sector.TYPE_LAND, null);
        addSector("BD", "bxd", Sector.TYPE_LAND, null);
        addSector("BE", "bxe", Sector.TYPE_LAND, null);

        addSector("CA", "cxa", Sector.TYPE_LAND, null);
        addSector("CB", "cxb", Sector.TYPE_SUPPLY, null);
        addSector("CC Bay", "cxc", Sector.TYPE_DEFAULT, null);
        addSector("CD Water", "cxd", Sector.TYPE_DEFAULT, null);
        addSector("CE Water", "cxe", Sector.TYPE_DEFAULT, null);

        addSector("DA", "dxa", Sector.TYPE_LAND, null);
        addSector("DB Supply", "dxb", Sector.TYPE_SUPPLY, null);
        addSector("DC Bicoastal", "dxc", Sector.TYPE_SUPPLY, null);
        addSector("DD Water", "dxd", Sector.TYPE_LAND, null);
        addSector("DE Water", "dxe", Sector.TYPE_DEFAULT, null);

        addSector("EA", "exa", Sector.TYPE_LAND, null);
        addSector("EB Home", "exb", Sector.TYPE_SUPPLY, null);
        addSector("EC Bay", "exc", Sector.TYPE_DEFAULT, null);
        addSector("ED", "exd", Sector.TYPE_DEFAULT, null);
        addSector("EE Island", "exe", Sector.TYPE_LAND, null);

        addAdjacent("axa", Sector.Coast.ARMY, new String[]{"axb", "bxa", "bxb"});
        addAdjacent("axb", Sector.Coast.ARMY, new String[]{"axa", "axc", "bxc", "bxb"});
        addAdjacent("axc", Sector.Coast.ARMY, new String[]{"axb", "axd", "bxc", "bxd"});
        addAdjacent("axd", Sector.Coast.ARMY, new String[]{"axc", "axe", "bxd", "bxe"});
        addAdjacent("axe", Sector.Coast.ARMY, new String[]{"axd", "bxe"});

        addAdjacent("bxa", Sector.Coast.ARMY, new String[]{"axa", "bxb", "cxa"});
        addAdjacent("bxb", Sector.Coast.ARMY, new String[]{"axa", "axb", "bxa", "bxc", "cxa", "cxc"});
        addAdjacent("bxc", Sector.Coast.ARMY, new String[]{"axb", "axc", "bxb", "bxd", "cxb"});
        addAdjacent("bxc", Sector.Coast.FLEET, new String[]{"cxc"});
        addAdjacent("bxd", Sector.Coast.ARMY, new String[]{"axc", "axd", "bxc", "bxe"});
        addAdjacent("bxd", Sector.Coast.FLEET, new String[]{"cxc", "cxd"});
        addAdjacent("bxe", Sector.Coast.ARMY, new String[]{"axd", "axe", "bxd"});
        addAdjacent("bxe", Sector.Coast.FLEET, new String[]{"cxd", "cxe", "bxd"});

        addAdjacent("cxa", Sector.Coast.ARMY, new String[]{"bxa", "bxb", "cxb", "dxa", "dxb"});
        addAdjacent("cxb", Sector.Coast.ARMY, new String[]{"bxb", "bxc", "dxb", "dxc"});
        addAdjacent("cxb", Sector.Coast.FLEET, new String[]{"bxc", "cxc", "dxc/wc"});
        addAdjacent("cxc", Sector.Coast.FLEET, new String[]{"bxc", "bxd", "cxb", "cxd", "dxc/wc", "dxd"});
        addAdjacent("cxd", Sector.Coast.FLEET, new String[]{"bxd", "bxe", "cxc", "cxe", "dxd", "dxe"});
        addAdjacent("cxe", Sector.Coast.FLEET, new String[]{"bxe", "cxd", "dxe"});

        addAdjacent("dxa", Sector.Coast.ARMY, new String[]{"cxa", "dxb", "exa"});
        addAdjacent("dxb", Sector.Coast.ARMY, new String[]{"cxa", "cxb", "dxa", "dxc", "exa", "exb"});
        addAdjacent("dxc", Sector.Coast.ARMY, new String[]{"cxb", "dxb", "dxd", "exb"});
        addAdjacent("dxc", Sector.Coast.EAST, new String[]{"dxd", "exb", "exc"});
        addAdjacent("dxc", Sector.Coast.WEST, new String[]{"cxb", "cxc", "dxd"});
        addAdjacent("dxd", Sector.Coast.ARMY, new String[]{"dxc"});
        addAdjacent("dxd", Sector.Coast.FLEET, new String[]{"cxc", "cxd", "dxc/ec", "dxc/wc", "dxe", "exc", "exd"});
        addAdjacent("dxe", Sector.Coast.FLEET, new String[]{"cxd", "cxe", "dxd", "exd", "exe"});

        addAdjacent("exa", Sector.Coast.ARMY, new String[]{"dxa", "dxb", "exb"});
        addAdjacent("exb", Sector.Coast.ARMY, new String[]{"dxb", "dxc", "exa"});
        addAdjacent("exb", Sector.Coast.FLEET, new String[]{"dxc/ec", "exc"});
        addAdjacent("exc", Sector.Coast.FLEET, new String[]{"dxc/ec", "dxd", "exb", "exd"});
        addAdjacent("exd", Sector.Coast.FLEET, new String[]{"dxd", "dxe", "exc", "exe"});
        addAdjacent("exe", Sector.Coast.ARMY, new String[]{});
        addAdjacent("exe", Sector.Coast.FLEET, new String[]{"dxe", "exd"});

        variant.addCountry(new Country(0, "One", "First", 'f', Country.Color.BLACK));
        variant.addCountry(new Country(1, "Two", "Second", 's', Country.Color.GREEN));
        variant.addCountry(new Country(2, "Three", "Third", 't', Country.Color.YELLOW));
    }

    protected void assertLocationEquals(String name, Sector.Coast coast, Location location) {
        assertEquals(name, location.sector.name);
        assertEquals(coast, location.coast);
    }

    private void addSector(String name, String firstAbbrev, int type, String[] otherAbbrevs) {
        Sector sector = new Sector(sectorId++, name, firstAbbrev, type);
        variant.addSector(sector);
        variant.addSectorAbbrev(sector, firstAbbrev);
        if (otherAbbrevs != null) {
            for (String abbrev : otherAbbrevs) {
                variant.addSectorAbbrev(sector, abbrev);
            }
        }
    }

    private void addAdjacent(String abbrev, Sector.Coast coast, String[] locations) throws ReadException {
        Sector sector = variant.getSectorByAbbrev(abbrev);
        for (String location : locations) {
            sector.addAdjacent(coast, TokenParser.parseLocation(variant, location,
                    coast == Sector.Coast.ARMY ? Sector.Coast.ARMY : Sector.Coast.FLEET));
        }
    }

}
