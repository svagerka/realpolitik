package com.realpolitik.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * http://www.java2s.com/Code/Android/UI/Fileopendialog.htm
 */
public class FileList extends ListView {

    public static interface OnPathChangedListener {
        public void onChanged(String path);
    }

    public static interface OnFileSelectedListener {
        public void onSelected(String path, String fileName);
    }

    public FileList(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public FileList(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FileList(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        setOnItemClickListener(onItemClick);
    }

    private Context context = null;
    private Pattern fileNamePattern = null;

    private String path = "";

    private OnPathChangedListener onPathChangedListener = null;
    private OnFileSelectedListener onFileSelectedListener = null;

    private List<String> listPath(String path) {
        List<String> folderList = new ArrayList<>();
        List<String> fileList = new ArrayList<>();

        File[] files = new File(path).listFiles();
        if (files == null) {
            return null;
        }

        for (File file : files) {
            String name = file.getName();
            if (file.isDirectory()) {
                folderList.add("<" + name + ">");
            } else {
                if (fileNamePattern == null || fileNamePattern.matcher(name).matches()) {
                    fileList.add(name);
                }
            }
        }

        Collections.sort(folderList);
        Collections.sort(fileList);

        folderList.add(0, "<..>");
        folderList.addAll(fileList);
        return folderList;
    }

    public void setPath(String newPath) {
        if (newPath.length() == 0) {
            newPath = "/";
        } else {
            String lastChar = newPath.substring(newPath.length() - 1,
                    newPath.length());
            if (!lastChar.matches("/"))
                newPath = newPath + "/";
        }

        List<String> entries = listPath(newPath);
        if (entries != null) {
            path = newPath;
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                    android.R.layout.simple_list_item_1, entries);
            setAdapter(adapter);
            if (onPathChangedListener != null) {
                onPathChangedListener.onChanged(newPath);
            }
        }
    }

    public void setOnPathChangedListener(OnPathChangedListener value) {
        onPathChangedListener = value;
    }

    public void setOnFileSelected(OnFileSelectedListener value) {
        onFileSelectedListener = value;
    }

    public void setFileNamePattern(Pattern fileNamePattern) {
        this.fileNamePattern = fileNamePattern;
    }

    private String deleteLastFolder(String value) {
        String list[] = value.split("/");

        String result = "";

        for (int i = 0; i < list.length - 1; i++) {
            result = result + list[i] + "/";
        }

        return result;
    }

    private String getRealPathName(String newPath) {
        String path = newPath.substring(1, newPath.length() - 1);

        if (path.matches("..")) {
            return deleteLastFolder(this.path);
        } else {
            return this.path + path + "/";
        }
    }

    private final OnItemClickListener onItemClick = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                long id) {
            String fileName = getItemAtPosition(position).toString();
            if (fileName.matches("<.*>")) {
                setPath(getRealPathName(fileName));
            } else {
                if (onFileSelectedListener != null)
                    onFileSelectedListener.onSelected(path, fileName);
            }
        }
    };
}
