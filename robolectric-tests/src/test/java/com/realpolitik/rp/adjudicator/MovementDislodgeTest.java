package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.reader.ReadException;
import com.realpolitik.rp.reader.TokenParser;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.season.SeasonBuilder;
import com.realpolitik.test.AdjudicatorTest;
import com.realpolitik.test.RobolectricGradleTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.realpolitik.rp.common.Order.Resolution.BOUNCE;
import static com.realpolitik.rp.common.Order.Resolution.DISLODGED;
import static com.realpolitik.rp.common.Order.Resolution.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricGradleTestRunner.class)
public class MovementDislodgeTest extends AdjudicatorTest {

    public MovementDislodgeTest() throws ReadException {
    }

    @Test
    public void testSupportedAttackCausesDislodge() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxa", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 0)
                .addUnit(Unit.Type.ARMY, "cxa", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxa - cxa");
        addOrder("One: A bxb Supports A bxa - cxa");
        addOrder("Two: A cxa Hold");
        season.resolve();
        assertOrderResolution("bxa", SUCCESS);
        assertOrderResolution("cxa", DISLODGED);
    }

    @Test
    public void testAdjacentSpacesCanBeRetreatedTo() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxa", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 0)
                .addUnit(Unit.Type.ARMY, "cxa", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxa - cxa");
        addOrder("One: A bxb Supports A bxa - cxa");
        addOrder("Two: A cxa Hold");
        Season nextSeason = season.resolve();
        assertCanRetreatTo(nextSeason, "cxa", Sector.Coast.ARMY, "dxb");
        assertCanRetreatTo(nextSeason, "cxa", Sector.Coast.ARMY, "dxa");
        assertCanRetreatTo(nextSeason, "cxa", Sector.Coast.ARMY, "cxb");
    }

    @Test
    public void testCannotRetreatToAreaWithNewUnit() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxa", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 0)
                .addUnit(Unit.Type.ARMY, "cxa", 1)
                .addUnit(Unit.Type.ARMY, "exa", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxa - cxa");
        addOrder("One: A bxb Supports A bxa - cxa");
        addOrder("Two: A cxa Hold");
        addOrder("Two: A exa - dxa");
        Season nextSeason = season.resolve();
        assertCannotRetreatTo(nextSeason, "cxa", Sector.Coast.ARMY, "dxa");
    }

    @Test
    public void testCanRetreatToVacatedSector() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxa", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 0)
                .addUnit(Unit.Type.ARMY, "cxa", 1)
                .addUnit(Unit.Type.ARMY, "dxa", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxa - cxa");
        addOrder("One: A bxb Supports A bxa - cxa");
        addOrder("Two: A cxa Hold");
        addOrder("Two: A dxa - exa");
        Season nextSeason = season.resolve();
        assertCanRetreatTo(nextSeason, "cxa", Sector.Coast.ARMY, "dxa");
    }

    @Test
    public void testCannotRetreatToSectorWithBounce() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxa", 0)
                .addUnit(Unit.Type.ARMY, "bxb", 0)
                .addUnit(Unit.Type.ARMY, "cxa", 1)
                .addUnit(Unit.Type.ARMY, "exa", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxa - cxa");
        addOrder("One: A bxb Supports A bxa - cxa");
        addOrder("Two: A cxa - dxa");
        addOrder("Two: A exa - dxa");
        Season nextSeason = season.resolve();
        assertOrderResolution("cxa", DISLODGED);
        assertOrderResolution("exa", BOUNCE);
        assertCannotRetreatTo(nextSeason, "cxa", Sector.Coast.ARMY, "dxa");
    }

    private void assertCanRetreatTo(Season season, String sector, Sector.Coast sourceCoast,
                                    String location) throws ReadException {
        assertEquals(Phase.PhaseType.RETREAT, season.getPhaseType());
        Dislodge retreat = season.getRetreat(variant.getSectorByAbbrev(sector));
        assertNotNull(retreat);

        Location parsedLocation = TokenParser.parseLocation(variant, location, sourceCoast);
        assertTrue(parsedLocation + " missing in retreats " + retreat.locations.toString(),
                retreat.canRetreatTo(parsedLocation));
    }

    private void assertCannotRetreatTo(Season season, String sector, Sector.Coast sourceCoast,
                                       String location) throws ReadException {
        Dislodge retreat = season.getRetreat(variant.getSectorByAbbrev(sector));
        if (retreat == null) {
            return;
        }
        Location parsedLocation = TokenParser.parseLocation(variant, location, sourceCoast);
        assertFalse(parsedLocation + " is in retreats " + retreat.locations.toString(),
                retreat.canRetreatTo(parsedLocation));
    }
}
