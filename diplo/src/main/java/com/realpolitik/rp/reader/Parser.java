package com.realpolitik.rp.reader;

import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;

class Parser {
    private final LineNumberReader reader;

    private char[] line;
    private int pos;

    Parser(LineNumberReader reader) {
        this.reader = reader;
    }

    String readLine() throws IOException {
        this.pos = 0;
        this.line = null;

        String line = reader.readLine();
        this.line = line != null ? line.toCharArray() : null;
        return line;
    }

    public int parseInt() throws ParseException {
        if (pos >= line.length) {
            throw new ParseException("Malformed line, expecting int", reader.getLineNumber());
        }

        while (line[pos] < '0' || line[pos] > '9') {
            pos++;
        }

        int result = 0;
        while (pos < line.length && line[pos] >= '0' && line[pos] <= '9') {
            result *= 10;
            result += line[pos] - '0';
            ++pos;
        }
        return result;
    }

}
