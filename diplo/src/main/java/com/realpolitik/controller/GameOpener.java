package com.realpolitik.controller;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.AsyncTask;

import com.realpolitik.database.DatabaseController;
import com.realpolitik.rp.common.CompressedSeason;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.DpyFileParser;
import com.realpolitik.rp.reader.FileParser;
import com.realpolitik.rp.season.Season;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.text.ParseException;
import java.util.List;

public class GameOpener {
    public interface Listener {
        void onOpen(Game game);

        void onOpenError(Exception exception);
    }

    private final Listener listener;
    private final FileParser fileParser;
    private final ContentResolver contentResolver;
    private final DatabaseController databaseController;
    private AsyncTask currentTask;

    public GameOpener(Listener listener, FileParser fileParser, ContentResolver contentResolver,
                      DatabaseController databaseController) {
        this.listener = listener;
        this.fileParser = fileParser;
        this.contentResolver = contentResolver;
        this.databaseController = databaseController;
    }

    public void openGame(final Uri data) {
        currentTask = new AsyncTask<Uri, Integer, Game>() {

            private Exception exception;

            @Override
            protected Game doInBackground(Uri... uris) {
                LineNumberReader reader;
                try {
                    reader = new LineNumberReader(
                            new InputStreamReader(contentResolver.openInputStream(uris[0])));
                } catch (FileNotFoundException e) {
                    exception = e;
                    return null;
                }

                try {
                    Game game = fileParser.parseGameFile(reader);
                    List<CompressedSeason> seasons =
                            fileParser.parsePastSeasons(reader, game.gameInfo.seasons);
                    databaseController.insertGame(game, seasons);
                    return game;
                } catch (ParseException e) {
                    exception = e;
                } catch (Exception e) {
                    exception = new ParseException("Failed to open game", reader.getLineNumber());
                    exception.initCause(e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Game game) {
                if (game != null) {
                    listener.onOpen(game);
                } else if (exception != null) {
                    listener.onOpenError(exception);
                }
            }
        }.execute(data);
    }

    public void openVariant(String variantName, final int mode) {
        currentTask = new AsyncTask<String, Integer, Game>() {

            private Exception exception;

            @Override
            protected Game doInBackground(String... strings) {
                try {
                    Variant variant = fileParser.parseVariantFile(strings[0]);
                    Game game = new Game(variant);
                    game.gameInfo.mode = mode;
                    databaseController.insertGame(game, null);
                    return game;
                } catch (IOException | ParseException e) {
                    exception = e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Game game) {
                if (game != null) {
                    listener.onOpen(game);
                } else if (exception != null) {
                    listener.onOpenError(exception);
                }
            }
        }.execute(variantName);
    }

    public void openGame(long gameId) {
        currentTask = new AsyncTask<Long, Integer, Game>() {

            private Exception exception;

            @Override
            protected Game doInBackground(Long... longs) {
                LineNumberReader reader = null;
                try {
                    Long gameId = longs[0];
                    GameInfo gameInfo = databaseController.getGameInfo(gameId);
                    Variant variant = fileParser.parseVariantFile(gameInfo.variantName);
                    CompressedSeason lastSeason = databaseController.getLastSeason(gameId);
                    databaseController.loadGameMode(gameInfo, variant.getCountries());
                    reader = new LineNumberReader(new StringReader(lastSeason.data));
                    Season season = DpyFileParser.parseSeasonInfo(reader, variant);
                    return new Game(gameInfo, variant, season);
                } catch (IOException | ParseException | IllegalArgumentException e) {
                    exception = e;
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            /* Either the game was loaded correctly or a more specific error
                             * occurred. Don't care about this exception, then. */
                        }
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Game game) {
                if (game != null) {
                    listener.onOpen(game);
                } else if (exception != null) {
                    listener.onOpenError(exception);
                }
            }
        }.execute(gameId);
    }

    public void cancel() {
        if (currentTask != null) {
            currentTask.cancel(true);
        }
    }

}
