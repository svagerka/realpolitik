package com.realpolitik.rp.season;

import com.realpolitik.rp.adjudicator.MovementAdjudicator;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.orders.MovementOrderParser;
import com.realpolitik.rp.reader.orders.OrderParser;

import java.util.Map;

public class MovementSeason extends Season {
    MovementSeason(Variant variant, Phase phase, int year,
                   int[] adjustmentCount, Country[] supplyCenters, Unit[] units,
                   Map<Sector, Order> orders) {
        super(variant, phase, year, adjustmentCount, supplyCenters, units, orders);
    }

    @Override
    public Season resolve() {
        return new MovementAdjudicator(variant, this).getNextSeason();
    }

    @Override
    public OrderParser getOrderParser() {
        return new MovementOrderParser(variant);
    }
}
