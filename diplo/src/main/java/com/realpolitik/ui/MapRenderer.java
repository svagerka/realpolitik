package com.realpolitik.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.SparseArray;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapRenderer {
    private static final int UNIT_ICON_SIZE_SCALED = 32;
    private static final int SUPPLY_CENTER_ALPHA = 0x66000000;
    private static final int SELECTED_SECTOR_COLOR = 0x99000000;

    private static final int ARROW_TYPE_MOVE = 1;
    private static final int ARROW_TYPE_SUPPORT_HOLD = 2;
    private static final int ARROW_TYPE_SUPPORT_MOVE = 3;
    private static final int ARROW_TYPE_CONVOY = 4;

    private static final int SUCCESSFUL_ORDER_ALPHA = 0xCC000000;
    private static final int FAILED_ORDER_ALPHA = 0x66000000;

    private static final float TRIANGLE_SIZE = 3.5f;
    private static final float ARROW_CIRCLE_RADIUS = 3f;
    private static final int BUILD_RECT_SIZE = 18;
    private static final int DISBAND_CROSS_SIZE = 14;
    private static final int REMOVE_CROSS_SIZE = 14;

    private final Point NORMAL_UNIT_OFFSET = new Point(0, 0);
    private final Point BUILT_UNIT_OFFSET = NORMAL_UNIT_OFFSET;
    private final Point RETREATING_UNIT_OFFSET = new Point(10, 10);

    private final Paint unitPaint;
    private final Paint centerPaint;
    private final Rect destRect;
    private final SparseArray<Bitmap> armyIcons;
    private final SparseArray<Bitmap> fleetIcons;
    private final Map<Character, Bitmap> countryIcons;
    private final Context context;
    private final ConvoyRenderer convoyRenderer;

    private static MapRenderer instance;
    private final Paint arrowPaint;

    public static MapRenderer getInstance(Context context) {
        if (instance == null) {
            instance = new MapRenderer(context);
        }
        return instance;
    }

    private MapRenderer(Context context) {
        this.context = context;

        unitPaint = new Paint();
        centerPaint = new Paint();
        centerPaint.setAntiAlias(true);
        arrowPaint = new Paint();
        arrowPaint.setStrokeWidth(1.5f);
        arrowPaint.setAntiAlias(true);

        destRect = new Rect(0, 0, UNIT_ICON_SIZE_SCALED, UNIT_ICON_SIZE_SCALED);
        armyIcons = new SparseArray<>();
        fleetIcons = new SparseArray<>();
        countryIcons = new HashMap<>();
        convoyRenderer = new ConvoyRenderer();
    }

    public void render(Canvas c, Game game, Sector selectedSector) {
        Variant variant = game.variant;
        Season season = game.getCurrentSeason();

        // select orders to draw
        List<Order> allOrders;
        if (game.gameInfo.mode == GameInfo.MODE_HOT_SEAT && game.isCurrentSeasonLast()) {
            allOrders = season.getOrdersForCountry(game.gameInfo.hotSeatCountry);
        } else {
            allOrders = season.getAllOrders();
        }

        // draw background
        c.drawBitmap(variant.colorMap, 0, 0, null);

        // draw SC
        Country[] supplyCenters = season.getSupplyCenters();
        for (int i = 0; i < supplyCenters.length; ++i) {
            Sector sector = variant.getSectors().get(i);
            Country country = supplyCenters[i];
            if (country != null) {
                Path path = variant.regionInfo.getPathForSector(sector);
                centerPaint.setColor(SUPPLY_CENTER_ALPHA | country.color.color);
                c.drawPath(path, centerPaint);
            }
        }

        // draw move and build orders
        convoyRenderer.prepare(variant, c, season);
        for (Order order : allOrders) {
            drawOrder(c, order, Order.Type.MOVE);
            drawOrder(c, order, Order.Type.RETREAT);
            drawOrder(c, order, Order.Type.BUILD);

            if (order.type == Order.Type.MOVE) {
                convoyRenderer.drawConvoyRoute(order, order.unit.location, 0);
            }
        }

        // draw secondary orders
        for (Order order : allOrders) {
            drawOrder(c, order, Order.Type.SUPPORT_HOLD);
            drawOrder(c, order, Order.Type.SUPPORT_MOVE);
            if (order.type == Order.Type.CONVOY && !convoyRenderer.convoySuccessful(order)) {
                drawOrder(c, order, Order.Type.CONVOY);
            }
        }

        // draw retreating units
        if (season.getPhaseType() == Phase.PhaseType.RETREAT) {
            for (Dislodge dislodge : season.getAllRetreats()) {
                drawUnit(c, dislodge.unit, RETREATING_UNIT_OFFSET);
            }
        }

        // draw retreating unit remove orders
        arrowPaint.setStrokeWidth(1.5f);
        for (Order order : allOrders) {
            drawOrder(c, order, Order.Type.DISBAND);
        }

        // draw units
        for (Unit unit : season.getAllUnits()) {
            drawUnit(c, unit, NORMAL_UNIT_OFFSET);
        }

        // draw removal orders
        for (Order order : allOrders) {
            drawOrder(c, order, Order.Type.REMOVE);
            drawOrder(c, order, Order.Type.DEFAULT_REMOVE);
        }

        // draw selected sector
        if (selectedSector != null) {
            Path path = variant.regionInfo.getPathForSector(selectedSector);
            centerPaint.setColor(SELECTED_SECTOR_COLOR);
            c.drawPath(path, centerPaint);
        }
    }

    private void drawOrder(Canvas c, Order order, Order.Type orderType) {
        if (orderType != order.type) {
            return;
        }
        setPaintColor(order);
        int x = order.unit.location.getX();
        int y = order.unit.location.getY();
        Location targetLocation = order.targetLocation;
        Unit targetUnit = order.targetUnit;
        switch (order.type) {
            case MOVE:
                drawArrow(c, ARROW_TYPE_MOVE, x, y,
                        targetLocation.getX(), targetLocation.getY());
                break;
            case RETREAT:
                drawArrow(c, ARROW_TYPE_MOVE,
                        x + RETREATING_UNIT_OFFSET.x, y + RETREATING_UNIT_OFFSET.y,
                        targetLocation.getX(), targetLocation.getY());
                break;
            case SUPPORT_HOLD:
                drawArrow(c, ARROW_TYPE_SUPPORT_HOLD, x, y,
                        targetUnit.getX(), targetUnit.getY());
                break;
            case HOLD:
            case NO_MOVE_RECEIVED:
                // do not draw
                break;
            case SUPPORT_MOVE:
                drawArrow(c, ARROW_TYPE_SUPPORT_MOVE, x, y,
                        0.6f * targetLocation.getX() + 0.4f * targetUnit.getX(),
                        0.6f * targetLocation.getY() + 0.4f * targetUnit.getY());
                break;
            case CONVOY:
                drawArrow(c, ARROW_TYPE_CONVOY, x, y,
                        targetLocation.getX(), targetLocation.getY());
                drawArrow(c, ARROW_TYPE_CONVOY,
                        targetUnit.getX(), targetUnit.getY(), x, y);
                break;
            case REMOVE:
            case DEFAULT_REMOVE:
                drawCross(c, x, y, REMOVE_CROSS_SIZE);
                break;
            case DISBAND:
                drawCross(c, x + RETREATING_UNIT_OFFSET.x, y + RETREATING_UNIT_OFFSET.y,
                        DISBAND_CROSS_SIZE);
                break;
            case BUILD:
                drawUnit(c, order.unit, BUILT_UNIT_OFFSET);
                drawRect(c, x, y, BUILD_RECT_SIZE);
                break;

        }
    }

    private void setPaintColor(Order order) {
        arrowPaint.setColor(order.unit.country.color.color
                | (order.getResolution() == Order.Resolution.SUCCESS
                ? SUCCESSFUL_ORDER_ALPHA : FAILED_ORDER_ALPHA));
    }

    private void drawRect(Canvas c, int x, int y, int s) {
        arrowPaint.setStyle(Paint.Style.STROKE);
        c.drawRect(x - s, y - s, x + s, y + s, arrowPaint);
    }

    private void drawCross(Canvas c, int x, int y, int s) {
        c.drawLine(x - s, y - s, x + s, y + s, arrowPaint);
        c.drawLine(x - s, y + s, x + s, y - s, arrowPaint);
    }

    private void drawUnit(Canvas c, Unit unit, Point offset) {
        Bitmap unitIcon = getUnitIcon(unit.type, unit.country.color);
        Bitmap countryIcon = getCountryIcon(unit.country.initial);
        destRect.offsetTo(
                unit.location.getUnitPosition().x + offset.x - UNIT_ICON_SIZE_SCALED / 2,
                unit.location.getUnitPosition().y + offset.y - UNIT_ICON_SIZE_SCALED / 2
        );

        c.drawBitmap(unitIcon, null, destRect, unitPaint);
        c.drawBitmap(countryIcon, null, destRect, unitPaint);
    }

    private void drawArrow(Canvas canvas, int arrowType,
                           float fromX, float fromY, float toX, float toY) {
        float margin;
        switch (arrowType) {
            case ARROW_TYPE_MOVE:
            case ARROW_TYPE_SUPPORT_HOLD:
                margin = 14f;
                break;
            case ARROW_TYPE_CONVOY:
                margin = 8f;
                break;
            case ARROW_TYPE_SUPPORT_MOVE:
                margin = 0;
                break;
            default:
                throw new IllegalArgumentException();
        }

        // draw line
        double deltaX = toX - fromX;
        double deltaY = toY - fromY;
        if (margin > 0) {
            double length = margin / Math.sqrt(deltaX * deltaX + deltaY * deltaY);
            toX -= (float) (deltaX * length);
            toY -= (float) (deltaY * length);
        }
        canvas.drawLine(fromX, fromY, toX, toY, arrowPaint);

        // draw arrow
        arrowPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        switch (arrowType) {
            case ARROW_TYPE_SUPPORT_HOLD:
            case ARROW_TYPE_SUPPORT_MOVE:
                canvas.drawCircle(toX, toY, ARROW_CIRCLE_RADIUS, arrowPaint);
                break;
            case ARROW_TYPE_CONVOY:
                canvas.drawCircle((fromX + toX) / 2, (fromY + toY) / 2, ARROW_CIRCLE_RADIUS,
                        arrowPaint);
                // fall through and draw the triangle as well
            case ARROW_TYPE_MOVE:
                double angle = Math.atan2(deltaY, deltaX);
                float sinUnit = TRIANGLE_SIZE * (float) Math.sin(angle);
                float cosUnit = TRIANGLE_SIZE * (float) Math.cos(angle);

                Path path = new Path();
                path.moveTo(toX - sinUnit, toY + cosUnit);
                path.lineTo(toX + 2 * cosUnit, toY + 2 * sinUnit);
                path.lineTo(toX + sinUnit, toY - cosUnit);
                path.lineTo(toX - sinUnit, toY + cosUnit);
                path.close();

                canvas.drawPath(path, arrowPaint);
                break;
        }

    }

    private Bitmap getCountryIcon(char initial) {
        Bitmap bitmap = countryIcons.get(initial);
        if (bitmap != null) {
            return bitmap;
        }

        // unit icon not loaded yet
        String iconName = String.format("_%s", Character.toLowerCase(initial));
        bitmap = loadBitmap(iconName);
        countryIcons.put(initial, bitmap);
        return bitmap;
    }

    private Bitmap getUnitIcon(Unit.Type type, Country.Color color) {
        SparseArray<Bitmap> map = (type == Unit.Type.ARMY) ? armyIcons : fleetIcons;
        Bitmap bitmap = map.get(color.color);
        if (bitmap != null) {
            return bitmap;
        }

        // unit icon not loaded yet
        String iconName = String.format("%s%s",
                color.name.toLowerCase(), (type == Unit.Type.ARMY) ? "army" : "fleet");
        bitmap = loadBitmap(iconName);
        map.put(color.color, bitmap);
        return bitmap;
    }

    private Bitmap loadBitmap(String bitmapName) {
        Resources resources = context.getResources();
        int id = resources.getIdentifier(bitmapName, "drawable",
                context.getApplicationInfo().packageName);
        return BitmapFactory.decodeResource(resources, id);
    }

    private class ConvoyRenderer {

        private Canvas canvas;
        private Season season;
        private int pathDepth[];
        private boolean successfulConvoys[];

        void prepare(Variant variant, Canvas canvas, Season season) {
            this.canvas = canvas;
            this.season = season;

            int count = variant.getSectors().size();
            if (pathDepth == null || pathDepth.length != count) {
                pathDepth = new int[count];
                successfulConvoys = new boolean[count];
            } else {
                Arrays.fill(pathDepth, 0);
                Arrays.fill(successfulConvoys, false);
            }
        }

        boolean drawConvoyRoute(Order order, Location curr, int depth) {
            Sector end = order.targetLocation.sector;
            Location start = order.unit.location;
            boolean result = false;

            if (pathDepth[curr.sector.id] != depth) {
                // We have already been here or we took a detour.
                return false;
            }

            boolean isStart = start.sector == curr.sector;
            if (curr.sector == end && !isStart) {
                // We found the end.
                // Only a successful convoy when the route is at
                // least 3 nodes (at least one fleet).
                return depth > 1;
            }

            Order currentOrder = isStart ? order : season.getOrder(curr.sector);
            if (!isStart) {
                if (currentOrder == null || currentOrder.type != Order.Type.CONVOY
                        || currentOrder.targetLocation.sector != end
                        || currentOrder.targetUnit.location.sector != start.sector) {
                    // We do not have a convoying fleet that can continue
                    // our journey.
                    return false;
                }
            }

            // Add marks
            for (Sector.Coast coast : curr.sector.getAvailableCoasts()) {
                for (Location location : curr.sector.getAllAdjacencies(coast)) {
                    // Do not overwrite pathDepth set by previous node
                    // on current route. Do not set pathDepth on destination when
                    // we start.
                    if (pathDepth[location.sector.id] == 0
                            && (!isStart || location.sector != end)) {
                        pathDepth[location.sector.id] = depth + 1;
                    }
                }
            }

            // Go into recursion for every neighbor.
            for (Sector.Coast coast : curr.sector.getAvailableCoasts()) {
                for (Location location : curr.sector.getAllAdjacencies(coast)) {
                    boolean found = drawConvoyRoute(order, location, depth + 1);
                    if (found) {
                        // Successful route.
                        // Now, we have to draw an arrow to that successful node.
                        if (location.sector == order.targetLocation.sector) {
                            location = order.targetLocation;
                        }
                        setPaintColor(currentOrder);
                        drawArrow(canvas, ARROW_TYPE_CONVOY,
                                curr.getX(), curr.getY(), location.getX(), location.getY());
                        result = true;
                    }
                }
            }

            // Clear marks
            for (Sector.Coast coast : curr.sector.getAvailableCoasts()) {
                for (Location location : curr.sector.getAllAdjacencies(coast)) {
                    if (pathDepth[location.sector.id] == depth + 1) {
                        pathDepth[location.sector.id] = 0;
                    }
                }
            }

            successfulConvoys[curr.sector.id] |= result;
            return result;
        }

        boolean convoySuccessful(Order order) {
            return successfulConvoys[order.unit.location.sector.id];
        }
    }


}
