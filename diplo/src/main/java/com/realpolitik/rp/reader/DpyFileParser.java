package com.realpolitik.rp.reader;

import android.util.Log;

import com.realpolitik.rp.common.CompressedSeason;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.orders.OrderParser;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.season.SeasonBuilder;

import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;

public class DpyFileParser {
    static private void parseSeasonUnits(LineNumberReader reader, Variant variant, SeasonBuilder seasonBuilder) throws IOException {
        char[] conciseUnits = reader.readLine().toCharArray();
        int pos = 0;
        for (Sector sector : variant.getSectors()) {
            char country = conciseUnits[pos++];
            if (country == '-') continue;

            Sector.Coast coast;
            Unit.Type type = Unit.Type.FLEET; // unit is fleet, unless specified otherwise
            if (pos == conciseUnits.length) {
                coast = Sector.Coast.FLEET;
            } else {
                // there can be a one char specification after the country initial, try to parse it
                switch (conciseUnits[pos++]) {
                    case 'a':
                        type = Unit.Type.ARMY;
                        coast = Sector.Coast.ARMY;
                        break;
                    case 'n':
                        coast = Sector.Coast.NORTH;
                        break;
                    case 's':
                        coast = Sector.Coast.SOUTH;
                        break;
                    case 'w':
                        coast = Sector.Coast.WEST;
                        break;
                    case 'e':
                        coast = Sector.Coast.EAST;
                        break;
                    default:
                        pos--; // the specification was not used, back one char
                        coast = Sector.Coast.FLEET;
                        break;
                }
            }

            seasonBuilder.addUnit(type, new Location(sector, coast),
                    variant.getCountryByInitial(country));
        }

    }

    private static void parseSeasonCenters(LineNumberReader reader, Variant variant, SeasonBuilder seasonBuilder) throws IOException {
        char[] conciseCenters = reader.readLine().toCharArray();
        int pos = 0;
        for (Sector sector : variant.getSectors()) {
            if (!sector.isSupply()) continue;
            char country = conciseCenters[pos++];
            if (country == '-') continue;
            seasonBuilder.setSupplyOwnership(sector, variant.getCountryByInitial(country));
        }
    }

    private static void parseAdjustmentCount(LineNumberReader reader, Variant variant, SeasonBuilder seasonBuilder) throws IOException {
        reader.readLine(); // ignore total adjustment count
        String[] tokens = reader.readLine().split(" ");
        int i = 0;
        for (Country country : variant.getCountries()) {
            seasonBuilder.setAdjustmentCount(country, Integer.parseInt(tokens[i++]));
        }
    }

    private static void parseOrders(LineNumberReader reader, Season season,
                                    int orderCount) throws IOException {
        OrderParser parser = season.getOrderParser();

        while (orderCount-- > 0) {
            String line = reader.readLine();
            if (line.isEmpty()) {
                continue;
            }
            try {
                Order order = parser.parseOrder(line);
                if (order != null) {
                    season.addOrder(order);
                } else {
                    Log.w("DPYPARSER", "Did not parse order " + line);
                }
            } catch (NullPointerException e) {
                Log.w("DPYPARSER", "Failed to parse order " + line);
                e.printStackTrace();
            }
        }

        // TODO: build waived
    }

    public static Season parseSeasonInfo(LineNumberReader reader, Variant variant) throws IOException, ParseException {
        String[] tokens = reader.readLine().split(" ");
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant,
                Phase.fromInt(Integer.parseInt(tokens[0])),
                Integer.parseInt(tokens[1]));

        parseSeasonUnits(reader, variant, seasonBuilder);
        parseSeasonCenters(reader, variant, seasonBuilder);
        parseAdjustmentCount(reader, variant, seasonBuilder);
        GamFileParser.parseRetreats(reader, variant, seasonBuilder);
        Season season = seasonBuilder.build();
        parseOrders(reader, season, seasonBuilder.getOrderCount());
        return season;
    }

    public static CompressedSeason parseCompressedSeason(LineNumberReader reader) throws IOException, ParseException {
        int orderCount = 0;
        CopyingParser parser = new CopyingParser(reader);

        parser.readLine();
        Phase phase = Phase.fromInt(parser.parseInt());
        int year = parser.parseInt();
        Phase.PhaseType phaseType = Phase.PhaseType.fromSeason(phase);

        // season units
        String unitLine = parser.readLine();
        if (phaseType == Phase.PhaseType.MOVEMENT) {
            for (char ch : unitLine.toCharArray()) {
                if (ch >= 'A' && ch <= 'Z') {
                    ++orderCount;
                }
            }
        }

        parser.readLine(); // season SCs

        // total adjustment count
        parser.readLine();
        if (phaseType == Phase.PhaseType.ADJUSTMENT) {
            orderCount = parser.parseInt();
        }

        // adjustment count
        parser.readLine();

        // retreats
        parser.readLine();
        int retreatCount = parser.parseInt();
        for (int i = 0; i < retreatCount; ++i) {
            parser.readLine();
        }
        if (phaseType == Phase.PhaseType.RETREAT) {
            orderCount = retreatCount;
        }


        for (int i = 0; i < orderCount; ++i) {
            parser.readLine();
        }

        return new CompressedSeason(phase, year, parser.toString());
    }
}
