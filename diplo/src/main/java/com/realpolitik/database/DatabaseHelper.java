package com.realpolitik.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "realpolitik";
    private static final int DATABASE_VERSION = 2;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createGamesV1(db);
        createSeasonsV1(db);
        updateGamesV2(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 2 && newVersion >= 2) {
            updateGamesV2(db);
        }
    }

    private void createGamesV1(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DatabaseContract.GameContract.TABLE_NAME + "( " +
                DatabaseContract.GameContract.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseContract.GameContract.COLUMN_NAME + " VARCHAR(64), " +
                DatabaseContract.GameContract.COLUMN_VARIANT + " VARCHAR(64) NOT NULL, " +
                DatabaseContract.GameContract.COLUMN_CREATED + " DATETIME NOT NULL, " +
                DatabaseContract.GameContract.COLUMN_MODIFIED + " DATETIME NOT NULL" +
                ")");
    }

    private void createSeasonsV1(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DatabaseContract.SeasonContract.TABLE_NAME + "( " +
                DatabaseContract.SeasonContract.COLUMN_GAME_ID + " INTEGER NOT NULL, " +
                DatabaseContract.SeasonContract.COLUMN_SEASON_ID + " INTEGER NOT NULL, " +
                DatabaseContract.SeasonContract.COLUMN_PHASE_AND_YEAR + " INTEGER NOT NULL, " +
                DatabaseContract.SeasonContract.COLUMN_RAW_DATA + " TEXT NOT NULL" +
                ")");
    }

    private void updateGamesV2(SQLiteDatabase db) {
        String addColumn = "ALTER TABLE " + DatabaseContract.GameContract.TABLE_NAME
                + " ADD COLUMN ";
        db.execSQL(addColumn + DatabaseContract.GameContract.COLUMN_MODE
                + " INTEGER NOT NULL DEFAULT 0");
        db.execSQL(addColumn + DatabaseContract.GameContract.COLUMN_HOT_SEAT_COUNTRY
                + " INTEGER NOT NULL DEFAULT -1");
        db.execSQL(addColumn + DatabaseContract.GameContract.COLUMN_MISSING_COUNTRIES
                + " BIGINT NOT NULL DEFAULT 0");
    }
}
