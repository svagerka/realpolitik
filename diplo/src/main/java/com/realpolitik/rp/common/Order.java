package com.realpolitik.rp.common;

import com.realpolitik.util.Assertions;

public class Order implements Comparable<Order> {

    public enum Type {
        NO_MOVE_RECEIVED,
        HOLD,
        MOVE,
        SUPPORT_MOVE,
        SUPPORT_HOLD,
        CONVOY,
        BUILD,
        REMOVE,
        DEFAULT_REMOVE,
        RETREAT,
        DISBAND
    }

    public enum Resolution {
        SUCCESS(null),
        NO_SUCH_UNIT("No such unit"),
        INVALID_UNIT("Invalid unit"),
        VOID("Void"),
        FAILS("Fails"),
        ORDERED_TO_MOVE("Ordered to move"),
        INVALID_SECTOR("Invalid sector"),
        BOUNCE("Bounce"),
        DISLODGED("Dislodged"),
        CUT("Cut"),
        DISBANDED("Disbanded");

        public final String label;

        private Resolution(String label) {
            this.label = label;
        }

        public static Resolution getByLabel(String label) {
            if (label == null) {
                return SUCCESS;
            }

            for (Resolution resolution : Resolution.values()) {
                if (label.equalsIgnoreCase(resolution.label)) {
                    return resolution;
                }
            }
            return null;
        }
    }

    public final Unit unit;
    public final Location targetLocation;
    public final Unit targetUnit;
    public final Type type;
    private Resolution resolution;

    public Order(Unit unit, Type type) {
        this(unit, type, null, null);
    }

    public Order(Unit unit, Type type, Location targetLocation) {
        this(unit, type, null, targetLocation);
    }

    public Order(Unit unit, Type type, Unit targetUnit) {
        this(unit, type, targetUnit, null);
    }

    public Order(Unit unit, Type type, Unit targetUnit, Location targetLocation) {
        this.unit = Assertions.notNull(unit);
        this.type = Assertions.notNull(type);
        this.targetLocation = targetLocation;
        this.targetUnit = targetUnit;
        this.resolution = Resolution.SUCCESS;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = Assertions.notNull(resolution);
    }

    @Override
    public int compareTo(Order order) {
        return unit.compareTo(order.unit);
    }

}
