package com.realpolitik.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.realpolitik.rp.common.CompressedSeason;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.util.Assertions;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.realpolitik.database.DatabaseContract.GameContract;
import static com.realpolitik.database.DatabaseContract.GameSeasonJoinedContract;
import static com.realpolitik.database.DatabaseContract.SeasonContract;

public class DatabaseController implements Game.SeasonStorage {
    private final SQLiteDatabase db;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public DatabaseController(SQLiteDatabase db) {
        this.db = db;
    }

    public Cursor listGames() {
        return db.query(
                GameSeasonJoinedContract.TABLE_NAME,
                GameSeasonJoinedContract.PROJECTION,
                null, null, GameContract.COLUMN_ID, null, GameContract.COLUMN_MODIFIED + " DESC");
    }

    public void renameGame(long gameId, String newName) {
        ContentValues values = new ContentValues();
        values.put(GameContract.COLUMN_NAME, newName);
        values.put(GameContract.COLUMN_MODIFIED, getCurrentTimestamp());
        db.update(GameContract.TABLE_NAME, values, GameContract.WHERE_ID,
                SelectionArgs.get(gameId));
    }

    /**
     * Inserts game to database. If there has already been a similar one, it is replaced.
     */
    public void insertGame(Game game, List<CompressedSeason> pastSeasons) {
        /* If there is a game with same name, variant and fewer or equal amount of seasons,
         * replace it. */
        long gameId = 0;
        boolean isInsert = true;
        Cursor cursor = db.query(
                GameSeasonJoinedContract.TABLE_NAME,
                GameSeasonJoinedContract.ID_PROJECTION,
                GameSeasonJoinedContract.WHERE_GAME_AND_VARIANT_NAME,
                SelectionArgs.get(game.gameInfo.gameName, game.gameInfo.variantName),
                GameSeasonJoinedContract.GROUP_BY_GAME_ID,
                GameSeasonJoinedContract.HAVING_SEASON_COUNT_LESS_THAN_OR_EQUAL
                        + (1 + game.gameInfo.seasons), null
        );
        if (cursor.moveToFirst()) {
            gameId = cursor.getLong(0);
            isInsert = false;
        }
        cursor.close();

        ContentValues values = new ContentValues();
        values.put(GameContract.COLUMN_NAME, game.gameInfo.gameName);
        values.put(GameContract.COLUMN_VARIANT, game.gameInfo.variantName);
        values.put(GameContract.COLUMN_CREATED, getCurrentTimestamp());
        values.put(GameContract.COLUMN_MODIFIED, getCurrentTimestamp());

        if (isInsert) {
            gameId = db.insert(GameContract.TABLE_NAME, null, values);
        } else {
            String[] selectionArgs = SelectionArgs.get(gameId);
            db.update(GameContract.TABLE_NAME, values, GameContract.WHERE_ID, selectionArgs);
            db.delete(SeasonContract.TABLE_NAME, SeasonContract.WHERE_GAME_ID, selectionArgs);
        }
        game.gameInfo.gameId = gameId;

        if (pastSeasons != null) {
            for (int seasonId = 0; seasonId < game.gameInfo.seasons; seasonId++) {
                insertSeason(gameId, seasonId, pastSeasons.get(seasonId));
            }
        }

        CompressedSeason lastSeason = new CompressedSeason(game.getLastSeason(), game.variant);
        insertSeason(gameId, game.gameInfo.seasons, lastSeason);
    }

    public void deleteGame(long gameId) {
        db.delete(GameContract.TABLE_NAME, GameContract.COLUMN_ID + " = ?",
                SelectionArgs.get(gameId));
    }

    public GameInfo getGameInfo(long gameId) {
        Cursor cursor = db.query(GameSeasonJoinedContract.TABLE_NAME,
                GameSeasonJoinedContract.PROJECTION,
                GameSeasonJoinedContract.WHERE_GAME_ID, SelectionArgs.get(gameId),
                null, null, null);
        Assertions.checkState(cursor.moveToFirst());
        GameInfo gameInfo = new GameInfo();
        gameInfo.gameId = gameId;
        gameInfo.gameName = cursor.getString(1);
        gameInfo.variantName = cursor.getString(2);
        gameInfo.seasons = cursor.getInt(4);
        cursor.close();
        return gameInfo;
    }

    public CompressedSeason getLastSeason(Long gameId) {
        Cursor cursor = db.query(SeasonContract.TABLE_NAME,
                SeasonContract.PROJECTION,
                SeasonContract.WHERE_GAME_ID,
                SelectionArgs.get(gameId),
                null, null, SeasonContract.COLUMN_SEASON_ID + " DESC", "1");
        return getSeasonFromCursor(cursor);
    }

    public void loadGameMode(GameInfo gameInfo, List<Country> countries) {
        Cursor cursor = db.query(GameContract.TABLE_NAME,
                GameContract.MODE_PROJECTION,
                GameContract.WHERE_GAME_ID, SelectionArgs.get(gameInfo.gameId),
                null, null, null);
        Assertions.checkState(cursor.moveToFirst());

        gameInfo.mode = cursor.isNull(0) ? GameInfo.MODE_LOCAL : cursor.getInt(0);
        gameInfo.hotSeatCountry = getCountryFromCursor(countries, cursor, 1);
        addCountriesFromCursor(gameInfo.missingCountries, countries, cursor, 2);
        cursor.close();
    }

    void insertSeason(long gameId, long seasonId, CompressedSeason season) {
        ContentValues values = new ContentValues();
        values.put(SeasonContract.COLUMN_GAME_ID, gameId);
        values.put(SeasonContract.COLUMN_SEASON_ID, seasonId);
        values.put(SeasonContract.COLUMN_PHASE_AND_YEAR, 10 * season.year + season.phase.id);
        values.put(SeasonContract.COLUMN_RAW_DATA, season.data);
        db.insert(SeasonContract.TABLE_NAME, null, values);
    }

    // Game.SeasonStorage
    @Override
    public CompressedSeason getSeason(long gameId, long seasonId) {
        Cursor cursor = db.query(SeasonContract.TABLE_NAME,
                SeasonContract.PROJECTION,
                SeasonContract.WHERE_GAME_ID_AND_SEASON_ID,
                SelectionArgs.get(gameId, seasonId),
                null, null, null);
        return getSeasonFromCursor(cursor);
    }

    @Override
    public void storeResolution(long gameId, long resolvedSeasonId,
                                CompressedSeason resolvedSeason, CompressedSeason nextSeason) {
        updateSeason(gameId, resolvedSeasonId, resolvedSeason);
        insertSeason(gameId, resolvedSeasonId + 1, nextSeason);
        updateModifiedTimestamp(gameId);
    }

    @Override
    public void removeGameEnd(long gameId, long seasonIdToKeep) {
        db.delete(SeasonContract.TABLE_NAME, SeasonContract.COLUMN_GAME_ID + " = ? AND "
                        + SeasonContract.COLUMN_SEASON_ID + " > ?",
                SelectionArgs.get(gameId, seasonIdToKeep)
        );
        updateModifiedTimestamp(gameId);
    }

    @Override
    public void updateSeason(long gameId, long seasonId, CompressedSeason season) {
        ContentValues values = new ContentValues();
        values.put(SeasonContract.COLUMN_PHASE_AND_YEAR, 10 * season.year + season.phase.id);
        values.put(SeasonContract.COLUMN_RAW_DATA, season.data);
        db.update(SeasonContract.TABLE_NAME, values, SeasonContract.WHERE_GAME_ID_AND_SEASON_ID,
                SelectionArgs.get(gameId, seasonId));
        updateModifiedTimestamp(gameId);
    }

    @Override
    public void updateGameInfo(GameInfo gameInfo) {
        ContentValues values = new ContentValues();
        values.put(GameContract.COLUMN_MODE, gameInfo.mode);
        values.put(GameContract.COLUMN_HOT_SEAT_COUNTRY, countryToInt(gameInfo.hotSeatCountry));
        values.put(GameContract.COLUMN_MISSING_COUNTRIES, countriesToBitmap(gameInfo.missingCountries));
        db.update(GameContract.TABLE_NAME, values, GameContract.WHERE_GAME_ID,
                SelectionArgs.get(gameInfo.gameId));
    }

    private CompressedSeason getSeasonFromCursor(Cursor cursor) {
        Assertions.checkState(cursor.moveToFirst());
        int phaseAndYear = cursor.getInt(0);
        String data = cursor.getString(1);
        return new CompressedSeason(Phase.fromInt(phaseAndYear % 10), phaseAndYear / 10, data);
    }

    private void updateModifiedTimestamp(long gameId) {
        ContentValues values = new ContentValues();
        values.put(GameContract.COLUMN_MODIFIED, getCurrentTimestamp());
        db.update(GameContract.TABLE_NAME, values, GameContract.WHERE_ID,
                SelectionArgs.get(gameId));
    }

    private String getCurrentTimestamp() {
        return DATE_FORMAT.format(new Date());
    }

    private int countryToInt(Country country) {
        return country == null ? GameContract.NO_COUNTRY : country.id;
    }

    private Country getCountryFromCursor(List<Country> countries, Cursor cursor, int id) {
        if (cursor.isNull(id)) {
            return null;
        }
        int countryId = cursor.getInt(id);
        return (countryId == GameContract.NO_COUNTRY) ? null : countries.get(countryId);
    }

    private long countriesToBitmap(Set<Country> countries) {
        long result = 0;
        for (Country country : countries) {
            result |= 1L << country.id;
        }
        return result;
    }

    private void addCountriesFromCursor(Set<Country> result, List<Country> countries, Cursor cursor,
                                        int id) {
        if (cursor.isNull(id)) {
            return;
        }

        long countriesBitmap = cursor.getLong(id);
        for (int countryId = 0; countriesBitmap != 0; countryId++, countriesBitmap >>= 1) {
            if ((countriesBitmap & 1) == 1) {
                result.add(countries.get(countryId));
            }
        }
    }

    @SuppressWarnings("SameReturnValue")
    private static class SelectionArgs {
        private static final String[] one = new String[1];
        private static final String[] two = new String[2];

        private static String[] get(long l1) {
            one[0] = Long.toString(l1);
            return one;
        }

        private static String[] get(long l1, long l2) {
            two[0] = Long.toString(l1);
            two[1] = Long.toString(l2);
            return two;
        }

        private static String[] get(String s1, String s2) {
            two[0] = s1;
            two[1] = s2;
            return two;
        }
    }

}
