package com.realpolitik.rp.reader;

import android.content.res.AssetManager;
import android.graphics.Point;

import com.realpolitik.rp.common.RegionInfo;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Variant;

import java.io.IOException;
import java.text.ParseException;

class RgnFileParser extends SmartParser {

    private final Variant variant;

    public RgnFileParser(Variant variant, AssetManager assetManager, String assetUri)
            throws IOException {
        super(assetManager, assetUri);
        this.variant = variant;
    }

    public RegionInfo parse() throws IOException, ParseException {
        try {
            RegionInfo regionInfo = new RegionInfo(variant.colorMap.getWidth(),
                    variant.colorMap.getHeight());
            skipLine(); // ignore variant name
            while (!skipWhitespace().isEof()) {
                skipUntilChar(' '); // "# "
                String abbrev = getUntilChar(' ');
                Sector sector = variant.getSectorByAbbrev(abbrev);
                if (sector == null) {
                    throw new NoSectorException(abbrev);
                }
                Sector.Coast coast = null;
                if (peekChar() == '(') {
                    getChar(); // read the '('
                    coast = TokenParser.parseCoast(getChar());
                    skipLine();
                }

                skipLine(); // "# Unit position"
                int x = getInt();
                getChar(); // skip ','
                int y = getInt();
                Point position = new Point(x, y);
                if (coast != null) {
                    sector.setUnitPosition(coast, position);
                } else {
                    sector.setUnitPosition(Sector.Coast.ARMY, position);
                    sector.setUnitPosition(Sector.Coast.FLEET, position);
                }

                if (coast == null) { // coasts do not have a label position
                    skipLine(); // ignore "# Name position"
                    skipLine(); // ignore label position
                }

                skipLine(); // "# Region"
                int lines = getInt();
                while (lines-- > 0) {
                    if (coast != null) {
                        // coast lines are not of any interest
                        skipLine();
                        continue;
                    }

                    try {
                        regionInfo.addLine(getInt(), getInt(), getInt(), sector);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ParseException ex = new ParseException("Failed to parse Rgn",
                                getLineNumber());
                        ex.initCause(e);
                        throw ex;
                    }
                }
            }

            return regionInfo;
        } catch (ReadException e) {
            ParseException ex = new ParseException("Failed to parse Rgn: " + e.getMessage(),
                    getLineNumber());
            ex.initCause(e);
            throw ex;
        }
    }

}