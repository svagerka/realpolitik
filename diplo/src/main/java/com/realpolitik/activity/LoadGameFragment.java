package com.realpolitik.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.realpolitik.R;
import com.realpolitik.database.DatabaseContract;
import com.realpolitik.database.DatabaseController;
import com.realpolitik.database.DatabaseHelper;

public class LoadGameFragment extends Fragment
        implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener,
        DialogInterface.OnClickListener {
    private SimpleCursorAdapter adapter;
    private DatabaseController databaseController;
    private int currentPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DatabaseHelper databaseHelper = new DatabaseHelper(getActivity());
        databaseController = new DatabaseController(databaseHelper.getReadableDatabase());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view_fragment, container, false);
        String[] fromColumns = {DatabaseContract.GameContract.COLUMN_NAME,
                DatabaseContract.GameContract.COLUMN_VARIANT, "phaseYear",
                DatabaseContract.GameContract.COLUMN_MODIFIED};
        int[] toViews = {R.id.game_name, R.id.variant_name, R.id.season_and_year,
                R.id.last_modified};
        adapter = new SimpleCursorAdapter(getActivity(), R.layout.saved_game_item,
                null, fromColumns, toViews, 0);
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if (columnIndex == 5) {
                    int phaseYear = cursor.getInt(columnIndex);
                    ((TextView) view).setText(
                            getResources().getStringArray(R.array.phases)[phaseYear % 10 - 1]
                                    + " " + (phaseYear / 10)
                    );
                    return true;
                }
                return false;
            }
        });

        @SuppressWarnings("ConstantConditions")
        ListView variantView = (ListView) view.findViewById(R.id.list_view);
        variantView.setAdapter(adapter);
        variantView.setOnItemClickListener(this);
        variantView.setOnItemLongClickListener(this);
        return view;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        fetchData();
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Cursor cursor = adapter.getCursor();
        if (cursor == null) {
            return;
        }
        cursor.moveToPosition(position);
        openGame(cursor.getLong(0));
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = adapter.getCursor();
        if (cursor == null) {
            return false;
        }
        currentPosition = position;
        new AlertDialog.Builder(getActivity()).setTitle(R.string.game_action_title)
                .setItems(R.array.game_actions, this).show();
        return true;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int id) {
        final Cursor cursor = adapter.getCursor();
        if (cursor == null) {
            return;
        }
        cursor.moveToPosition(currentPosition);
        final long gameId = cursor.getLong(0);

        switch (id) {
            case 0: // open
                openGame(gameId);
                break;
            case 1: // rename
                final EditText input = new EditText(getActivity());
                input.setText(cursor.getString(1));
                input.selectAll();
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.game_action_rename_title)
                        .setMessage(R.string.game_action_rename_message)
                        .setView(input)
                        .setPositiveButton(R.string.game_action_rename_action,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        @SuppressWarnings("ConstantConditions")
                                        String newName = input.getText().toString();
                                        databaseController.renameGame(gameId, newName);
                                        fetchData();
                                    }
                                }
                        ).show();
                break;
            case 2:
                databaseController.deleteGame(gameId);
                fetchData();
                break;
        }
    }

    private void openGame(long gameId) {
        Intent intent = new Intent(getActivity(), GameActivity.class);
        intent.putExtra(GameActivity.EXTRA_SAVED_GAME_ID, gameId);
        startActivity(intent);
    }

    private void fetchData() {
        adapter.swapCursor(databaseController.listGames());
    }

}
