package com.realpolitik.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Season;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    public static interface Listener {
        boolean onTouchEvent(MotionEvent ev);

        void onSurfaceChanged(int width, int height);
    }

    private Listener listener;

    @SuppressWarnings("UnusedDeclaration")
    public GameView(Context context) {
        super(context);
        setup();
    }

    @SuppressWarnings("UnusedDeclaration")
    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    @SuppressWarnings("UnusedDeclaration")
    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup();
    }

    private void setup() {
        getHolder().addCallback(this);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void repaint(Game game, Sector selectedSector,
                        float scaleFactor, PointF pan) {
        if (!getHolder().getSurface().isValid()) return;

        if (game == null) return;

        Canvas canvas = null;
        try {
            canvas = getHolder().lockCanvas();
            canvas.drawColor(0, PorterDuff.Mode.CLEAR);
            canvas.translate(-pan.x, -pan.y);
            canvas.scale(scaleFactor, scaleFactor);
            MapRenderer.getInstance(getContext()).render(canvas, game, selectedSector);
        } finally {
            if (canvas != null) {
                getHolder().unlockCanvasAndPost(canvas);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return listener.onTouchEvent(ev);
    }

    // SurfaceHolder.Callback
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        listener.onSurfaceChanged(width, height);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

}
