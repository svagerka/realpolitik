package com.realpolitik.rp.season;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.util.Assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("UnusedReturnValue")
public class SeasonBuilder {
    private final Variant variant;
    private final Phase phase;
    private final int year;

    private final List<Dislodge> dislodges;
    private final int[] adjustmentCount;
    private final Country[] supplyCenters;
    private final List<Unit> units;
    private final Map<Sector, Order> orders;

    public SeasonBuilder(Variant variant, Phase phase, int year) {
        this.variant = variant;
        this.phase = phase;
        this.year = year;

        dislodges = new ArrayList<>();
        adjustmentCount = new int[variant.getCountries().size()];
        supplyCenters = new Country[variant.getSectors().size()];
        units = new ArrayList<>();
        orders = new HashMap<>();
    }

    // Building methods for units
    public SeasonBuilder addUnits(Unit[] units) {
        if (units != null) {
            for (Unit unit : units) {
                addUnit(unit);
            }
        }
        return this;
    }

    public SeasonBuilder addUnit(Unit unit) {
        units.add(Assertions.notNull(unit));
        if (getSeasonType() == Phase.PhaseType.MOVEMENT) {
            addOrder(new Order(unit, Order.Type.NO_MOVE_RECEIVED));
        }
        return this;
    }

    public SeasonBuilder addUnit(Unit.Type type, Location targetLocation, Country country) {
        return addUnit(new Unit(type, targetLocation, country));
    }

    public List<Unit> getAllUnits() {
        return units;
    }

    public SeasonBuilder removeUnit(Location location) {
        for (int i = 0; i < units.size(); ++i) {
            if (units.get(i).location.sector == location.sector) {
                units.remove(i);
                orders.remove(location.sector);
                break;
            }
        }
        return this;
    }

    // Building methods for orders
    SeasonBuilder addOrder(Order order) {
        orders.put(order.unit.location.sector, order);
        return this;
    }

    // Building methods for adjustment count
    public SeasonBuilder setAdjustmentCount(Country country, int count) {
        adjustmentCount[country.id] = count;
        return this;
    }

    public SeasonBuilder setAdjustmentCount(int[] adjustmentCount) {
        Assertions.checkArgument(adjustmentCount.length == variant.getCountries().size());
        System.arraycopy(adjustmentCount, 0, this.adjustmentCount, 0, adjustmentCount.length);
        return this;
    }

    // Building methods for retreats
    public SeasonBuilder addRetreat(Dislodge dislodge) {
        addOrder(new Order(dislodge.unit, Order.Type.NO_MOVE_RECEIVED));
        dislodges.add(dislodge);
        return this;
    }

    // Building methods for supply ownership
    public SeasonBuilder setSupplyOwnership(Sector sector, Country country) {
        Assertions.notNull(sector);
        Assertions.notNull(country);
        supplyCenters[sector.id] = country;
        return this;
    }

    public SeasonBuilder setSupplyOwnership(Country[] supplyOwnership) {
        Assertions.checkArgument(supplyOwnership.length == supplyCenters.length);
        System.arraycopy(supplyOwnership, 0, supplyCenters, 0, supplyCenters.length);
        return this;
    }

    // Getters
    public Season build() {
        switch (getSeasonType()) {
            case MOVEMENT:
                return new MovementSeason(variant, phase, year, adjustmentCount,
                        supplyCenters,
                        units.toArray(new Unit[units.size()]), orders);
            case RETREAT:
                return new RetreatSeason(variant, phase, year, dislodges, adjustmentCount,
                        supplyCenters,
                        units.toArray(new Unit[units.size()]), orders);
            case ADJUSTMENT:
                return new AdjustmentSeason(variant, phase, year, adjustmentCount,
                        supplyCenters,
                        units.toArray(new Unit[units.size()]), orders);
            default:
                throw new IllegalStateException();
        }
    }

    public int getOrderCount() {
        switch (getSeasonType()) {
            case MOVEMENT:
                return units.size();
            case RETREAT:
                return dislodges.size();
            case ADJUSTMENT:
                int sum = 0;
                for (int i : adjustmentCount) {
                    sum += Math.abs(i);
                }
                return sum;
            default:
                throw new IllegalStateException();
        }
    }

    public Phase.PhaseType getSeasonType() {
        return Phase.PhaseType.fromSeason(phase);
    }

}
