package com.realpolitik.rp.reader;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.realpolitik.R;
import com.realpolitik.rp.common.CompressedSeason;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Season;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileParser {
    private final AssetManager assetManager;
    private final Resources resources;

    public FileParser(AssetManager assetManager, Resources resources) {
        this.assetManager = assetManager;
        this.resources = resources;
    }

    public Variant parseVariantFile(String variantName) throws IOException, ParseException {
        String variantDirectory = getVariantDirectory(variantName);
        String variantFileName = String.format("%s.var", variantDirectory);
        LineNumberReader reader = getAssetReader(getAssetUri(variantFileName, variantDirectory));

        // parse into key value pairs
        Map<String, String> variantData = new HashMap<>();
        String line;
        while (true) {
            line = reader.readLine();
            if (line == null) break;
            line = line.trim();
            if (line.equals("")) break;

            String[] tokens = line.trim().split("[: \t]");
            if (tokens.length < 2) {
                throw new ParseException(String.format("Malformed value description: %s", line),
                        reader.getLineNumber());
            }
            variantData.put(tokens[0], tokens[tokens.length - 1]);
        }

        Variant variant = new Variant(variantData.get("Build"));
        variant.colorMap = getAssetBitmap(variantData, "ColorMap", variantDirectory);
        variant.info = parseInfo(getAssetReader(variantData, "Info", variantDirectory));

        new MapFileParser(variant, assetManager,
                getAssetUri(variantData, "MapData", variantDirectory)).parse();
        CntFileParser.parse(variant, getAssetReader(variantData, "Countries", variantDirectory));
        GamFileParser.parse(variant, getAssetReader(variantData, "Game", variantDirectory));
        variant.regionInfo = new RgnFileParser(variant, assetManager,
                getAssetUri(variantData, "Regions", variantDirectory)).parse();

        return variant;
    }

    private String parseInfo(LineNumberReader reader) {
        try {
            StringBuilder builder = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                builder.append(line).append('\n');
                line = reader.readLine();
            }
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getVariantDirectory(String variantName) {
        String[] variants = resources.getStringArray(R.array.internalVariantNames);
        for (int i = 0; i < variants.length; ++i) {
            if (variants[i].equals(variantName)) {
                return resources.getStringArray(R.array.variantFolders)[i];
            }
        }
        throw new IllegalArgumentException("Variant " + variantName + " does not exist");
    }

    public Game parseGameFile(LineNumberReader reader) throws IOException, ParseException, ReadException {
        GameInfo gameInfo = GamFileParser.parseGameInfo(reader);
        Variant variant = parseVariantFile(gameInfo.variantName);
        Season lastSeason = GamFileParser.parseSeasonInfo(reader, variant, false);

        reader.readLine(); // weird line, contains v2?
        gameInfo.seasons = Integer.parseInt(reader.readLine());
        return new Game(gameInfo, variant, lastSeason);
    }

    public List<CompressedSeason> parsePastSeasons(LineNumberReader reader, int count) throws IOException, ParseException {
        List<CompressedSeason> pastSeasons = new ArrayList<>(count);
        while (count-- > 0) {
            pastSeasons.add(DpyFileParser.parseCompressedSeason(reader));
        }
        return pastSeasons;
    }

    private String getAssetUri(Map<String, String> variantData, String assetName,
                               String variantName) throws ParseException {
        String assetRelativeUri = variantData.get(assetName);
        if (assetRelativeUri == null) {
            throw new ParseException(String.format("Did not find asset %s", assetName), 0);
        }

        return getAssetUri(assetRelativeUri, variantName);
    }

    private LineNumberReader getAssetReader(Map<String, String> variantData, String assetName,
                                            String variantName) throws IOException, ParseException {
        return getAssetReader(getAssetUri(variantData, assetName, variantName));
    }

    private LineNumberReader getAssetReader(String assetAbsoluteUri) throws IOException {
        return new LineNumberReader(new InputStreamReader(assetManager.open(assetAbsoluteUri)));
    }

    private String getAssetUri(String fileName, String variantName) {
        String directory = variantName;

        // if the filename contains path, then it uses files from another variant
        String[] tokens = fileName.split("/");
        if (tokens.length == 2) {
            directory = tokens[0];
            fileName = tokens[1];
        }

        return String.format("variants/%s/%s", directory, fileName);
    }

    private Bitmap getAssetBitmap(Map<String, String> variantData, String bitmapName,
                                  String variantName) throws IOException, ParseException {
        String assetRelativeUri = variantData.get(bitmapName);
        if (assetRelativeUri == null) {
            throw new ParseException(String.format("Did not find bitmap %s", bitmapName), 0);
        }

        String assetAbsoluteUri = getAssetUri(assetRelativeUri, variantName);
        return BitmapFactory.decodeStream(assetManager.open(assetAbsoluteUri));
    }

}
