package com.realpolitik.rp.reader.orders;

import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.TokenParser;

import java.util.regex.Matcher;

public class MovementOrderParser extends OrderParser {
    public MovementOrderParser(Variant variant) {
        super(variant, new OrderMatcher[]{new EmptyMatcher(), new SupportHoldMatcher(),
                new SupportMoveMatcher(), new ConvoyMatcher(), new HoldMatcher(),
                new MoveMatcher(), new NoMoveReceivedMatcher()});
    }

    static class ConvoyMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return UNIT_REGEXP + "\\s+Convoys\\s+" + MOVE_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.CONVOY,
                    parseUnit(variant, null, matcher, MATCHER_FIRST_ID),
                    parseLocation(variant, matcher,
                            MATCHER_FIRST_ID + MATCHER_SECTOR_GROUPS + MATCHER_UNIT_GROUPS,
                            Sector.Coast.ARMY)
            );
        }
    }

    static class HoldMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return UNIT_REGEXP + "\\s+Hold";
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.HOLD);
        }
    }

    static class MoveMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return MOVE_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            Location location =
                    parseLocation(variant, matcher, MATCHER_FIRST_ID, unit.type.getDefaultCoast());
            return new Order(unit, Order.Type.MOVE, location);
        }
    }

    static class SupportHoldMatcher extends OrderMatcher {

        @Override
        protected String getRegexp() {
            return UNIT_REGEXP + "\\s+Supports\\s+" + UNIT_REGEXP + "(\\s+Hold)?";
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.SUPPORT_HOLD,
                    parseUnit(variant, null, matcher, MATCHER_FIRST_ID));
        }
    }

    static class SupportMoveMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return UNIT_REGEXP + "\\s+Supports\\s+" + MOVE_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            Unit.Type type = TokenParser.parseUnitType(matcher.group(MATCHER_FIRST_ID));
            return new Order(unit, Order.Type.SUPPORT_MOVE,
                    parseUnit(variant, null, matcher, MATCHER_FIRST_ID),
                    parseLocation(variant, matcher,
                            MATCHER_FIRST_ID + MATCHER_UNIT_GROUPS + MATCHER_SECTOR_GROUPS,
                            type.getDefaultCoast())
            );
        }
    }
}
