package com.realpolitik.rp.common;

import com.realpolitik.util.Assertions;

public class Unit implements Comparable<Unit> {

    public enum Type {
        ARMY,
        FLEET;

        public Sector.Coast getDefaultCoast() {
            return this == ARMY ? Sector.Coast.ARMY : Sector.Coast.FLEET;
        }
    }

    public final Type type;
    public final Location location;
    public final Country country;

    public Unit(Type type, Location location, Country country) {
        this.type = Assertions.notNull(type);
        this.country = country;
        this.location = Assertions.notNull(location);
    }

    public int getX() {
        return location.getX();
    }

    public int getY() {
        return location.getY();
    }

    @Override
    public int compareTo(Unit unit) {
        return country != unit.country
                ? country.compareTo(unit.country)
                : location.sector.compareTo(unit.location.sector);
    }

}
