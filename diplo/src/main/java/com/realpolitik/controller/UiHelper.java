package com.realpolitik.controller;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.util.LongSparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.realpolitik.R;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;

import java.util.Timer;
import java.util.TimerTask;

public class UiHelper implements DialogInterface.OnClickListener,
        DialogInterface.OnCancelListener {

    private static final long TOAST_DURATION_MILLIS = 3000;

    public interface OnDialogResult {
        void cancelled();

        void itemClicked(int which);
    }

    private final Activity activity;
    private final View loadingPanel;
    private final TextView toastView;
    private OnDialogResult listener;
    private final LongSparseArray<String> displayedToasts;
    private final Timer timer;
    private final Runnable updateToastRunnable;

    public UiHelper(final Activity activity) {
        this.activity = activity;
        loadingPanel = activity.findViewById(R.id.loadingPanel);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            prepareClipboardManager(activity);

        toastView = (TextView) activity.findViewById(R.id.toast);
        displayedToasts = new LongSparseArray<>();
        timer = new Timer();
        updateToastRunnable = new Runnable() {
            @Override
            public void run() {
                updateToast();
            }
        };
    }

    public void setLoading(boolean loading) {
        loadingPanel.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    public void displayError(int errorResId) {
        showToast(activity.getString(errorResId));
        toastView.performHapticFeedback(0);
    }

    public void showToast(String message) {
        long endTime = SystemClock.elapsedRealtime() + TOAST_DURATION_MILLIS;
        String previousToast = displayedToasts.get(endTime);
        if (previousToast != null) {
            message = (previousToast + "\n" + message);
        } else {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    activity.runOnUiThread(updateToastRunnable);
                }
            }, TOAST_DURATION_MILLIS);
        }
        displayedToasts.put(endTime, message);
        updateToast();
    }

    public void displayError(int errorId, Exception exception, OnDialogResult listener) {
        this.listener = listener;
        new AlertDialog.Builder(activity)
                .setTitle(errorId)
                .setMessage(exception.getMessage())
                .setOnCancelListener(this).show();
    }

    public void displayPicker(int titleId, int[] options, OnDialogResult listener) {
        this.listener = listener;

        String[] stringOptions = new String[options.length];
        for (int i = 0; i < options.length; ++i) {
            stringOptions[i] = activity.getString(options[i]);
        }

        new AlertDialog.Builder(activity).setTitle(titleId)
                .setItems(stringOptions, this)
                .setOnCancelListener(this).show();
    }

    public void prepareMenu(Menu menu, Game game) {
        setMenuItemEnabled(menu, R.id.action_previous,
                game != null && !game.isCurrentSeasonFirst(), true);
        setMenuItemEnabled(menu, R.id.action_next,
                game != null && !game.isCurrentSeasonLast(), true);
        setMenuItemEnabled(menu, R.id.action_paste, true, game != null && game.isCurrentSeasonLast()
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && hasClipData());
        setMenuItemEnabled(menu, R.id.action_resolve,
                true, game != null && game.isCurrentSeasonLast()
                        && !game.isHotSeatWithMissingCountries());
        setMenuItemEnabled(menu, R.id.action_advance_to_next_country,
                true, game != null && game.isHotSeatWithMissingCountries());
        setMenuItemEnabled(menu, R.id.action_edit,
                true, game != null && game.gameInfo.mode == GameInfo.MODE_LOCAL
                        && !game.isCurrentSeasonLast());
        setMenuItemEnabled(menu, R.id.action_variant_info,
                true, game != null && game.variant.info != null);
    }

    private void setMenuItemEnabled(Menu menu, int menuItemId, boolean enabled, boolean visible) {
        MenuItem menuItem = menu.findItem(menuItemId);
        if (menuItem != null) {
            menuItem.setVisible(visible);
            menuItem.setEnabled(enabled);
            Drawable icon = menuItem.getIcon();
            if (icon != null) {
                icon.setAlpha(enabled ? 255 : 130);
            }
        }
    }

    private void updateToast() {
        while (displayedToasts.size() > 0
                && SystemClock.elapsedRealtime() >= displayedToasts.keyAt(0)) {
            displayedToasts.removeAt(0);
        }

        if (displayedToasts.size() == 0) {
            toastView.setVisibility(View.GONE);
            return;
        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < displayedToasts.size(); ++i) {
            if (i != 0) {
                builder.append('\n');
            }
            builder.append(displayedToasts.valueAt(i));
        }

        toastView.setText(builder.toString());
        toastView.setVisibility(View.VISIBLE);

        while (displayedToasts.size() > 0
                && SystemClock.elapsedRealtime() >= displayedToasts.keyAt(0)) {
            displayedToasts.removeAt(0);
        }
    }

    @TargetApi(11)
    private void prepareClipboardManager(final Activity activity) {
        ClipboardManager clipboardManager =
                (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.addPrimaryClipChangedListener(
                new ClipboardManager.OnPrimaryClipChangedListener() {
                    @Override
                    public void onPrimaryClipChanged() {
                        activity.invalidateOptionsMenu();
                    }
                }
        );
    }

    @SuppressWarnings("ConstantConditions")
    @TargetApi(11)
    private boolean hasClipData() {
        ClipboardManager clipboardManager =
                (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        return clipboardManager.hasPrimaryClip();
    }

    // DialogInterface.OnClickListener
    @Override
    public void onClick(DialogInterface dialogInterface, int which) {
        if (listener != null) {
            listener.itemClicked(which);
        }
    }

    // DialogInterface.OnCancelListener
    @Override
    public void onCancel(DialogInterface dialogInterface) {
        if (listener != null) {
            listener.cancelled();
        }
    }

}
