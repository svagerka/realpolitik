package com.realpolitik.database;

public abstract class DatabaseContract {
    public abstract static class GameContract {
        public static final String TABLE_NAME = "Games";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_VARIANT = "variant";
        public static final String COLUMN_CREATED = "created";
        public static final String COLUMN_MODIFIED = "modified";
        public static final String COLUMN_MODE = "mode";

        /**
         * Id of the GameInfo.hotSeatCountry or -1 if null.
         */
        public static final String COLUMN_HOT_SEAT_COUNTRY = "hotseat_country";
        /**
         * Sum of 1 << country.id for all countries in the set of missing countries.
         */
        public static final String COLUMN_MISSING_COUNTRIES = "missing_countries";

        public static final int NO_COUNTRY = -1;

        public static final String WHERE_ID = GameContract.COLUMN_ID + " = ?";
        public static final String[] MODE_PROJECTION =
                new String[]{COLUMN_MODE, COLUMN_HOT_SEAT_COUNTRY, COLUMN_MISSING_COUNTRIES};
        public static final String WHERE_GAME_ID = COLUMN_ID + " = ?";
    }

    public abstract static class SeasonContract {
        public static final String TABLE_NAME = "Seasons";
        public static final String COLUMN_GAME_ID = "gameId";
        public static final String COLUMN_SEASON_ID = "seasonId";
        public static final String COLUMN_PHASE_AND_YEAR = "phaseYear";
        public static final String COLUMN_RAW_DATA = "rawData";

        public static final String[] PROJECTION =
                new String[]{SeasonContract.COLUMN_PHASE_AND_YEAR, SeasonContract.COLUMN_RAW_DATA};

        public static final String WHERE_GAME_ID =
                COLUMN_GAME_ID + " = ?";
        public static final String WHERE_GAME_ID_AND_SEASON_ID =
                COLUMN_GAME_ID + " = ? AND " + COLUMN_SEASON_ID + " = ?";
    }

    public abstract static class GameSeasonJoinedContract {
        public static final String TABLE_NAME =
                GameContract.TABLE_NAME + " LEFT JOIN " + SeasonContract.TABLE_NAME
                        + " ON " + GameContract.COLUMN_ID + " = " + SeasonContract.COLUMN_GAME_ID;

        public static final String[] PROJECTION = new String[]{
                GameContract.COLUMN_ID + " _id", GameContract.COLUMN_NAME,
                GameContract.COLUMN_VARIANT, GameContract.COLUMN_MODIFIED,
                "MAX(" + SeasonContract.TABLE_NAME + "." + SeasonContract.COLUMN_SEASON_ID + ")",
                "MAX(" + SeasonContract.TABLE_NAME + "."
                        + SeasonContract.COLUMN_PHASE_AND_YEAR + ") AS phaseYear"
        };

        public static final String[] ID_PROJECTION = new String[]{GameContract.COLUMN_ID};

        public static final String WHERE_GAME_ID = GameContract.COLUMN_ID + " = ?";
        public static final String WHERE_GAME_AND_VARIANT_NAME =
                GameContract.COLUMN_NAME + " = ? AND " + GameContract.COLUMN_VARIANT + " = ?";

        public static final String GROUP_BY_GAME_ID =
                GameContract.TABLE_NAME + "." + GameContract.COLUMN_ID;

        public static final String HAVING_SEASON_COUNT_LESS_THAN_OR_EQUAL =
                "COUNT(" + SeasonContract.TABLE_NAME + "." + SeasonContract.COLUMN_SEASON_ID
                        + ") <= ";
    }
}
