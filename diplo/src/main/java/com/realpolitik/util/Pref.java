package com.realpolitik.util;

abstract public class Pref {
    // Settable preferences
    public static final String KEY_FORBID_IMPOSSIBLE_MOVES = "forbidImpossibleMoves";
    public static final boolean DEFAULT_FORBID_IMPOSSIBLE_MOVES = false;
    public static final String KEY_AUTO_ORDER_TYPE = "autoOrderType";
    public static final boolean DEFAULT_AUTO_ORDER_TYPE = false;
    public static final CharSequence KEY_VERSION = "version";

    // State preferences
    public static final String LAST_OPENED_PATH = "last_opened_path";
    public static final String ACCEPTED_HOT_SEAT_INTRODUCTION = "accepted_hot_seat_description";
}
