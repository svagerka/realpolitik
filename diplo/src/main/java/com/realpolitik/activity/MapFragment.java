package com.realpolitik.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;

import com.realpolitik.R;
import com.realpolitik.controller.OrderInput;
import com.realpolitik.controller.PanZoomHelper;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.writer.OrderWriter;
import com.realpolitik.ui.GameView;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

public class MapFragment extends GameFragment
        implements GameView.Listener, GestureDetector.OnGestureListener, PanZoomHelper.Listener,
        OrderInput.Listener {

    private PanZoomHelper panZoomHelper;
    private OrderInput orderInput;
    private GameView gameView;

    private ScaleGestureDetector scaleDetector;
    private GestureDetector gestureDetector;
    private SharedPreferences preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.panZoomHelper = new PanZoomHelper(this);
        this.preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        this.orderInput = new OrderInput(preferences, this);
        preferences.registerOnSharedPreferenceChangeListener(orderInput);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        preferences.unregisterOnSharedPreferenceChangeListener(orderInput);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);
        gameView = (GameView) view.findViewById(R.id.game_view);
        gameView.setListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        scaleDetector = new ScaleGestureDetector(activity, new ScaleListener());
        gestureDetector = new GestureDetector(activity, this);
    }

    @Override
    protected void performUpdate() {
        panZoomHelper.setMapSize(getGame().variant.colorMap.getWidth(),
                getGame().variant.colorMap.getHeight());
        orderInput.setGame(getGame());
        orderInput.setCurrentSeason(getGame().isCurrentSeasonLast()
                ? getGame().getCurrentSeason() : null);
        performPaint();
    }

    private void performPaint() {
        if (getGame() != null) {
            gameView.repaint(getGame(),
                    orderInput.getSelectedSector(),
                    panZoomHelper.getZoom(), panZoomHelper.getPan());
        }
    }

    private Sector getSector(float viewX, float viewY) {
        if (getGame() != null) {
            return getGame().variant.regionInfo.getSector(panZoomHelper.convertToMapCoords(
                    new PointF(viewX, viewY)));
        } else {
            return null;
        }
    }

    // OrderInput.Listener
    @Override
    public void onSelectedSectorChanged() {
        performPaint();
    }

    @Override
    public void onInputError(int errorDescriptionResourceId) {
        getUiHelper().displayError(errorDescriptionResourceId);
    }

    @Override
    public void onMultipleChoices(int dialogTitleResourceId, int[] dialogOptionsResourceIds) {
        getUiHelper().displayPicker(dialogTitleResourceId, dialogOptionsResourceIds, orderInput);
    }

    @Override
    public void onOrder(Order order) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream);
        OrderWriter.writeOrder(writer, order, OrderWriter.FLAGS_DEFAULT);
        writer.flush();
        getUiHelper().showToast(stream.toString());
        writer.close();

        performFullUpdate();
    }

    // PanZoomHelper.Listener
    @Override
    public void onPanZoomChanged(float scale, PointF pan) {
        performPaint();
    }

    // ScaleListener
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return panZoomHelper.scale(detector.getScaleFactor(),
                    detector.getFocusX(), detector.getFocusY());
        }
    }

    // GestureDetector.OnGestureListener
    @Override
    public boolean onDown(MotionEvent e) {
        orderInput.startSectorClick(getSector(e.getX(), e.getY()));
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        orderInput.cancelSectorClick();
        return panZoomHelper.pan(distanceX, distanceY);
    }

    @Override
    public void onLongPress(MotionEvent e) {
        orderInput.longPress();
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    // GameView.Listener
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if ((ev.getAction() & MotionEvent.ACTION_UP) == MotionEvent.ACTION_UP) {
            orderInput.finishSectorClick(getSector(ev.getX(), ev.getY()));
        }

        boolean result = scaleDetector.onTouchEvent(ev);
        if (!scaleDetector.isInProgress()) {
            result |= gestureDetector.onTouchEvent(ev);
        }
        return result;
    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        panZoomHelper.setViewSize(width, height);
        performPaint();
    }
}
