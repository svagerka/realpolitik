package com.realpolitik.rp.reader;

import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.test.DiploTest;
import com.realpolitik.test.RobolectricGradleTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(RobolectricGradleTestRunner.class)
public class TokenParserTest extends DiploTest {

    public TokenParserTest() throws ReadException {
    }

    @Test
    public void testParseCoast() {
        assertEquals(Sector.Coast.SOUTH, TokenParser.parseCoast('s'));
        assertEquals(Sector.Coast.NORTH, TokenParser.parseCoast('n'));
        assertEquals(Sector.Coast.EAST, TokenParser.parseCoast('e'));
        assertEquals(Sector.Coast.WEST, TokenParser.parseCoast('w'));
        assertEquals(null, TokenParser.parseCoast('a'));
        assertEquals(Sector.Coast.NORTH, TokenParser.parseCoast("nc"));
        assertEquals(Sector.Coast.ARMY, TokenParser.parseCoast("mv"));
        assertEquals(Sector.Coast.FLEET, TokenParser.parseCoast("XC"));
    }

    @Test
    public void testParseUnitType() {
        assertEquals(Unit.Type.ARMY, TokenParser.parseUnitType("A"));
        assertEquals(Unit.Type.FLEET, TokenParser.parseUnitType("F"));
    }

    @Test
    public void testParseLocation() throws ReadException {
        assertLocationEquals("AA", Sector.Coast.ARMY,
                TokenParser.parseLocation(variant, "axa", Sector.Coast.ARMY));
        assertLocationEquals("BC", Sector.Coast.FLEET,
                TokenParser.parseLocation(variant, "bxc", Sector.Coast.FLEET));
        assertLocationEquals("DC Bicoastal", Sector.Coast.EAST,
                TokenParser.parseLocation(variant, "dxc/ec", Sector.Coast.FLEET));
        assertLocationEquals("DC Bicoastal", Sector.Coast.WEST,
                TokenParser.parseLocation(variant, "dxc(wc)", Sector.Coast.FLEET));
        assertLocationEquals("EE Island", Sector.Coast.FLEET,
                TokenParser.parseLocation(variant, "EXE", Sector.Coast.FLEET));
        //noinspection EmptyCatchBlock
        try {
            TokenParser.parseLocation(variant, "foo(WC)", Sector.Coast.FLEET);
            fail("Returned some sector for foo");
        } catch (NoSectorException e) {

        }

    }


}