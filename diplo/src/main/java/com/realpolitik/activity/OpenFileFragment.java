package com.realpolitik.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.realpolitik.R;
import com.realpolitik.ui.FileList;
import com.realpolitik.util.Pref;

import java.io.File;
import java.util.regex.Pattern;

public class OpenFileFragment extends Fragment implements FileList.OnPathChangedListener, FileList.OnFileSelectedListener {

    private SharedPreferences sharedPreferences;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
    }

    @Override
    public void onDetach() {
        sharedPreferences = null;
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String path = sharedPreferences.getString(Pref.LAST_OPENED_PATH, "/");

        View view = inflater.inflate(R.layout.open_file_fragment, container, false);
        FileList fileList = (FileList) view.findViewById(R.id.open_file_layout);
        fileList.setFileNamePattern(Pattern.compile("^.*\\.dpy$"));
        fileList.setPath(path);
        fileList.setOnFileSelected(this);
        fileList.setOnPathChangedListener(this);
        return view;
    }

    @Override
    public void onChanged(String path) {
        sharedPreferences.edit().putString(Pref.LAST_OPENED_PATH, path).apply();
    }

    @Override
    public void onSelected(String path, String fileName) {
        File file = new File(path + "/" + fileName);

        Intent intent = new Intent(getActivity(), GameActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), "application/octet-stream");
        startActivity(intent);
    }
}
