package com.realpolitik.rp.common;

import com.realpolitik.util.Assertions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Country implements Comparable<Country> {
    public final int id;
    public final String name;
    public final String adjective;
    public final char initial;
    public final Color color;
    private final List<Sector> homeCenters;

    public Country(int id, String name, String adjective, char initial, Color color) {
        this.id = id;
        this.name = Assertions.notNull(name);
        this.adjective = Assertions.notNull(adjective);
        this.initial = initial;
        this.color = Assertions.notNull(color);
        this.homeCenters = new ArrayList<>();
    }

    public void addHomeCenter(Sector sector) {
        Assertions.notNull(sector);
        homeCenters.add(sector);
    }

    public boolean isHomeCenter(Sector sector) {
        Assertions.notNull(sector);
        return homeCenters.contains(sector);
    }

    public List<Sector> getHomeCenters() {
        return Collections.unmodifiableList(homeCenters);
    }

    @Override
    public int compareTo(Country country) {
        return id - country.id;
    }

    @SuppressWarnings("UnusedDeclaration")
    public enum Color {
        BLACK(0x000000, "Black"),
        BLUE(0x0000ff, "Blue"),
        BROWN(0x663300, "Brown"),
        CRIMSON(0x800000, "Crimson"),
        CYAN(0x00ffff, "Cyan"),
        FOREST(0x008000, "Forest"),
        GREEN(0x00ff00, "Green"),
        CHARCOAL(0x808080, "Charcoal"),
        MAGENTA(0xff00ff, "Magenta"),
        NAVY(0x000080, "Navy"),
        OLIVE(0x808000, "Olive"),
        ORANGE(0xff9900, "Orange"),
        PURPLE(0x800080, "Purple"),
        RED(0xff0000, "Red"),
        TAN(0xcc9966, "Tan"),
        TEAL(0x008080, "Teal"),
        WHITE(0xffffff, "White"),
        YELLOW(0xffff00, "Yellow");

        public final int color;
        public final String name;

        private Color(int color, String name) {
            this.color = color;
            this.name = name;
        }

    }

    public static Color getColor(String name) {
        for (Color color : Color.class.getEnumConstants()) {
            if (color.name.equals(name)) {
                return color;
            }
        }
        return null;
    }

}

