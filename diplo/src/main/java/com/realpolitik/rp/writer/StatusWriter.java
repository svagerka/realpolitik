package com.realpolitik.rp.writer;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.season.Season;

import java.util.List;

public class StatusWriter {
    private static final int COUNTRY_NAME_PADDING = 3;
    private static final int SUPPLIES_WIDTH = 3;
    private static final int UNITS_WIDTH = 3;
    private static final int BUILD_WIDTH = 3;


    public String write(Game game, Season season) {
        StringBuilder builder = new StringBuilder();
        builder.append("Ownership:\n");

        Country[] supplyCenters = season.getSupplyCenters();
        List<Country> countries = game.variant.getCountries();
        List<Sector> sectors = game.variant.getSectors();

        int longestCountryName = 0;
        for (Country country : countries) {
            longestCountryName = Math.max(longestCountryName, country.name.length());
        }

        for (Country country : countries) {
            boolean firstSector = true;
            for (Sector sector : sectors) {
                if (supplyCenters[sector.id] != country) {
                    continue;
                }

                if (firstSector) {
                    firstSector = false;
                    appendPaddedCountry(builder, longestCountryName, country);
                } else {
                    builder.append(", ");
                }

                builder.append(sector.name);
            }
            if (!firstSector) {
                builder.append(".\n");
            }
        }

        builder.append("\nAdjustments:\n\n");

        for (Country country : countries) {
            appendPaddedCountry(builder, longestCountryName, country);
            builder.append("Supp");
            appendPaddedInt(builder, SUPPLIES_WIDTH, season.getSupplyCenterCount(country));
            builder.append("Unit");
            appendPaddedInt(builder, UNITS_WIDTH, season.getUnitCount(country));
            builder.append(season.getAdjustmentCount(country) >= 0 ? "Build" : "Remove");
            appendPaddedInt(builder, BUILD_WIDTH, Math.abs(season.getAdjustmentCount(country)));
            builder.append('\n');
        }

        return builder.toString();
    }


    private void appendPaddedCountry(StringBuilder builder, int width, Country country) {
        builder.append(country.name);
        builder.append(':');
        appendSpaces(builder, width + COUNTRY_NAME_PADDING - country.name.length());
    }

    private void appendPaddedInt(StringBuilder builder, int width, int value) {
        if (value >= 10) {
            appendSpaces(builder, width - 2);
        } else {
            appendSpaces(builder, width - 1);
        }
        builder.append(value);
        builder.append(' ');
    }

    private void appendSpaces(StringBuilder builder, int count) {
        for (int i = 0; i < count; ++i) {
            builder.append(' ');
        }
    }
}
