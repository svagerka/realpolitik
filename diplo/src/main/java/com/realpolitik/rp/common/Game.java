package com.realpolitik.rp.common;

import android.util.Log;

import com.realpolitik.rp.reader.DpyFileParser;
import com.realpolitik.rp.season.Season;
import com.realpolitik.util.Assertions;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.text.ParseException;
import java.util.Iterator;

public class Game {

    public interface SeasonStorage {
        CompressedSeason getSeason(long gameId, long seasonId);

        void storeResolution(long gameId, long resolvedSeasonId, CompressedSeason resolvedSeason,
                             CompressedSeason nextSeason);

        void removeGameEnd(long gameId, long seasonIdToKeep);

        void updateSeason(long gameId, long seasonId, CompressedSeason season);

        void updateGameInfo(GameInfo gameInfo);
    }

    public final GameInfo gameInfo;
    public final Variant variant;

    private Season lastSeason;

    private int currentSeasonId;
    private Season currentSeason;
    private SeasonStorage seasonStorage;

    public Game(GameInfo gameInfo, Variant variant, Season lastSeason) {
        this.variant = Assertions.notNull(variant);
        this.gameInfo = Assertions.notNull(gameInfo);

        this.lastSeason = lastSeason;
        this.currentSeason = lastSeason;
        this.currentSeasonId = gameInfo.seasons;
    }

    public Game(Variant variant) {
        this(variant.gameInfo, variant, variant.startingSeason);
        updateHotSeatInfo();
    }

    public void setSeasonStorage(SeasonStorage seasonStorage) {
        this.seasonStorage = seasonStorage;
    }

    public Season getLastSeason() {
        return lastSeason;
    }

    public Season getCurrentSeason() {
        return currentSeason;
    }

    public Season getPreviousSeason() {
        if (isCurrentSeasonFirst()) {
            Log.e("GAME", "Could not get previous season, as this is the first one.");
            return null;
        }

        try {
            return getSeason(currentSeasonId - 1);
        } catch (IOException | ParseException e) {
            Log.e("GAME", "Exception while reading from string " + e.getMessage());
            return null;
        }
    }

    public boolean isCurrentSeasonLast() {
        return currentSeasonId == gameInfo.seasons;
    }

    public boolean isCurrentSeasonFirst() {
        return currentSeasonId == 0;
    }

    public void moveToNext() throws ParseException {
        if (currentSeasonId != gameInfo.seasons) {
            moveToSeason(currentSeasonId + 1);
        }
    }

    public void moveToPrevious() throws ParseException {
        if (currentSeasonId != 0) {
            moveToSeason(currentSeasonId - 1);
        }
    }

    private void moveToSeason(int id) throws ParseException {
        if (id >= gameInfo.seasons) {
            currentSeasonId = gameInfo.seasons;
            currentSeason = lastSeason;
        } else {
            currentSeasonId = id;
            try {
                currentSeason = getSeason(id);
                currentSeasonId = id;
            } catch (IOException e) {
                Log.e("GAME", "I/O exception while reading from string " + e.getMessage());
            }
        }
    }

    private Season getSeason(int id) throws ParseException, IOException {
        CompressedSeason season = seasonStorage.getSeason(gameInfo.gameId, id);
        return DpyFileParser.parseSeasonInfo(
                new LineNumberReader(new StringReader(season.data)),
                variant);
    }

    /**
     * Returns true if this is a hot seat game and some of the countries are still missing orders.
     */
    public boolean isHotSeatWithMissingCountries() {
        return isCurrentSeasonLast()
                && gameInfo.mode == GameInfo.MODE_HOT_SEAT
                && !gameInfo.missingCountries.isEmpty();
    }

    /**
     * In hot-seat mode, marks the country as correctly input and proceeds to the next one.
     */
    public void advanceToNextCountry() {
        Assertions.checkState(isHotSeatWithMissingCountries());
        gameInfo.hotSeatCountry = null;
    }

    public void resolve() {
        Assertions.checkState(isCurrentSeasonLast() && !isHotSeatWithMissingCountries());

        Season nextSeason = getLastSeason().resolve();
        seasonStorage.storeResolution(gameInfo.gameId, gameInfo.seasons,
                new CompressedSeason(lastSeason, variant),
                new CompressedSeason(nextSeason, variant)
        );
        lastSeason = nextSeason;
        ++gameInfo.seasons;

        updateHotSeatInfo();
    }

    public void performEdit() {
        Assertions.checkState(gameInfo.mode == GameInfo.MODE_LOCAL);

        lastSeason = currentSeason;
        gameInfo.seasons = currentSeasonId;

        Iterator<Order> orderIterator = currentSeason.getAllOrders().iterator();
        while (orderIterator.hasNext()) {
            Order order = orderIterator.next();
            order.setResolution(Order.Resolution.SUCCESS);
            if (order.type == Order.Type.DEFAULT_REMOVE) {
                orderIterator.remove();
            }
        }

        seasonStorage.removeGameEnd(gameInfo.gameId, currentSeasonId);
        seasonStorage.updateSeason(gameInfo.gameId, gameInfo.seasons,
                new CompressedSeason(lastSeason, variant)
        );
    }


    public void persistStatus() {
        if (currentSeason == lastSeason) {
            seasonStorage.updateSeason(gameInfo.gameId, gameInfo.seasons,
                    new CompressedSeason(lastSeason, variant)
            );
        }
        seasonStorage.updateGameInfo(gameInfo);
    }

    private void updateHotSeatInfo() {
        gameInfo.hotSeatCountry = null;

        // update missingCountries list with those countries that have any orders to be input
        gameInfo.missingCountries.clear();
        for (Country country : variant.getCountries()) {
            if (lastSeason.hasOrders(country)) {
                gameInfo.missingCountries.add(country);
            }
        }
    }

}
