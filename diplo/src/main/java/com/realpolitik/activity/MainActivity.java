package com.realpolitik.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.realpolitik.R;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabbed_activity);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        addTab(R.string.tab_new_game, "NEW_GAME", NewGameFragment.class);
        addTab(R.string.tab_open_game, "OPEN_GAME", LoadGameFragment.class);
        addTab(R.string.tab_open_file, "OPEN_FILE", OpenFileFragment.class);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_prefs:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.action_help:
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.help_url))));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addTab(int labelId, String tag, Class<? extends Fragment> clz) {
        ActionBar.Tab tab = getSupportActionBar().newTab()
                .setText(labelId).setTabListener(new TabListener(this, tag, clz));
        getSupportActionBar().addTab(tab);
    }
}
