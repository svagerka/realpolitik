package com.realpolitik.rp.common;

import android.graphics.Bitmap;

import com.realpolitik.rp.season.Season;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Variant {
    private enum BuildType {
        STANDARD,
        ABERRATION,
        CHAOS
    }

    private final BuildType buildType;
    private final List<Sector> sectors;
    private final Map<String, Sector> abbreviations;
    private final List<Country> countries;
    public GameInfo gameInfo;
    public Season startingSeason;
    public Bitmap colorMap;
    public RegionInfo regionInfo;
    public String info;

    public Variant(String buildType) {
        switch (buildType) {
            case "Aberration":
                this.buildType = BuildType.ABERRATION;
                break;
            case "Chaos":
                this.buildType = BuildType.CHAOS;
                break;
            default:
                this.buildType = BuildType.STANDARD;
                break;
        }

        this.sectors = new ArrayList<>();
        this.abbreviations = new HashMap<>();
        this.countries = new ArrayList<>();
    }

    public Country getCountryByInitial(char initial) {
        for (Country country : countries) {
            if (country.initial == initial) {
                return country;
            }
        }
        return null;
    }

    public Sector getSectorByAbbrev(String abbrev) {
        return abbreviations.get(abbrev.toLowerCase());
    }


    public Sector getSectorByName(String name) {
        for (Sector sector : sectors) {
            if (sector.name.equalsIgnoreCase(name)) {
                return sector;
            }
        }
        return null;
    }

    public Country getCountryByName(String name) {
        for (Country country : countries) {
            if (country.name.equalsIgnoreCase(name)) {
                return country;
            }
        }
        return null;
    }

    public Country getCountryByAdjective(String adjective) {
        for (Country country : countries) {
            if (country.adjective.equalsIgnoreCase(adjective)) {
                return country;
            }
        }
        return null;
    }

    public List<Sector> getSectors() {
        return sectors;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void addSectorAbbrev(Sector sector, String abbrev) {
        abbreviations.put(abbrev.toLowerCase(), sector);
    }

    public void addSector(Sector sector) {
        sectors.add(sector);
    }

    public void addCountry(Country country) {
        countries.add(country);
    }

    public boolean isBuildable(Country country, Sector sector) {
        switch (buildType) {
            case STANDARD:
                return country.isHomeCenter(sector);
            default:
                return sector.isSupply();
        }

    }

}
