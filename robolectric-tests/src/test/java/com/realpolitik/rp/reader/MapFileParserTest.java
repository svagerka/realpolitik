package com.realpolitik.rp.reader;

import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.test.DiploTest;
import com.realpolitik.test.RobolectricGradleTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(RobolectricGradleTestRunner.class)
public class MapFileParserTest extends DiploTest {

    private Variant variant;

    public MapFileParserTest() throws ReadException {
        try {
            variant = new Variant("TEST", "Standard");
            String data = "Coast,    l coa\n" +
                    "Double Coast,   l dco dou\n" +
                    "Home,     C hom\n" +
                    "Land,     l lan\n" +
                    "Sea,      w sea\n" +
                    "Supply,   x sup\n" +
                    "-1\n" +
                    "coa-mv: lan sup hom dco\n" +
                    "coa-xc: sea dco/sc\n" +
                    "hom-mv: coa sup lan dco\n" +
                    "lan-mv: coa hom dco\n" +
                    "sea-xc: sea coa dco/nc dco/sc\n" +
                    "sup-mv: coa hom dco\n" +
                    "dco-mv: coa hom lan sup\n" +
                    "dco-sc: coa sea\n" +
                    "dco-nc: sea\n" +
                    "-1";
            new MapFileParser(variant, data.getBytes()).parse();
        } catch (IOException e) {
            e.printStackTrace();
            fail("I/O error");
        } catch (ParseException e) {
            e.printStackTrace();
            fail("Parse error");
        }
    }

    @Test
    public void testParseSectorNames() {
        assertEquals("Coast", variant.getSectorByName("Coast").name);
        assertEquals("Supply", variant.getSectorByName("SUPPLY").name);
        assertEquals(null, variant.getSectorByName("Foo"));
    }

    @Test
    public void testParseSectorAbbrevs() {
        assertEquals("Double Coast", variant.getSectorByAbbrev("dco").name);
        assertEquals("Double Coast", variant.getSectorByAbbrev("dou").name);
        assertEquals("Home", variant.getSectorByAbbrev("HOM").name);
        assertEquals(null, variant.getSectorByAbbrev("wsee"));
    }

    @Test
    public void testParseSectorCoasts() {
        assertTrue(variant.getSectorByAbbrev("coa").isCoastal());
        assertTrue(variant.getSectorByAbbrev("dco").isCoastal());
        assertFalse(variant.getSectorByAbbrev("hom").isCoastal());
        assertFalse(variant.getSectorByAbbrev("lan").isCoastal());
        assertFalse(variant.getSectorByAbbrev("sup").isCoastal());
        assertFalse(variant.getSectorByAbbrev("sea").isCoastal());

        assertFalse(variant.getSectorByAbbrev("coa").hasMultipleCoasts());
        assertTrue(variant.getSectorByAbbrev("dco").hasMultipleCoasts());
        assertFalse(variant.getSectorByAbbrev("hom").hasMultipleCoasts());
        assertFalse(variant.getSectorByAbbrev("lan").hasMultipleCoasts());
        assertFalse(variant.getSectorByAbbrev("sup").hasMultipleCoasts());
        assertFalse(variant.getSectorByAbbrev("sea").hasMultipleCoasts());
    }

    @Test
    public void testParseSectorType() {
        assertFalse(variant.getSectorByAbbrev("coa").isLandlocked());
        assertFalse(variant.getSectorByAbbrev("dco").isLandlocked());
        assertTrue(variant.getSectorByAbbrev("hom").isLandlocked());
        assertTrue(variant.getSectorByAbbrev("lan").isLandlocked());
        assertFalse(variant.getSectorByAbbrev("sea").isLandlocked());
        assertTrue(variant.getSectorByAbbrev("sup").isLandlocked());

        assertFalse(variant.getSectorByAbbrev("coa").isSea());
        assertFalse(variant.getSectorByAbbrev("dco").isSea());
        assertFalse(variant.getSectorByAbbrev("hom").isSea());
        assertFalse(variant.getSectorByAbbrev("lan").isSea());
        assertTrue(variant.getSectorByAbbrev("sea").isSea());
        assertFalse(variant.getSectorByAbbrev("sup").isSea());

        assertFalse(variant.getSectorByAbbrev("coa").isSupply());
        assertFalse(variant.getSectorByAbbrev("dco").isSupply());
        assertTrue(variant.getSectorByAbbrev("hom").isSupply());
        assertFalse(variant.getSectorByAbbrev("lan").isSupply());
        assertFalse(variant.getSectorByAbbrev("sea").isSupply());
        assertTrue(variant.getSectorByAbbrev("sup").isSupply());

    }

    @Test
    public void testParseSectorAdjacency() {
        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.ARMY,
                variant.getSectorByAbbrev("sea")));
        assertTrue(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.ARMY,
                variant.getSectorByAbbrev("hom")));
        assertTrue(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.ARMY,
                variant.getSectorByAbbrev("dco")));
        assertTrue(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.FLEET,
                variant.getSectorByAbbrev("sea")));
        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.FLEET,
                variant.getSectorByAbbrev("hom")));
        assertTrue(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.FLEET,
                variant.getSectorByAbbrev("dco")));

        assertTrue(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.ARMY,
                variant.getSectorByAbbrev("hom")));
        assertTrue(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.ARMY,
                variant.getSectorByAbbrev("coa")));
        assertTrue(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.SOUTH,
                variant.getSectorByAbbrev("coa")));
        assertTrue(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.SOUTH,
                variant.getSectorByAbbrev("sea")));
        assertFalse(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.NORTH,
                variant.getSectorByAbbrev("coa")));
        assertTrue(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.NORTH,
                variant.getSectorByAbbrev("sea")));

        assertTrue(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.ARMY,
                new Location(variant.getSectorByAbbrev("coa"), Sector.Coast.ARMY)));
        assertFalse(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.ARMY,
                new Location(variant.getSectorByAbbrev("coa"), Sector.Coast.FLEET)));
        assertTrue(variant.getSectorByAbbrev("dco").isReachable(Sector.Coast.SOUTH,
                new Location(variant.getSectorByAbbrev("coa"), Sector.Coast.FLEET)));

        assertTrue(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.FLEET,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.SOUTH)));
        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.FLEET,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.NORTH)));
        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.FLEET,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.FLEET)));
        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.FLEET,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.ARMY)));

        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.ARMY,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.SOUTH)));
        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.ARMY,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.NORTH)));
        assertFalse(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.ARMY,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.FLEET)));
        assertTrue(variant.getSectorByAbbrev("coa").isReachable(Sector.Coast.ARMY,
                new Location(variant.getSectorByAbbrev("dco"), Sector.Coast.ARMY)));
    }

}
