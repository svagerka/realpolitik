package com.realpolitik.rp.common;

import android.graphics.Point;
import android.util.Log;

import com.realpolitik.util.Assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sector implements Comparable<Sector> {
    public static final int TYPE_DEFAULT = 0x0;
    public static final int TYPE_LAND = 0x1;
    public static final int TYPE_SUPPLY = 0x3; // supplies are land

    public enum Coast {
        ARMY,
        FLEET,
        SOUTH,
        NORTH,
        WEST,
        EAST;

        private static final Coast[] allCoasts = {ARMY, FLEET, SOUTH, NORTH, WEST, EAST};
    }

    public final int id;
    public final String name;
    public final String abbrev; // the main abbrev
    private final int type;

    private final Map<Coast, List<Location>> adjacent;
    private final Map<Coast, Point> unitPosition;

    public Sector(int id, String name, String abbrev, int type) {
        this.id = id;
        this.name = Assertions.notNull(name);
        this.abbrev = Assertions.notNull(abbrev);
        this.type = type;
        this.adjacent = new HashMap<>();
        this.unitPosition = new HashMap<>();

        // land sectors can be islands and have no adjacencies
        if ((type & TYPE_LAND) == TYPE_LAND) {
            adjacent.put(Coast.ARMY, new ArrayList<Location>());
        }
    }

    public void setUnitPosition(Coast coast, Point point) {
        unitPosition.put(coast, point);
    }

    public Point getUnitPosition(Coast coast) {
        if (unitPosition.containsKey(coast)) {
            return unitPosition.get(coast);
        } else {
            Log.w("NO COAST", "Sector: " + name + ", " + coast.toString() + "; using ARMY instead");
            return unitPosition.get(Coast.ARMY);
        }
    }

    public void addAdjacent(Coast sourceCoast, Location destination) {
        Assertions.notNull(destination);

        List<Location> adjacentSectors = adjacent.get(sourceCoast);
        if (adjacentSectors == null) {
            adjacentSectors = new ArrayList<>();
            adjacent.put(sourceCoast, adjacentSectors);
        }

        adjacentSectors.add(destination);
    }

    public List<Location> getAllAdjacencies(Coast sourceCoast) {
        return adjacent.get(sourceCoast);
    }

    public boolean isSupply() {
        return (type & TYPE_SUPPLY) == TYPE_SUPPLY;
    }

    public boolean isLandlocked() {
        return adjacent.size() == 1 && adjacent.containsKey(Coast.ARMY);
    }

    public boolean isSea() {
        return adjacent.size() == 1 && adjacent.containsKey(Coast.FLEET);
    }

    public boolean isCoastal() {
        return adjacent.size() > 1;
    }

    public boolean hasMultipleCoasts() {
        return adjacent.size() > 2;
    }

    public Coast[] getAvailableCoasts() {
        Coast[] coasts = new Coast[adjacent.size()];
        int index = 0;
        for (Coast coast : Coast.allCoasts) {
            if (adjacent.containsKey(coast)) {
                coasts[index++] = coast;
            }
        }
        return coasts;
    }

    public boolean isReachable(Coast source, Location location) {
        return adjacent.containsKey(source) && adjacent.get(source).contains(location);
    }

    public boolean isReachable(Coast source, Sector sector) {
        if (!adjacent.containsKey(source)) {
            return false;
        }

        for (Location location : adjacent.get(source)) {
            if (location.sector == sector) {
                return true;
            }
        }

        return false;
    }


    @Override
    public int compareTo(Sector sector) {
        return id - sector.id;
    }

    @Override
    public String toString() {
        return id + abbrev;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Sector)) {
            return false;
        }

        Sector sector = (Sector) o;
        return sector.id == id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}

