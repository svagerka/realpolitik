package com.realpolitik.rp.reader;

import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.util.Assertions;

public class TokenParser {
    public static Location parseLocation(Variant variant, String location, Sector.Coast defaultCoast) throws ReadException {
        Assertions.notNull(defaultCoast);

        Sector.Coast coast = defaultCoast;
        int pos = location.indexOf('(');
        if (pos == -1) {
            pos = location.indexOf('/');
        }
        if (pos != -1) {
            coast = parseCoast(location.charAt(pos + 1));
            location = location.substring(0, pos);
        }

        Sector sector = variant.getSectorByAbbrev(location);
        if (sector == null) {
            throw new NoSectorException(location);
        }
        return new Location(sector, coast);
    }

    public static Sector.Coast parseCoast(String coast) {
        return parseCoast(coast.charAt(0));
    }

    public static Sector.Coast parseCoast(char coast) {
        switch (coast) {
            case 'n':
            case 'N':
                return Sector.Coast.NORTH;
            case 's':
            case 'S':
                return Sector.Coast.SOUTH;
            case 'e':
            case 'E':
                return Sector.Coast.EAST;
            case 'w':
            case 'W':
                return Sector.Coast.WEST;
            case 'x':
            case 'X':
                return Sector.Coast.FLEET;
            case 'm':
            case 'M':
                return Sector.Coast.ARMY;
            default:
                return null;
        }
    }

    public static Unit.Type parseUnitType(String token) {
        return token.equals("A") ? Unit.Type.ARMY : Unit.Type.FLEET;
    }
}
