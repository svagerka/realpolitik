package com.realpolitik.rp.common;

import com.realpolitik.util.Assertions;

import java.util.Collections;
import java.util.List;

public class Dislodge {
    public final Unit unit;
    public final List<Location> locations;

    public Dislodge(Unit unit, List<Location> locations) {
        this.unit = Assertions.notNull(unit);
        this.locations = Collections.unmodifiableList(locations);
    }

    public boolean canRetreatTo(Sector sector) {
        Assertions.notNull(sector);
        for (Location retreat : locations) {
            if (retreat.sector == sector) {
                return true;
            }
        }
        return false;
    }

}
