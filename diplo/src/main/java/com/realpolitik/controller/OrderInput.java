package com.realpolitik.controller;

import android.content.SharedPreferences;

import com.realpolitik.R;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.season.Season;
import com.realpolitik.util.Assertions;
import com.realpolitik.util.Pref;

import java.util.Arrays;

public class OrderInput implements UiHelper.OnDialogResult, SharedPreferences.OnSharedPreferenceChangeListener {

    public interface Listener {
        void onSelectedSectorChanged();

        void onInputError(int errorDescriptionResourceId);

        void onMultipleChoices(int dialogTitleResourceId, int[] dialogOptionsResourceIds);

        void onOrder(Order order);
    }

    private static final int STATE_IDLE = 1;
    private static final int STATE_PICKING_ORDER = 2;
    private static final int STATE_MOVEMENTS_MOVING_UNIT_SELECTED = 12;
    private static final int STATE_MOVEMENTS_SUPPORTIVE_UNIT_SELECTED = 13;
    private static final int STATE_MOVEMENTS_SUPPORTED_UNIT_SELECTED = 14;
    private static final int STATE_MOVEMENTS_CONVOYING_UNIT_SELECTED = 15;
    private static final int STATE_MOVEMENTS_CONVOYED_UNIT_SELECTED = 16;
    private static final int STATE_RETREATS_RETREATING_UNIT_SELECTED = 22;

    private final Listener listener;

    private boolean forbidImpossibleMoves;
    private boolean autoOrderType;

    private Season currentSeason;
    private Game game;

    private Sector selectedSector;
    private Sector.Coast selectedCoast;
    private Unit selectedUnit;
    private Dislodge selectedDislodge;
    private int state;
    private final Unit[] unitStack;

    public OrderInput(SharedPreferences preferences, Listener listener) {
        this.listener = Assertions.notNull(listener);
        unitStack = new Unit[2];
        state = STATE_IDLE;

        forbidImpossibleMoves = preferences.getBoolean(
                Pref.KEY_FORBID_IMPOSSIBLE_MOVES, Pref.DEFAULT_FORBID_IMPOSSIBLE_MOVES);
        autoOrderType = preferences.getBoolean(
                Pref.KEY_AUTO_ORDER_TYPE, Pref.DEFAULT_AUTO_ORDER_TYPE);
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setCurrentSeason(Season currentSeason) {
        this.currentSeason = currentSeason;
        this.selectedSector = null;
        state = STATE_IDLE;
    }

    public Sector getSelectedSector() {
        return selectedSector;
    }

    public void startSectorClick(Sector clickedSector) {
        if (currentSeason == null || clickedSector == null) {
            return;
        }

        if (state == STATE_IDLE) {
            switch (currentSeason.getPhaseType()) {
                case MOVEMENT:
                    selectedUnit = currentSeason.getUnit(clickedSector);
                    if (selectedUnit == null
                            || !isOrderForCountryAllowed(selectedUnit.country)) {
                        return;
                    }
                    break;

                case RETREAT:
                    selectedDislodge = currentSeason.getRetreat(clickedSector);
                    if (selectedDislodge == null
                            || !isOrderForCountryAllowed(selectedDislodge.unit.country)) {
                        return;
                    }
                    break;

                case ADJUSTMENT:
                    selectedUnit = currentSeason.getUnit(clickedSector);
                    if (!canSetAdjustmentOrder(clickedSector)) {
                        return;
                    }
            }
        }

        selectedSector = clickedSector;
        listener.onSelectedSectorChanged();

        if (!autoOrderType && state == STATE_IDLE) {
            displayOrderPicker();
        }
    }

    private boolean isOrderForCountryAllowed(Country country) {
        switch (game.gameInfo.mode) {
            case GameInfo.MODE_LOCAL:
                return true;
            case GameInfo.MODE_HOT_SEAT:
                if (game.gameInfo.hotSeatCountry == null) {
                    return game.gameInfo.missingCountries.contains(country);
                } else {
                    return game.gameInfo.hotSeatCountry == country;
                }
            default:
                throw new IllegalStateException();
        }
    }

    private boolean canSetAdjustmentOrder(Sector sector) {
        if (currentSeason.getOrder(sector) != null) {
            // an order is here => can always be canceled or changed
            return true;
        }

        Country country = (selectedUnit != null)
                ? selectedUnit.country : currentSeason.getSupplyCenterOwner(sector);
        if (country == null) {
            // empty sector no owned by anyone => no order allowed
            return false;
        }

        if (!isOrderForCountryAllowed(country)) {
            // different country is selected => no order allowed
            return false;
        }

        int adjustmentCount = currentSeason.getAdjustmentCount(country);
        int orderCount = currentSeason.getOrderCount(country);
        if (orderCount == Math.abs(adjustmentCount)) {
            // no more orders accepted
            return false;
        }

        if (adjustmentCount < 0 && selectedUnit != null) {
            // removes don't have any requirements
            return true;
        }


        return adjustmentCount > 0 && game.variant.isBuildable(country, sector) && selectedUnit == null;
    }

    public void longPress() {
        if (selectedSector != null && autoOrderType) {
            displayOrderPicker();
        }
    }

    private void displayOrderPicker() {
        switch (currentSeason.getPhaseType()) {
            case MOVEMENT:
                selectedUnit = currentSeason.getUnit(selectedSector);
                if (selectedUnit == null) {
                    state = STATE_IDLE;
                } else {
                    state = STATE_PICKING_ORDER;
                    unitStack[0] = selectedUnit;
                    showOrderPicker();
                }
                break;

            case ADJUSTMENT:
                state = STATE_PICKING_ORDER;
                if (selectedUnit != null) {
                    showRemovePicker();
                } else {
                    showBuildPicker(selectedSector);
                }
                break;

            case RETREAT:
                selectedDislodge = currentSeason.getRetreat(selectedSector);
                if (selectedDislodge == null) {
                    state = STATE_IDLE;
                } else {
                    state = STATE_PICKING_ORDER;
                    unitStack[0] = selectedDislodge.unit;
                    showOrderPicker();
                }
                break;

            default:
                throw new IllegalArgumentException();
        }
    }

    public void cancelSectorClick() {
        // when picking order, the current selection needs to be remembered
        if (state == STATE_PICKING_ORDER) {
            return;
        }

        selectedSector = null;
        selectedCoast = null;
        selectedUnit = null;
        listener.onSelectedSectorChanged();
    }

    public void finishSectorClick(Sector clickedSector) {
        if (selectedSector == null) {
            return;
        }

        if (selectedSector != clickedSector) {
            cancelSectorClick();
            return;
        }

        switch (state) {
            case STATE_PICKING_ORDER:
                // do nothing - picker is displayed in longPress()
                return;

            case STATE_IDLE:
                switch (currentSeason.getPhaseType()) {
                    case MOVEMENT:
                        Assertions.notNull(selectedUnit);
                        unitStack[0] = selectedUnit;
                        state = STATE_MOVEMENTS_MOVING_UNIT_SELECTED;
                        break;

                    case RETREAT:
                        Assertions.notNull(selectedDislodge);
                        unitStack[0] = selectedDislodge.unit;
                        state = STATE_RETREATS_RETREATING_UNIT_SELECTED;
                        break;

                    case ADJUSTMENT:
                        Order currentOrder = currentSeason.getOrder(selectedSector);
                        if (selectedUnit != null) {
                            if (currentOrder != null) {
                                currentSeason.removeOrderAt(selectedSector);
                            } else {
                                addOrder(new Order(selectedUnit, Order.Type.REMOVE));
                            }
                        } else {
                        /* Simple click cycles through:
                         * No build => Build Army => Build Fleet
                         *  or
                         * No build => Build Army => Build Fleet (nc) => Build Fleet (ec) ... */
                            Sector.Coast[] possibleBuildCoasts = selectedSector.getAvailableCoasts();
                            if (selectedCoast == null) {
                                int currentOrderCoastIndex = -1;
                                if (currentOrder != null) {
                                    currentOrderCoastIndex = Arrays.binarySearch(possibleBuildCoasts,
                                            currentOrder.unit.location.coast);
                                    if (currentOrderCoastIndex == possibleBuildCoasts.length - 1) {
                                        // cycled through all orders, just remove the build
                                        currentSeason.removeOrderAt(selectedSector);
                                        break;
                                    }
                                }
                                selectedCoast = possibleBuildCoasts[currentOrderCoastIndex + 1];
                            }
                            Unit builtUnit = new Unit(
                                    selectedCoast == Sector.Coast.ARMY ? Unit.Type.ARMY : Unit.Type.FLEET,
                                    new Location(selectedSector, selectedCoast),
                                    currentSeason.getSupplyCenterOwner(selectedSector));
                            addOrder(new Order(builtUnit, Order.Type.BUILD));
                        }
                        break;
                }
                break;

            case STATE_MOVEMENTS_MOVING_UNIT_SELECTED:
                Assertions.notNull(unitStack[0]);
                if (selectedSector == unitStack[0].location.sector) {
                    addOrder(new Order(unitStack[0], Order.Type.HOLD));
                } else if (forbidImpossibleMoves && selectedSector.isLandlocked()
                        && unitStack[0].type == Unit.Type.FLEET) {
                    listener.onInputError(R.string.error_fleet_to_landlocked);
                } else if (forbidImpossibleMoves && selectedSector.isSea()
                        && unitStack[0].type == Unit.Type.ARMY) {
                    listener.onInputError(R.string.error_army_to_sea);
                } else {
                    Sector.Coast coast = getCoastForUnit(unitStack[0].type, selectedSector);
                    if (coast == null) {
                        // no coast known, wait until value is selected with picker
                        return;
                    }
                    Location target = new Location(selectedSector, coast);
                    addOrder(new Order(unitStack[0], Order.Type.MOVE, target));
                }
                state = STATE_IDLE;
                break;

            case STATE_MOVEMENTS_SUPPORTIVE_UNIT_SELECTED:
                Assertions.notNull(unitStack[0]);
                Unit unit = currentSeason.getUnit(clickedSector);
                if (selectedSector == unitStack[0].location.sector) {
                    listener.onInputError(R.string.error_cannot_support_itself);
                    state = STATE_IDLE;
                } else if (unit == null) {
                    listener.onInputError(R.string.error_no_unit_for_support);
                    state = STATE_IDLE;
                } else {
                    unitStack[1] = unit;
                    state = STATE_MOVEMENTS_SUPPORTED_UNIT_SELECTED;
                }
                break;

            case STATE_MOVEMENTS_SUPPORTED_UNIT_SELECTED:
                Assertions.notNull(unitStack[0]);
                Assertions.notNull(unitStack[1]);
                if (forbidImpossibleMoves && selectedSector.isLandlocked()
                        && (unitStack[0].type == Unit.Type.FLEET
                        || unitStack[1].type == Unit.Type.FLEET)) {
                    listener.onInputError(R.string.error_fleet_to_landlocked);
                } else if (forbidImpossibleMoves &&
                        selectedSector.isSea() && (unitStack[0].type == Unit.Type.ARMY
                        || unitStack[1].type == Unit.Type.ARMY)) {
                    listener.onInputError(R.string.error_army_to_sea);
                } else if (selectedSector == unitStack[1].location.sector) {
                    addOrder(new Order(unitStack[0], Order.Type.SUPPORT_HOLD,
                            unitStack[1]));
                } else {
                    Sector.Coast coast = getCoastForUnit(unitStack[1].type, selectedSector);
                    if (coast == null) {
                        // no coast known, wait until value is selected with picker
                        return;
                    }
                    Location target = new Location(selectedSector, coast);
                    addOrder(new Order(unitStack[0], Order.Type.SUPPORT_MOVE,
                            unitStack[1], target));
                }
                state = STATE_IDLE;
                break;

            case STATE_MOVEMENTS_CONVOYING_UNIT_SELECTED:
                Assertions.notNull(unitStack[0]);
                unit = currentSeason.getUnit(clickedSector);
                if (selectedSector.isLandlocked()) {
                    listener.onInputError(R.string.error_convoy_landlocked);
                    state = STATE_IDLE;
                } else if (unit == null) {
                    listener.onInputError(R.string.error_no_unit_to_convoy);
                    state = STATE_IDLE;
                } else if (unit.type == Unit.Type.FLEET) {
                    listener.onInputError(R.string.error_cannot_convoy_fleet);
                    state = STATE_IDLE;
                } else {
                    unitStack[1] = unit;
                    state = STATE_MOVEMENTS_CONVOYED_UNIT_SELECTED;
                }
                break;

            case STATE_MOVEMENTS_CONVOYED_UNIT_SELECTED:
                Assertions.notNull(unitStack[0]);
                Assertions.notNull(unitStack[1]);
                if (selectedSector.isLandlocked()) {
                    listener.onInputError(R.string.error_convoy_landlocked);
                } else if (selectedSector == unitStack[1].location.sector) {
                    listener.onInputError(R.string.error_cannot_convoy_hold);
                } else {
                    Location target = new Location(selectedSector, Sector.Coast.ARMY);
                    addOrder(new Order(unitStack[0], Order.Type.CONVOY,
                            unitStack[1], target));
                }
                state = STATE_IDLE;
                break;

            case STATE_RETREATS_RETREATING_UNIT_SELECTED:
                Assertions.notNull(unitStack[0]);
                if (selectedSector == unitStack[0].location.sector) {
                    addOrder(new Order(unitStack[0], Order.Type.DISBAND));
                } else if (!selectedDislodge.canRetreatTo(selectedSector)) {
                    listener.onInputError(R.string.error_cannot_retreat_there);
                } else {
                    Sector.Coast coast = getCoastForUnit(unitStack[0].type, selectedSector);
                    if (coast == null) {
                        // no coast known, wait until value is selected with picker
                        return;
                    }
                    Location target = new Location(selectedSector, coast);
                    addOrder(new Order(unitStack[0], Order.Type.RETREAT, target));
                }
                state = STATE_IDLE;
                break;

            default:
                throw new IllegalStateException();
        }

        cancelSectorClick();
    }

    private void addOrder(Order order) {
        if (game.gameInfo.mode == GameInfo.MODE_HOT_SEAT) {
            game.gameInfo.hotSeatCountry = order.unit.country;
            game.gameInfo.missingCountries.remove(order.unit.country);
        }

        currentSeason.addOrder(order);

        listener.onOrder(order);
    }

    private void showOrderPicker() {
        Assertions.checkState(state == STATE_PICKING_ORDER);
        int[] options;

        switch (currentSeason.getPhaseType()) {
            case MOVEMENT:
                Assertions.notNull(unitStack[0]);
                if (unitStack[0].type == Unit.Type.ARMY
                        || unitStack[0].location.sector.isCoastal()) {
                    options = new int[]{R.string.order_hold, R.string.order_move,
                            R.string.order_support};
                } else {
                    options = new int[]{R.string.order_hold, R.string.order_move,
                            R.string.order_support, R.string.order_convoy};
                }
                break;
            case RETREAT:
                Assertions.notNull(unitStack[0]);
                options = new int[]{R.string.order_retreat, R.string.order_disband};
                break;
            default:
                throw new IllegalStateException();
        }

        listener.onMultipleChoices(R.string.order_selection_dialog_title, options);
    }

    /**
     * Returns target coast for the unit. If the coast is ambiguous, displays coast picker.
     * If a picker was recently issued and user selected a value, use it.
     */
    private Sector.Coast getCoastForUnit(Unit.Type unit, Sector targetSector) {
        if (unit == Unit.Type.ARMY) {
            return Sector.Coast.ARMY;
        }
        if (!targetSector.hasMultipleCoasts()) {
            return Sector.Coast.FLEET;
        }
        if (selectedCoast != null) {
            return selectedCoast;
        }

        Sector.Coast[] coasts = targetSector.getAvailableCoasts();
        int[] availableCoastArray = new int[coasts.length - 1]; // omit army coast
        for (int i = 0; i < coasts.length - 1; ++i) {
            switch (coasts[i + 1]) {
                case NORTH:
                    availableCoastArray[i] = R.string.coast_north;
                    break;
                case WEST:
                    availableCoastArray[i] = R.string.coast_west;
                    break;
                case EAST:
                    availableCoastArray[i] = R.string.coast_east;
                    break;
                case SOUTH:
                    availableCoastArray[i] = R.string.coast_south;
                    break;
            }
        }

        listener.onMultipleChoices(R.string.coast_selection_dialog_title, availableCoastArray);
        return null;
    }

    private void showBuildPicker(Sector sector) {
        Sector.Coast[] coasts = sector.getAvailableCoasts();
        int[] availableBuilds = new int[coasts.length + 1];
        availableBuilds[0] = R.string.order_no_build;
        for (int i = 1; i < coasts.length + 1; ++i) {
            switch (coasts[i - 1]) {
                case ARMY:
                    availableBuilds[i] = R.string.order_build_army;
                    break;
                case FLEET:
                    availableBuilds[i] = R.string.order_build_fleet;
                    break;
                case NORTH:
                    availableBuilds[i] = R.string.order_build_fleet_north;
                    break;
                case WEST:
                    availableBuilds[i] = R.string.order_build_fleet_west;
                    break;
                case EAST:
                    availableBuilds[i] = R.string.order_build_fleet_east;
                    break;
                case SOUTH:
                    availableBuilds[i] = R.string.order_build_fleet_south;
                    break;
            }
        }

        listener.onMultipleChoices(R.string.build_selection_title, availableBuilds);
    }

    private void showRemovePicker() {
        listener.onMultipleChoices(R.string.build_selection_title,
                new int[]{R.string.order_keep, R.string.order_remove});
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (Pref.KEY_AUTO_ORDER_TYPE.equals(key)) {
            autoOrderType = sharedPreferences.getBoolean(key, autoOrderType);
        } else if (Pref.KEY_FORBID_IMPOSSIBLE_MOVES.equals(key)) {
            forbidImpossibleMoves = sharedPreferences.getBoolean(key, forbidImpossibleMoves);
        }
    }

    @Override
    public void cancelled() {
        switch (state) {
            case STATE_MOVEMENTS_MOVING_UNIT_SELECTED:
            case STATE_MOVEMENTS_SUPPORTED_UNIT_SELECTED:
            case STATE_RETREATS_RETREATING_UNIT_SELECTED:
                break;
            case STATE_PICKING_ORDER:
                state = STATE_IDLE;
                break;
        }

        cancelSectorClick();
    }

    @Override
    public void itemClicked(int which) {
        switch (state) {
            case STATE_MOVEMENTS_MOVING_UNIT_SELECTED:
            case STATE_MOVEMENTS_SUPPORTED_UNIT_SELECTED:
            case STATE_RETREATS_RETREATING_UNIT_SELECTED:
                selectedCoast = selectedSector.getAvailableCoasts()[which + 1];
                finishSectorClick(selectedSector);
                break;

            case STATE_PICKING_ORDER:
                switch (currentSeason.getPhaseType()) {
                    case MOVEMENT:
                        switch (which) {
                            case 0:
                                state = STATE_MOVEMENTS_MOVING_UNIT_SELECTED;
                                finishSectorClick(selectedSector);
                                break;
                            case 1:
                                state = STATE_MOVEMENTS_MOVING_UNIT_SELECTED;
                                cancelSectorClick();
                                break;
                            case 2:
                                state = STATE_MOVEMENTS_SUPPORTIVE_UNIT_SELECTED;
                                cancelSectorClick();
                                break;
                            case 3:
                                Assertions.checkState(unitStack[0].type == Unit.Type.FLEET);
                                state = STATE_MOVEMENTS_CONVOYING_UNIT_SELECTED;
                                cancelSectorClick();
                                break;
                        }
                        break;

                    case RETREAT:
                        state = STATE_RETREATS_RETREATING_UNIT_SELECTED;
                        if (which == 1) {
                            finishSectorClick(selectedSector);
                        } else {
                            cancelSectorClick();
                        }
                        break;

                    case ADJUSTMENT:
                        state = STATE_IDLE;
                        if (selectedUnit != null) {
                            currentSeason.removeOrderAt(selectedSector);
                            if (which == 0) {
                                cancelSectorClick();
                            } else {
                                finishSectorClick(selectedSector);
                            }
                        } else {
                            if (which != 0) {
                                selectedCoast = selectedSector.getAvailableCoasts()[which - 1];
                                finishSectorClick(selectedSector);
                            } else {
                                currentSeason.removeOrderAt(selectedSector);
                                cancelSectorClick();
                            }
                        }
                        break;
                }
                break;

            default:
                Assertions.checkState(false);
        }
    }

}
