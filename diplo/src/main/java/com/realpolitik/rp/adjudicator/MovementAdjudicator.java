package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.season.SeasonBuilder;

public class MovementAdjudicator extends Adjudicator {

    public MovementAdjudicator(Variant variant, Season season) {
        super(variant, season);
    }

    @Override
    protected void perform(SeasonBuilder seasonBuilder) {
        seasonBuilder.setSupplyOwnership(season.getSupplyCenters());
        int[] remainingAdjustments = getRemainingAdjustments();

        OrderInfo[] orders = new OrderInfo[variant.getSectors().size()];
        for (Sector sector : variant.getSectors()) {
            orders[sector.id] = new OrderInfo(orders, season);
        }

        for (Order order : season.getAllOrders()) {
            orders[order.unit.location.sector.id].order = order;
        }

        // register orders affecting other sectors and check for fast failures
        for (OrderInfo order : orders) {
            order.preProcess();
        }

        // calculate attacks to each sector to determine dislodges, bounces and successful moves
        for (OrderInfo order : orders) {
            order.process();
        }

        // un-break cycles, construct resolutions
        for (OrderInfo order : orders) {
            order.postProcess();
        }

        // finalize dislodges
        for (OrderInfo order : orders) {
            order.findDislodges();
        }

        for (OrderInfo order : orders) {
            if (order.order != null) {
                Unit unit = order.order.unit;
                season.getOrder(unit.location.sector).setResolution(order.resolution);

                if (order.isMoveSuccessful) {
                    seasonBuilder.addUnit(unit.type, order.order.targetLocation,
                            unit.country);
                } else if (order.resolution == Order.Resolution.DISLODGED) {
                    Dislodge dislodge = new Dislodge(unit, order.dislodgeLocations);
                    seasonBuilder.addRetreat(dislodge);
                } else if (order.resolution == Order.Resolution.DISBANDED) {
                    ++remainingAdjustments[unit.country.id];
                } else {
                    seasonBuilder.addUnit(unit);
                }
            }
        }

        seasonBuilder.setAdjustmentCount(remainingAdjustments);
    }

}
