package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.reader.ReadException;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.SeasonBuilder;
import com.realpolitik.test.AdjudicatorTest;
import com.realpolitik.test.RobolectricGradleTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.realpolitik.rp.common.Order.Resolution.BOUNCE;
import static com.realpolitik.rp.common.Order.Resolution.FAILS;
import static com.realpolitik.rp.common.Order.Resolution.SUCCESS;

@RunWith(RobolectricGradleTestRunner.class)
public class MovementAdjudicatorTest extends AdjudicatorTest {

    public MovementAdjudicatorTest() throws ReadException {
    }

    @Test
    public void testMoveToAdjacentLocationSucceeds() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxc", 0)
                .addUnit(Unit.Type.ARMY, "dxa", 0)
                .addUnit(Unit.Type.FLEET, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "cxc", 0)
                .addUnit(Unit.Type.FLEET, "exc", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxc - cxb"); // coast -> coast
        addOrder("A dxa - dxb"); // landlocked -> landlocked
        addOrder("F bxe - bxd"); // coast -> coast
        addOrder("F cxc - cxd"); // sea -> sea
        addOrder("F exc - dxd"); // coast -> sea
        season.resolve();
        assertOrderResolution("bxc", SUCCESS);
        assertOrderResolution("dxa", SUCCESS);
        assertOrderResolution("bxe", SUCCESS);
        assertOrderResolution("cxc", SUCCESS);
        assertOrderResolution("exc", SUCCESS);
    }

    @Test
    public void testMoveToDistantLocationFails() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxc", 0)
                .addUnit(Unit.Type.ARMY, "dxa", 0)
                .addUnit(Unit.Type.FLEET, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "cxc", 0)
                .addUnit(Unit.Type.FLEET, "exc", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxc - dxd"); // coast -> coast
        addOrder("A dxa - bxb"); // landlocked -> landlocked
        addOrder("F bxe - bxc"); // coast -> coast
        addOrder("F cxc - dxe"); // sea -> sea
        addOrder("F exc - exc"); // coast -> sea
        season.resolve();
        assertOrderResolution("bxc", FAILS);
        assertOrderResolution("dxa", FAILS);
        assertOrderResolution("bxe", FAILS);
        assertOrderResolution("cxc", FAILS);
        assertOrderResolution("exc", FAILS);
    }

    @Test
    public void testMoveToCorrectCoastSucceeds() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.FLEET, "exc", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: F exc - dxc(ec)");
        season.resolve();
        assertOrderResolution("exc", SUCCESS);
    }

    @Test
    public void testMoveToUnspecifiedCoastFails() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.FLEET, "exc", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: F exc - dxc");
        season.resolve();
        assertOrderResolution("exc", FAILS);
    }

    @Test
    public void testBouncedOrdersFail() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.FLEET, "exc", 0)
                .addUnit(Unit.Type.FLEET, "dxe", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: F exc - exd");
        addOrder("F dxe - exd");
        season.resolve();
        assertOrderResolution("exc", BOUNCE);
        assertOrderResolution("dxe", BOUNCE);
    }

    @Test
    public void testFailedOrderDoesNotCauseBounce() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.FLEET, "exc", 0)
                .addUnit(Unit.Type.FLEET, "cxc", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: F exc - exd");
        addOrder("F cxc - exd");
        season.resolve();
        assertOrderResolution("exc", SUCCESS);
        assertOrderResolution("cxc", FAILS);
    }


    @Test
    public void testUnitsCannotSwapPlaces() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxc", 0)
                .addUnit(Unit.Type.FLEET, "bxd", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxc - bxd");
        addOrder("F bxd - bxc");
        season.resolve();
        assertOrderResolution("bxc", FAILS);
        assertOrderResolution("bxd", FAILS);
    }

    @Test
    public void testChainOfUnitsMoves() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxc", 0)
                .addUnit(Unit.Type.FLEET, "bxd", 0)
                .addUnit(Unit.Type.FLEET, "bxe", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxc - bxb");
        addOrder("A bxd - bxc");
        addOrder("F bxe - bxd");
        season.resolve();
        assertOrderResolution("bxc", SUCCESS);
        assertOrderResolution("bxd", SUCCESS);
        assertOrderResolution("bxe", SUCCESS);
    }


    @Test
    public void testCycleOfUnitsMoves() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxc", 0)
                .addUnit(Unit.Type.FLEET, "bxd", 0)
                .addUnit(Unit.Type.ARMY, "axc", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxc - axc");
        addOrder("A bxd - bxc");
        addOrder("A axc - bxd");
        season.resolve();
        assertOrderResolution("bxc", SUCCESS);
        assertOrderResolution("bxd", SUCCESS);
        assertOrderResolution("axc", SUCCESS);
    }

    @Test
    public void testCycleOfUnitsMovesEvenIfDislodged() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxc", 0)
                .addUnit(Unit.Type.FLEET, "bxd", 0)
                .addUnit(Unit.Type.ARMY, "axc", 1)
                .addUnit(Unit.Type.ARMY, "axd", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxc - axc");
        addOrder("One: A bxd - bxc");
        addOrder("Two: A axc - bxd");
        addOrder("Two: A axd Supports A axc - bxd");
        season.resolve();
        assertOrderResolution("bxc", SUCCESS);
        assertOrderResolution("bxd", SUCCESS);
        assertOrderResolution("axc", SUCCESS);
    }
}
