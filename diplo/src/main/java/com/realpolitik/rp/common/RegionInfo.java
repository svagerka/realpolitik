package com.realpolitik.rp.common;

import android.graphics.Path;
import android.graphics.Point;
import android.util.SparseArray;

import com.realpolitik.util.Assertions;

import java.util.HashMap;
import java.util.Map;

public class RegionInfo {
    /*
     * Stores the point => sector mapping.
     * Each SparseArray represents one horizontal line. Each line is split into chunks,
     * that either have a sector attached, or are outside of any sectors. The Key represents
     * the left-most point of the chunk and the Value the sector, if any. Everything with lower
     * key than the lowest in the map does not belong to any sector. */
    private final SparseArray<Sector> sectors[];
    private final int width;
    private final int height;
    private final Map<Sector, Path> precomputedPaths;

    @SuppressWarnings("unchecked")
    public RegionInfo(int width, int height) {
        this.width = width;
        this.height = height;
        this.sectors = new SparseArray[height];
        this.precomputedPaths = new HashMap<>();
    }

    public void addLine(int x, int y, int xLength, Sector sector) {
        Assertions.notNull(sector);

        if (xLength <= 0 || (x + xLength) > width) {
            throw new ArrayIndexOutOfBoundsException(xLength);
        }

        if (x < 0) {
            throw new ArrayIndexOutOfBoundsException(x);
        }

        if (y < 0 || y >= height) {
            throw new ArrayIndexOutOfBoundsException(y);
        }

        SparseArray<Sector> lines = sectors[y];
        if (lines == null) {
            lines = new SparseArray<>();
            sectors[y] = lines;
        }

        lines.put(x, sector); // chunk representing the sector
        // put the sector end only if there already is not a next sector starting
        if (lines.get(x + xLength) == null)
        {
            lines.put(x + xLength, null); // chunk representing the empty are after the sector
        }

        Path path = precomputedPaths.get(sector);
        if (path == null) {
            path = new Path();
            path.incReserve(100);
            precomputedPaths.put(sector, path);
        }
        path.addRect(x, y, x + xLength, y + 1, Path.Direction.CW);
    }

    public Sector getSector(Point pos) {
        if (pos.x < 0 || pos.x >= width) {
            return null;
        }

        if (pos.y < 0 || pos.y >= height) {
            return null;
        }

        SparseArray<Sector> lines = sectors[pos.y];
        if (lines == null) {
            // line is empty, no sector
            return null;
        }

        int floorKey = -1;
        Sector result = null;

        for (int i = 0; i < lines.size(); i++) {
            int key = lines.keyAt(i);
            if (key <= pos.x && key > floorKey) {
                floorKey = key;
                result = lines.valueAt(i);
            }
        }

        return result;
    }

    public Path getPathForSector(Sector sector) {
        return precomputedPaths.get(sector);
    }
}
