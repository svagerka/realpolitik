package com.realpolitik.rp.season;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.orders.OrderParser;
import com.realpolitik.util.Assertions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

abstract public class Season {

    final Variant variant;
    public final Phase phase;
    public final int year;
    final int[] adjustmentCount;
    private final Country[] supplyCenters;
    private final Unit[] units;
    private final Map<Sector, Order> orders;

    Season(Variant variant, Phase phase, int year,
           int[] adjustmentCount, Country[] supplyCenters,
           Unit[] units, Map<Sector, Order> orders) {
        this.variant = variant;
        this.phase = Assertions.notNull(phase);
        this.year = year;

        this.adjustmentCount = Assertions.notNull(adjustmentCount);
        this.supplyCenters = Assertions.notNull(supplyCenters);
        this.units = Assertions.notNull(units);
        this.orders = Assertions.notNull(orders);
    }

    abstract public Season resolve();

    abstract public OrderParser getOrderParser();

    // Setters for orders
    public void addOrder(Order order) {
        orders.put(order.unit.location.sector, order);
    }

    public void removeOrderAt(Sector sector) {
        orders.remove(sector);
    }

    // Getters
    public Phase.PhaseType getPhaseType() {
        return Phase.PhaseType.fromSeason(phase);
    }

    // Getters for orders
    public List<Order> getAllOrders() {
        return new ArrayList<>(orders.values());
    }

    public List<Order> getOrdersForCountry(Country country) {
        List<Order> countryOrders = new ArrayList<>();
        for (Order order : orders.values()) {
            if (order.unit.country == country) {
                countryOrders.add(order);
            }
        }
        return countryOrders;
    }

    public int getOrderCount(Country country) {
        int count = 0;
        for (Map.Entry<Sector, Order> sectorOrderEntry : orders.entrySet()) {
            if (sectorOrderEntry.getValue().unit.country == country) {
                ++count;
            }
        }
        return count;
    }

    public Order getOrder(Sector sector) {
        return orders.get(sector);
    }

    public boolean hasOrders(Country country) {
        for (Map.Entry<Sector, Order> sectorOrderEntry : orders.entrySet()) {
            if (sectorOrderEntry.getValue().unit.country == country) {
                return true;
            }
        }
        return false;
    }

    // Getters for adjustments
    public int getAdjustmentCount() {
        int sum = 0;
        for (int i : adjustmentCount) {
            sum += Math.abs(i);
        }
        return sum;
    }

    public int getAdjustmentCount(Country country) {
        return adjustmentCount[country.id];
    }

    // Getters for SC
    public final Country[] getSupplyCenters() {
        return supplyCenters;
    }

    public int getSupplyCenterCount(Country country) {
        int count = 0;
        for (Country owningCountry : supplyCenters) {
            if (country == owningCountry) {
                ++count;
            }
        }
        return count;
    }

    public Country getSupplyCenterOwner(Sector sector) {
        Assertions.notNull(sector);
        return supplyCenters[sector.id];
    }

    // Getters for units
    public final Unit[] getAllUnits() {
        return units;
    }

    public int getUnitCount(Country country) {
        int count = 0;
        for (Unit unit : units) {
            if (country == unit.country) {
                ++count;
            }
        }
        return count;
    }

    public List<Unit> getUnits(Country country) {
        List<Unit> units = new ArrayList<>(getUnitCount(country));
        for (Unit unit : this.units) {
            if (unit.country == country) {
                units.add(unit);
            }
        }
        return units;
    }

    public Unit getUnit(Sector sector) {
        for (Unit unit : units) {
            if (unit.location.sector == sector) {
                return unit;
            }
        }
        return null;
    }

    // Getters for dislodges
    public List<Dislodge> getAllRetreats() {
        return null;
    }

    public Dislodge getRetreat(Sector sector) {
        return null;
    }

}
