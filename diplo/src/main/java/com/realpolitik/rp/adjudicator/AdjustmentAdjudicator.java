package com.realpolitik.rp.adjudicator;

import android.util.Pair;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.season.SeasonBuilder;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class AdjustmentAdjudicator extends Adjudicator {

    private static final Comparator<Pair<Unit, Integer>> REMOVE_UNIT_COMPARATOR =
            new Comparator<Pair<Unit, Integer>>() {
                @Override
                public int compare(Pair<Unit, Integer> unit1, Pair<Unit, Integer> unit2) {
                    if (!unit1.second.equals(unit2.second)) {
                        return unit1.second.compareTo(unit1.second);
                    } else if (!unit1.first.type.equals(unit2.first.type)) {
                        return -unit1.first.type.compareTo(unit2.first.type);
                    } else {
                        return unit1.first.location.sector.name.compareTo(
                                unit2.first.location.sector.name);
                    }
                }
            };

    public AdjustmentAdjudicator(Variant variant, Season season) {
        super(variant, season);
    }

    @Override
    protected void perform(SeasonBuilder seasonBuilder) {
        seasonBuilder.addUnits(season.getAllUnits()).setSupplyOwnership(season.getSupplyCenters());

        int[] remainingAdjustments = getRemainingAdjustments();
        for (Order order : season.getAllOrders()) {
            Country country = order.unit.country;
            switch (order.type) {
                case BUILD:
                    if (remainingAdjustments[country.id] <= 0
                            || !variant.isBuildable(country, order.unit.location.sector)) {
                        order.setResolution(Order.Resolution.FAILS);
                    } else {
                        --remainingAdjustments[country.id];
                        seasonBuilder.addUnit(order.unit);
                    }
                    break;
                case REMOVE:
                case DEFAULT_REMOVE:
                    if (remainingAdjustments[country.id] >= 0) {
                        order.setResolution(Order.Resolution.FAILS);
                    } else if (season.getUnit(order.unit.location.sector) == null) {
                        order.setResolution(Order.Resolution.NO_SUCH_UNIT);
                    } else {
                        ++remainingAdjustments[country.id];
                        seasonBuilder.removeUnit(order.unit.location);
                    }
                    break;
                default:
                    throw new IllegalStateException();
            }
        }

        for (Country country : variant.getCountries()) {
            if (remainingAdjustments[country.id] < 0) {
                List<Unit> units = season.getUnits(country);
                // sanity check for too many adjustments
                if (remainingAdjustments[country.id] + units.size() < 0) {
                    remainingAdjustments[country.id] = -units.size();
                }

                Map<Sector, Integer> sectorDistances = constructReachabilityGraph(country);

                List<Pair<Unit, Integer>> unitDistances = new ArrayList<>();
                for (Unit unit : units) {
                    int distance = sectorDistances.containsKey(unit.location.sector)
                            ? sectorDistances.get(unit.location.sector)
                            : Integer.MAX_VALUE;
                    unitDistances.add(new Pair<>(unit, distance));
                }

                Collections.sort(unitDistances, REMOVE_UNIT_COMPARATOR);

                for (int i = 0; i < -remainingAdjustments[country.id]; ++i) {
                    Unit unit = unitDistances.get(i).first;
                    season.addOrder(new Order(unit, Order.Type.DEFAULT_REMOVE));
                    seasonBuilder.removeUnit(unit.location);
                }
                remainingAdjustments[country.id] = 0;
            }
        }

        seasonBuilder.setAdjustmentCount(remainingAdjustments);
    }

    private Map<Sector, Integer> constructReachabilityGraph(Country country) {
        Map<Sector, Integer> sectorDistances = new HashMap<>();
        for (Sector sector : country.getHomeCenters()) {
            sectorDistances.put(sector, 0);
        }

        Queue<Sector> sectorsToProcess = new ArrayDeque<>(country.getHomeCenters());
        int distance = 1;
        int remainingSectorCount = sectorsToProcess.size();
        while (!sectorsToProcess.isEmpty()) {
            if (remainingSectorCount-- == 0) {
                remainingSectorCount = sectorsToProcess.size();
                ++distance;
            }
            Sector sector = sectorsToProcess.remove();
            for (Sector.Coast coast : sector.getAvailableCoasts()) {
                for (Location location : sector.getAllAdjacencies(coast)) {
                    if (!sectorDistances.containsKey(location.sector)) {
                        sectorDistances.put(location.sector, distance);
                        sectorsToProcess.add(location.sector);
                    }
                }
            }
        }
        return sectorDistances;
    }

}
