package com.realpolitik.rp.reader;

import android.content.res.AssetManager;

import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Variant;

import java.io.IOException;
import java.text.ParseException;

class MapFileParser extends SmartParser {
    private final Variant variant;

    public MapFileParser(Variant variant, AssetManager assetManager, String assetUri) throws IOException {
        super(assetManager, assetUri);
        this.variant = variant;
    }


    public void parse() throws IOException, ParseException {
        int sectorId = 0;
        try {
            while (true) {
                Sector sector = parseSector(sectorId++);
                if (sector == null) break;
                variant.addSector(sector);
            }

            //noinspection StatementWithEmptyBody
            while (parseSectorAdjacency()) ;
        } catch (RuntimeException | ReadException e) {
            ParseException ex = new ParseException("Failed to parse Map: " + e.getMessage(),
                    getLineNumber());
            ex.initCause(e);
            throw ex;
        }
    }

    private Sector parseSector(int sectorId) throws ParseException, IOException, ReadException {
        if (peekChar() == '-') {
            skipLine();
            return null;
        }

        String sectorName = getUntilChar(',');
        skipWhitespace();
        int sectorType = parseSectorType(sectorName);
        Sector sector = null;
        while (!isAtEndOfLine()) {
            String abbrev = getUntilChar(' ');
            Sector sectorByAbbrev = variant.getSectorByAbbrev(abbrev);
            if (sectorByAbbrev != null) {
                throw new AmbiguousSectorException(abbrev, sectorName, sectorByAbbrev.name);
            }
            if (sector == null) {
                sector = new Sector(sectorId, sectorName, abbrev, sectorType);
            }
            variant.addSectorAbbrev(sector, abbrev);
        }


        return sector;
    }

    private int parseSectorType(String sectorName) throws ParseException, IOException {
        int sectorType = Sector.TYPE_DEFAULT;
        while (true) {
            char ch = getChar();
            switch (ch) {
                case 'l':
                    sectorType |= Sector.TYPE_LAND;
                    break;
                case 'w':
                    break;
                case 'x':
                    sectorType |= Sector.TYPE_SUPPLY;
                    break;
                case ' ':
                    return sectorType;
                default:
                    if (Character.isUpperCase(ch)) {
                        /* We don't check whether the country actually exists, as this info
                         * is also in the CNT file. */
                        sectorType |= Sector.TYPE_SUPPLY;
                    } else {
                        throw new ParseException(String.format("Sector %s has unknown type %c",
                                sectorName, ch), getLineNumber());
                    }
            }
        }
    }

    private boolean parseSectorAdjacency() throws ParseException, IOException, ReadException {
        if (peekChar() == '-') {
            skipLine();
            return false;
        }

        Sector sector = variant.getSectorByAbbrev(getUntilChar('-'));
        Sector.Coast coast = TokenParser.parseCoast(getChar());
        skipUntilChar(':').skipWhitespace();

        while (!isAtEndOfLine()) {
            sector.addAdjacent(coast, TokenParser.parseLocation(variant, getUntilChar(' '),
                    coast == Sector.Coast.ARMY ? Sector.Coast.ARMY : Sector.Coast.FLEET));
        }

        return true;
    }

}
