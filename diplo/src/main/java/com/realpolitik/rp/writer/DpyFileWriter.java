package com.realpolitik.rp.writer;

import com.realpolitik.database.DatabaseController;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Season;

import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

public class DpyFileWriter {

    public static void write(DatabaseController controller, PrintWriter writer, Game game) {
        writeGameInfo(writer, game.gameInfo);
        writeCurrentSeasonInfo(writer, game.variant, game.getLastSeason());
        writer.println("v2");
        writer.println(game.gameInfo.seasons);
        for (int i = 0; i < game.gameInfo.seasons; ++i) {
            writer.write(controller.getSeason(game.gameInfo.gameId, i).data);
        }

        writer.println(-1);
        OrderWriter.writeOrders(writer, game.getCurrentSeason(), game.variant,
                null, OrderWriter.FLAG_PREPEND_COUNTRY);
    }

    /**
     * This is used for past seasons, currently not needed, as there is no resolution,
     * keeping for future.
     */
    public static void writePastSeasonInfo(PrintWriter writer, Variant variant, Season season) {
        writer.print(season.phase.id);
        writer.print(' ');
        writer.println(season.year);

        writeSeasonUnits(writer, variant, season);
        writeSeasonCenters(writer, variant, season);
        writeAdjustmentCount(writer, variant, season);
        writeRetreats(writer, season.getAllRetreats());
        OrderWriter.writeOrders(writer, season, variant,
                null, OrderWriter.FLAG_PREPEND_COUNTRY | OrderWriter.FLAG_USE_ABBREVS);
    }

    private static void writeAdjustmentCount(PrintWriter writer, Variant variant,
                                             Season season) {
        writer.println(season.getAdjustmentCount());
        List<Country> countries = variant.getCountries();
        int count = countries.size();
        for (Country country : countries) {
            writer.print(season.getAdjustmentCount(country));
            writer.write((--count == 0) ? '\n' : ' ');
        }
    }

    private static void writeSeasonCenters(PrintWriter writer, Variant variant,
                                           Season season) {
        for (Sector sector : variant.getSectors()) {
            if (sector.isSupply()) {
                Country country = season.getSupplyCenterOwner(sector);
                writer.write(country == null ? '-' : country.initial);
            }
        }
        writer.println();
    }

    private static void writeSeasonUnits(PrintWriter writer, Variant variant,
                                         Season season) {
        for (Sector sector : variant.getSectors()) {
            Unit unit = season.getUnit(sector);
            if (unit == null) {
                writer.write('-');
                continue;
            }

            writer.print(unit.country.initial);
            switch (unit.location.coast) {
                case FLEET:
                    break;
                case ARMY:
                    writer.write('a');
                    break;
                case SOUTH:
                    writer.write('s');
                    break;
                case NORTH:
                    writer.write('n');
                    break;
                case WEST:
                    writer.write('w');
                    break;
                case EAST:
                    writer.write('e');
                    break;
            }
        }
        writer.println();
    }

    private static void writeGameInfo(PrintWriter writer, GameInfo gameInfo) {
        writer.println(1); // version
        writer.println(gameInfo.gameName);
        writer.println(gameInfo.variantName);
    }

    private static void writeCurrentSeasonInfo(PrintWriter writer, Variant variant,
                                               Season currentSeason) {
        writer.print(currentSeason.phase.name);
        writer.write(' ');
        writer.println(currentSeason.year);
        writer.println(0); // ignored line

        writer.println(variant.getCountries().size());

        for (Country country : variant.getCountries()) {
            writer.print(currentSeason.getAdjustmentCount(country));
            writer.println();

            writeCurrentCenters(writer, variant, currentSeason, country);
            writeCurrentUnits(writer, currentSeason, country);
        }

        writeRetreats(writer, currentSeason.getAllRetreats());
    }

    private static void writeRetreats(PrintWriter writer, List<Dislodge> dislodges) {
        if (dislodges == null) {
            writer.println(0);
            return;
        }

        writer.println(dislodges.size());
        for (Dislodge dislodge : dislodges) {
            writer.print(dislodge.unit.country.adjective);
            writer.write(' ');
            OrderWriter.writeUnit(writer, dislodge.unit, OrderWriter.FLAG_USE_ABBREVS);
            for (Location location : dislodge.locations) {
                writer.write(' ');
                OrderWriter.writeLocation(writer, location, OrderWriter.FLAG_USE_ABBREVS);
            }
            writer.println();
        }
    }

    private static void writeCurrentCenters(PrintWriter writer, Variant variant, Season season,
                                            Country country) {
        Country[] supplyCenters = season.getSupplyCenters();
        List<Sector> sectors = variant.getSectors();
        boolean firstSector = true;
        for (Sector sector : sectors) {
            if (supplyCenters[sector.id] == country) {
                if (firstSector) {
                    firstSector = false;
                } else {
                    writer.print(", ");
                }
                writer.print(sector.abbrev);
            }
        }

        if (!firstSector) {
            writer.print(".");
        }
        writer.println();
    }

    private static void writeCurrentUnits(PrintWriter writer, Season season,
                                          Country country) {
        List<Unit> units = season.getUnits(country);
        Collections.sort(units);
        int count = units.size();
        for (Unit unit : units) {
            OrderWriter.writeUnit(writer, unit, OrderWriter.FLAG_USE_ABBREVS);
            if (--count != 0) {
                writer.print(", ");
            }
        }
        writer.println();
    }


}
