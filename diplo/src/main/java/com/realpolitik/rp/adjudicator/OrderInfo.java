package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.season.Season;
import com.realpolitik.util.Assertions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class OrderInfo {
    private final OrderInfo[] orders;
    private final Season season;

    Order order;
    Order.Resolution resolution = Order.Resolution.SUCCESS;

    private boolean attackedUnitSupportsAgainstUs;

    private boolean isProcessing;
    private boolean isProcessed;
    private OrderInfo strongestAttacker;

    private boolean isAnyAttackHereCutting;
    private boolean isAttackHereSuccessful;
    /**
     * This is a move and it has been adjudicated as the single strongest attack to target sector,
     * but has not sufficient strength to dislodge target unit.
     */
    private boolean isMoveStrongestFailure;
    /**
     * This is a move and it has been adjudicated as successful.
     * Invariant: !isMoveSuccessful || !isMoveStrongestFailure
     */
    boolean isMoveSuccessful;
    /**
     * A bounce occurred in this sector.
     */
    private boolean bouncedHere;
    /**
     * This is a move and it is the strongest attack to target sector,
     * but others were as strong.
     */
    private boolean moveHasBounced;
    /**
     * This is a move and unit in target sector moves the same way.
     */
    private boolean isCounterAttack;

    private List<OrderInfo> convoyFleets;
    private List<OrderInfo> supporters;
    private List<OrderInfo> attackers;
    private OrderInfo targetOrder;
    List<Location> dislodgeLocations;

    public OrderInfo(OrderInfo[] orders, Season season) {
        this.orders = orders;
        this.season = season;
        isProcessing = false;
        isProcessed = false;
        isAttackHereSuccessful = false;
    }

    public void preProcess() {
        if (order == null) {
            return;
        }

        Unit targetUnit;
        switch (order.type) {
            case NO_MOVE_RECEIVED:
                break;
            case HOLD:
                break;
            case MOVE:
                if (order.unit.type == Unit.Type.ARMY && order.targetLocation.sector.isSea()) {
                    this.resolution = Order.Resolution.FAILS;
                } else if (order.unit.type == Unit.Type.FLEET
                        && order.targetLocation.sector.isLandlocked()) {
                    this.resolution = Order.Resolution.FAILS;
                } else if (!isMovePossibleWithProperConvoys()) {
                    this.resolution = Order.Resolution.FAILS;
                } else {
                    targetOrder = orders[order.targetLocation.sector.id];
                    if (targetOrder.attackers == null) {
                        targetOrder.attackers = new LinkedList<>();
                    }
                    targetOrder.attackers.add(this);
                    if (targetOrder.order != null
                            && (targetOrder.order.type == Order.Type.SUPPORT_MOVE
                            || targetOrder.order.type == Order.Type.CONVOY)
                            && targetOrder.order.targetLocation.sector == order.unit.location.sector) {
                        attackedUnitSupportsAgainstUs = true;
                    }
                }

                break;
            case SUPPORT_MOVE:
                targetUnit = season.getUnit(order.targetUnit.location.sector);
                if (targetUnit == null) {
                    this.resolution = Order.Resolution.NO_SUCH_UNIT;
                } else if (!orders[targetUnit.location.sector.id].isMoveTo(order.targetLocation)) {
                    this.resolution = Order.Resolution.VOID;
                } else if (!order.unit.location.isReachable(order.targetLocation.sector)) {
                    this.resolution = Order.Resolution.FAILS;
                } else {
                    OrderInfo target = orders[targetUnit.location.sector.id];
                    if (target.supporters == null) {
                        target.supporters = new LinkedList<>();
                    }
                    orders[targetUnit.location.sector.id].supporters.add(this);
                }

                break;
            case SUPPORT_HOLD:
                targetUnit = season.getUnit(order.targetUnit.location.sector);
                if (targetUnit == null) {
                    this.resolution = Order.Resolution.NO_SUCH_UNIT;
                } else if (orders[targetUnit.location.sector.id].order.type == Order.Type.MOVE) {
                    this.resolution = Order.Resolution.ORDERED_TO_MOVE;
                } else if (!order.unit.location.isReachable(order.targetUnit.location.sector)) {
                    this.resolution = Order.Resolution.FAILS;
                } else {
                    OrderInfo target = orders[targetUnit.location.sector.id];
                    if (target.supporters == null) {
                        target.supporters = new LinkedList<>();
                    }
                    orders[targetUnit.location.sector.id].supporters.add(this);
                }

                break;
            case CONVOY:
                targetUnit = season.getUnit(order.targetUnit.location.sector);
                if (targetUnit == null) {
                    this.resolution = Order.Resolution.NO_SUCH_UNIT;
                } else if (order.unit.type != Unit.Type.FLEET || targetUnit.type != Unit.Type.ARMY) {
                    this.resolution = Order.Resolution.INVALID_UNIT;
                } else if (!order.unit.location.sector.isSea()
                        || !order.targetUnit.location.sector.isCoastal()
                        || !order.targetLocation.sector.isCoastal()) {
                    this.resolution = Order.Resolution.INVALID_SECTOR;
                } else if (!orders[targetUnit.location.sector.id].isMoveTo(order.targetLocation)) {
                    this.resolution = Order.Resolution.VOID;
                } else {
                    OrderInfo target = orders[targetUnit.location.sector.id];
                    if (target.convoyFleets == null) {
                        target.convoyFleets = new LinkedList<>();
                    }
                    target.convoyFleets.add(this);
                }

                break;
            default:
                throw new IllegalStateException();
        }
    }

    public void process() {
        if (isProcessed || isProcessing) {
            return;
        }

        isProcessing = true;
        if (attackedUnitSupportsAgainstUs) {
            targetOrder.process();
            Assertions.checkState(targetOrder.isProcessed);
        }

        if (attackers == null) {
            isProcessed = true;
            isProcessing = false;
            return;
        }

        if (order != null && order.type == Order.Type.MOVE && attackers.contains(targetOrder)) {
            isCounterAttack = true;
            int ourStrength = offenseStrength();
            int counterStrength = targetOrder.offenseStrength();
            if (counterStrength < ourStrength) {
                targetOrder.process();
                Assertions.checkState(targetOrder.isProcessed);
            }
        }

        int strongestAttack = 0;
        for (OrderInfo attacker : attackers) {
            if (isCounterAttack && isMoveSuccessful && targetOrder == attacker
                    || !(attacker.isMovePossibleWithoutConvoy()
                    || attacker.convoyPathExists(null))) {
                continue;
            }
            int strength = attacker.offenseStrength();
            isAnyAttackHereCutting |= (isHostile(attacker) && strength > 0
                    && (order == null || order.targetLocation == null
                    || order.targetLocation.sector != attacker.order.unit.location.sector));
            if (strength == strongestAttack) {
                strongestAttacker = null;
                bouncedHere = true;
            } else if (strength > strongestAttack) {
                strongestAttack = strength;
                strongestAttacker = attacker;
                bouncedHere = false;
            }
        }

        if (bouncedHere) {
            for (OrderInfo attacker : attackers) {
                if (attacker.offenseStrength() == strongestAttack) {
                    attacker.moveHasBounced = true;
                }
            }
        }

        if (strongestAttacker != null) {
            int offenseStrength = strongestAttacker.friendlyOffenseStrength(this);
            isAttackHereSuccessful = offenseStrength > defenseStrength() &&
                    (!isCounterAttack || strongestAttacker != targetOrder
                            || offenseStrength > offenseStrength());
            strongestAttacker.isMoveStrongestFailure = !isAttackHereSuccessful;
            strongestAttacker.isMoveSuccessful = isAttackHereSuccessful;
        }

        isProcessed = true;
        isProcessing = false;
    }

    public void postProcess() {
        if (order == null || isMoveSuccessful) {
            // there is no order or it has been already adjudicated
            return;
        } else if (isCycleOrChainOfStrongestFailedMoves()) {
            OrderInfo target = this;
            do {
                target.isMoveSuccessful = true;
                target.isMoveStrongestFailure = false;
                target = target.targetOrder;
            } while (target != null && !target.isMoveSuccessful);
        } else if (isAttackHereSuccessful) {
            resolution = Order.Resolution.DISLODGED;
        } else if (isAnyAttackHereCutting
                && (order.type == Order.Type.SUPPORT_MOVE
                || order.type == Order.Type.SUPPORT_HOLD)) {
            resolution = Order.Resolution.CUT;
        } else if (order.type == Order.Type.MOVE) {
            resolution = moveHasBounced ? Order.Resolution.BOUNCE : Order.Resolution.FAILS;
        }
    }

    public void findDislodges() {
        if (resolution != Order.Resolution.DISLODGED) {
            return;
        }

        for (Location location : order.unit.location.getAllAdjacencies()) {
            if (canRetreatTo(location)) {
                if (dislodgeLocations == null) {
                    dislodgeLocations = new ArrayList<>();
                }
                dislodgeLocations.add(location);
            }
        }
        resolution = dislodgeLocations == null
                ? Order.Resolution.DISBANDED : Order.Resolution.DISLODGED;
    }

    private boolean canRetreatTo(Location location) {
        OrderInfo info = orders[location.sector.id];
        return strongestAttacker.order.unit.location.sector != location.sector // no counter-retreat
                && !info.isAttackHereSuccessful // no sector with an incoming unit
                && !info.bouncedHere            // no sector with bounce occurred
                && (info.order == null || info.isMoveSuccessful); // no sector with a unit
    }

    private int defenseStrength() {
        if (order == null || isMoveSuccessful) {
            return 0;
        } else if (order.type == Order.Type.MOVE) {
            // the move can be unprocessed, count as one and resolve loops later
            return 1;
        } else {
            return 1 + getUncutSupports();
        }
    }

    private int offenseStrength() {
        if (order.type != Order.Type.MOVE) {
            return 0;
        } else {
            return 1 + getUncutSupports();
        }
    }

    private int friendlyOffenseStrength(OrderInfo defender) {
        if (defender.order == null) {
            return offenseStrength();
        }

        Country country = defender.order.unit.country;
        if (order.type != Order.Type.MOVE) {
            return 0;
        } else if (order.unit.country == country) {
            return 0;
        } else {
            int strength = 1;
            if (supporters != null) {
                for (OrderInfo supporter : supporters) {
                    if (country != supporter.order.unit.country && !supporter.isSupportCut()) {
                        ++strength;
                    }
                }
            }
            return strength;
        }
    }

    private int getUncutSupports() {
        if (supporters == null) {
            return 0;
        }

        int strength = 0;
        for (OrderInfo supporter : supporters) {
            if (!supporter.isSupportCut()) {
                ++strength;
            }
        }
        return strength;
    }

    private boolean isMovePossibleWithoutConvoy() {
        return order.unit.location.isReachable(order.targetLocation);
    }

    private boolean isMovePossibleWithProperConvoys() {
        // if directly adjacent => true
        if (isMovePossibleWithoutConvoy()) {
            return true;
        }

        // fleets need to be directly adjacent
        if (order.unit.type == Unit.Type.FLEET) {
            return false;
        }

        // armies need to be convoyed from and to coastal provinces
        return !(!order.unit.location.sector.isCoastal()
                || !order.targetLocation.sector.isCoastal());
    }

    private boolean convoyPathExists(OrderInfo fleetToExclude) {
        if (convoyFleets == null) {
            return false;
        }

        Iterator<OrderInfo> iter = convoyFleets.iterator();
        while (iter.hasNext()) {
            OrderInfo fleet = iter.next();
            if (fleet == fleetToExclude) {
                iter.remove();
                continue;
            }

            fleet.process();
            if (fleet.isAttackHereSuccessful) {
                iter.remove();
            }
        }

        /* Do a BFS for convoy path to determine the path */
        List<OrderInfo> unusedConvoyFleets = new LinkedList<>(convoyFleets);
        Queue<Sector> bfsSectorQueue = new LinkedList<>();
        bfsSectorQueue.add(order.unit.location.sector);
        Sector targetSector = order.targetLocation.sector;
        while (!bfsSectorQueue.isEmpty()) {
            Sector sector = bfsSectorQueue.poll();
            iter = unusedConvoyFleets.iterator();
            while (iter.hasNext()) {
                OrderInfo fleet = iter.next();
                Location fleetLocation = fleet.order.unit.location;
                if (fleet.isProcessed && fleetLocation.isReachable(sector)) {
                    if (fleetLocation.isReachable(targetSector)) {
                        return true;
                    }
                    bfsSectorQueue.add(fleetLocation.sector);
                    iter.remove();
                }
            }
        }

        return false;
    }

    private boolean isMoveTo(Location location) {
        return order.type == Order.Type.MOVE && order.targetLocation.sector == location.sector;
    }

    private boolean isHostile(OrderInfo orderInfo) {
        return order == null || orderInfo.order == null
                || orderInfo.order.unit.country != order.unit.country;
    }

    private boolean isSupportCut() {
        if (isProcessed) {
            return (isAnyAttackHereCutting || isAttackHereSuccessful);
        }

        if (attackers == null) {
            return false;
        }

        for (OrderInfo attacker : attackers) {
            if (!isHostile(attacker)) {
                continue;
            }

            if (order.type == Order.Type.SUPPORT_MOVE
                    && order.targetLocation.sector.equals(attacker.order.unit.location.sector)) {
                /* We're supporting someone against this attacker, we need to process
                 * first to determine if we've been dislodged.
                 * Note that this cannot cause a cycle in resolution. */
                process();
                Assertions.checkState(isProcessed);
                return (isAnyAttackHereCutting || isAttackHereSuccessful);
            }

            if (order.type != Order.Type.SUPPORT_MOVE ||
                    attacker.isMovePossibleWithoutConvoy()) {
                return true;
            }

            // this Unit is attacked by convoy made with a Fleet
            // which is attacked with this Support
            if (attacker.convoyPathExists(targetOrder)) {
                return true;
            }
        }
        return false;
    }

    private boolean isCycleOrChainOfStrongestFailedMoves() {
        if (isCounterAttack || !isMoveStrongestFailure) {
            return false;
        }

        OrderInfo target = targetOrder;
        while (target != this && target.isMoveStrongestFailure) {
            target = target.targetOrder;
        }
        return target == this /* cycle */ || target.isMoveSuccessful /* chain */;
    }

}
