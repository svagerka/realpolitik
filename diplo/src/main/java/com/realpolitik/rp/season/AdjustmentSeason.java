package com.realpolitik.rp.season;

import com.realpolitik.rp.adjudicator.AdjustmentAdjudicator;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.orders.AdjustmentOrderParser;
import com.realpolitik.rp.reader.orders.OrderParser;

import java.util.Map;

public class AdjustmentSeason extends Season {
    AdjustmentSeason(Variant variant, Phase phase, int year,
                     int[] adjustmentCount, Country[] supplyCenters, Unit[] units,
                     Map<Sector, Order> orders) {
        super(variant, phase, year, adjustmentCount, supplyCenters, units, orders);
    }

    @Override
    public OrderParser getOrderParser() {
        return new AdjustmentOrderParser(variant);
    }

    @Override
    public Season resolve() {
        return new AdjustmentAdjudicator(variant, this).getNextSeason();
    }

    @Override
    public boolean hasOrders(Country country) {
        return adjustmentCount[country.id] != 0;
    }

}
