package com.realpolitik.rp.reader.orders;

import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;

import java.util.regex.Matcher;

public class RetreatOrderParser extends OrderParser {

    public RetreatOrderParser(Variant variant) {
        super(variant, new OrderMatcher[]{new EmptyMatcher(), new RetreatMatcher(),
                new RemoveMatcher(), new DisbandMatcher(), new NoMoveReceivedMatcher()});
    }

    static class RemoveMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return "Remove\\s+" + UNIT_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.DISBAND);
        }
    }

    static class DisbandMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return "disband\\s+" + UNIT_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.DISBAND);
        }
    }

    static class RetreatMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return MOVE_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            Location location =
                    parseLocation(variant, matcher, MATCHER_FIRST_ID, unit.type.getDefaultCoast());
            return new Order(unit, Order.Type.RETREAT, location);
        }
    }
}
