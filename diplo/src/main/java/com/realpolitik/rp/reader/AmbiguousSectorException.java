package com.realpolitik.rp.reader;

class AmbiguousSectorException extends ReadException {
    private final String abbrev;
    private final String sector1;
    private final String sector2;

    AmbiguousSectorException(String abbrev, String sector1, String sector2) {
        this.abbrev = abbrev;
        this.sector1 = sector1;
        this.sector2 = sector2;
    }

    @Override
    public String getMessage() {
        return String.format(
                "Abbreviation %s is ambiguous: \"%s\", \"%s\"",
                abbrev, sector1, sector2);
    }
}
