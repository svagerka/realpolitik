package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.season.SeasonBuilder;

abstract class Adjudicator {

    public Season getNextSeason() {
        int id = (season.phase.id % 5) + 1;
        int year = season.year + (id == 1 ? 1 : 0);

        SeasonBuilder seasonBuilder =
                new SeasonBuilder(variant, Phase.fromInt(id), year);
        perform(seasonBuilder);
        Season nextSeason = seasonBuilder.build();
        return seasonBuilder.getOrderCount() == 0
                ? nextSeason.resolve() : nextSeason;
    }


    final Season season;
    final Variant variant;

    Adjudicator(Variant variant, Season season) {
        this.season = season;
        this.variant = variant;
    }

    int[] getRemainingAdjustments() {
        int[] remainingAdjustments = new int[variant.getCountries().size()];
        for (Country country : variant.getCountries()) {
            remainingAdjustments[country.id] = season.getAdjustmentCount(country);
        }
        return remainingAdjustments;
    }


    abstract protected void perform(SeasonBuilder seasonBuilder);
}
