package com.realpolitik.rp.season;

public enum Phase {
    SPRING(1, "Spring"),
    SUMMER(2, "Summer"),
    FALL(3, "Fall"),
    AUTUMN(4, "Autumn"),
    WINTER(5, "Winter");

    public final int id;
    public final String name;

    Phase(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Phase fromString(String name) {
        for (Phase phase : Phase.values()) {
            if (phase.name.equalsIgnoreCase(name)) {
                return phase;
            }
        }
        return null;
    }

    public static Phase fromInt(int id) {
        for (Phase phase : Phase.values()) {
            if (phase.id == id) {
                return phase;
            }
        }
        return null;
    }

    public enum PhaseType {
        MOVEMENT,
        RETREAT,
        ADJUSTMENT;

        public static PhaseType fromSeason(Phase phase) {
            switch (phase) {
                case SPRING:
                case FALL:
                    return PhaseType.MOVEMENT;
                case SUMMER:
                case AUTUMN:
                    return PhaseType.RETREAT;
                case WINTER:
                    return PhaseType.ADJUSTMENT;
                default:
                    throw new IllegalStateException();
            }
        }
    }
}
