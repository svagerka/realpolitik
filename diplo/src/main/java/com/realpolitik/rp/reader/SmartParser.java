package com.realpolitik.rp.reader;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

@SuppressWarnings("UnusedReturnValue")
class SmartParser {
    private final BufferedReader reader;

    private char[] line;
    private int pos;
    private int lineNumber;
    private boolean eof;

    SmartParser(byte[] buffer) throws IOException {
        reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buffer)));
        eof = false;
        loadNextLine();

    }

    SmartParser(AssetManager assetManager, String assetUri) throws IOException {
        reader = new BufferedReader(new InputStreamReader(assetManager.open(assetUri)));
        eof = false;
        loadNextLine();
    }

    int getInt() throws ParseException, IOException {
        skipWhitespace();
        if (eof) {
            throw new ParseException("Unexpected end of file", lineNumber);
        } else if (pos == line.length || line[pos] < '0' || line[pos] > '9') {
            throw new ParseException("Number expected", lineNumber);
        }

        int result = 0;
        while (pos < line.length && line[pos] >= '0' && line[pos] <= '9') {
            result *= 10;
            result += line[pos] - '0';
            ++pos;
        }
        return result;
    }

    char getChar() throws ParseException, IOException {
        char res = peekChar();
        ++pos;
        return res;
    }

    char peekChar() throws ParseException, IOException {
        if (eof || (pos == line.length && !loadNextLine())) {
            throw new ParseException("Unexpected end of file", lineNumber);
        }
        return line[pos];
    }

    String getUntilChar(char ch) throws ParseException, IOException {
        skipWhitespace();
        if (eof) {
            throw new ParseException("Character " + ch + " expected", lineNumber);
        }
        int startPos = pos;
        while (pos < line.length && line[pos] != ch) {
            ++pos;
        }

        String token = new String(line, startPos, pos - startPos);
        if (pos < line.length) {
            // read the separator as well
            ++pos;
        }
        return token;
    }

    SmartParser skipWhitespace() throws IOException {
        if (eof) {
            return this;
        }

        while (pos == line.length || line[pos] == ' ' || line[pos] == '\t') {
            if (pos == line.length) {
                if (!loadNextLine()) {
                    return this;
                }
            } else {
                ++pos;
            }
        }

        return this;
    }

    SmartParser skipLine() throws IOException {
        skipWhitespace().loadNextLine();
        return this;
    }

    SmartParser skipUntilChar(char ch) throws ParseException, IOException {
        skipWhitespace();
        if (eof) {
            throw new ParseException("Unexpected end of file", lineNumber);
        }

        while (pos < line.length && line[pos] != ch) {
            if (pos == line.length) {
                throw new ParseException("Character " + ch + " expected", lineNumber);
            }
            ++pos;
        }


        if (pos < line.length) {
            // read the separator as well
            ++pos;
        }
        return this;
    }

    boolean isAtEndOfLine() {
        while (pos < line.length && line[pos] == ' ') {
            ++pos;
        }
        return (pos == line.length);
    }

    boolean isEof() {
        return eof;
    }

    int getLineNumber() {
        return lineNumber;
    }

    private boolean loadNextLine() throws IOException {
        String lineString = reader.readLine();
        if (lineString == null) {
            eof = true;
            return false;
        } else {
            line = lineString.toCharArray();
            pos = 0;
            ++lineNumber;
            return true;
        }
    }


}
