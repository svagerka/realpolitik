package com.realpolitik.rp.reader.orders;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.TokenParser;
import com.realpolitik.util.Assertions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OrderParser {
    private final OrderMatcher[] matchers;
    private final Variant variant;
    private Country currentCountry;

    OrderParser(Variant variant, OrderMatcher[] matchers) {
        this.variant = variant;
        currentCountry = null;
        this.matchers = matchers;
    }

    public Order parseOrder(String line) {
        Matcher matcher;
        for (OrderMatcher orderMatcher : matchers) {
            matcher = orderMatcher.pattern.matcher(line);
            if (!matcher.matches()) {
                continue;
            }

            if (matcher.group(1) != null) {
                currentCountry = variant.getCountryByName(matcher.group(2));
            }

            Assertions.notNull(currentCountry);
            return orderMatcher.parseOrder(variant, currentCountry, matcher);
        }

        return null;
    }

    abstract static class OrderMatcher {
        final Pattern pattern;

        static final String SECTOR_REGEXP = "(\\w+([- ]\\w+)*)(\\((\\w+)\\))?";
        static final String UNIT_REGEXP = "([AF])\\s+" + SECTOR_REGEXP;
        static final String MOVE_REGEXP = UNIT_REGEXP + "\\s+\\-\\s+" + SECTOR_REGEXP;
        static final int MATCHER_SECTOR_GROUPS = 4;
        static final int MATCHER_UNIT_GROUPS = 1;
        static final int MATCHER_FIRST_ID = 3 + MATCHER_SECTOR_GROUPS + MATCHER_UNIT_GROUPS;


        OrderMatcher() {
            pattern = Pattern.compile(
                    "((\\w+):)?\\s*" + getRegexp() + "\\s*(\\(\\*([^*]+)\\*\\))?");
        }

        static Unit parseUnit(Variant variant, Country country, Matcher matcher, int offset) {
            Unit.Type type = TokenParser.parseUnitType(matcher.group(offset));
            Location location = parseLocation(variant, matcher, offset + 1, type.getDefaultCoast());
            return new Unit(type, location, country);
        }

        static Location parseLocation(Variant variant, Matcher matcher, int offset,
                                      Sector.Coast defaultCoast) {
            Sector sector = variant.getSectorByAbbrev(matcher.group(offset));
            if (sector == null) {
                sector = variant.getSectorByName(matcher.group(offset));
            }
            Sector.Coast coast = (matcher.group(offset + 2) != null)
                    ? TokenParser.parseCoast(matcher.group(offset + 3)) : defaultCoast;
            return new Location(sector, coast);
        }

        public Order parseOrder(Variant variant, Country country, Matcher matcher) {
            Unit unit = OrderMatcher.parseUnit(variant, country, matcher, 3);
            Order order = parseOrder(variant, unit, matcher);
            if (matcher.group(matcher.groupCount() - 1) != null) {
                String label = matcher.group(matcher.groupCount());
                Order.Resolution resolution = Order.Resolution.getByLabel(label);
                if (resolution != null) {
                    order.setResolution(resolution);
                }
            }
            return order;
        }

        abstract protected String getRegexp();

        abstract protected Order parseOrder(Variant variant, Unit unit, Matcher matcher);

    }

    static class EmptyMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return "";
        }

        @Override
        public Order parseOrder(Variant variant, Country country, Matcher matcher) {
            return null;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return null;
        }
    }

    static class NoMoveReceivedMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return UNIT_REGEXP + ", no move received";
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.NO_MOVE_RECEIVED);
        }
    }
}
