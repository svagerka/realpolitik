package com.realpolitik.rp.season;

import com.realpolitik.rp.adjudicator.RetreatAdjudicator;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Dislodge;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Sector;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.reader.orders.OrderParser;
import com.realpolitik.rp.reader.orders.RetreatOrderParser;
import com.realpolitik.util.Assertions;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class RetreatSeason extends Season {
    private final List<Dislodge> dislodges;

    RetreatSeason(Variant variant, Phase phase, int year, List<Dislodge> dislodges,
                  int[] adjustmentCount, Country[] supplyCenters, Unit[] units,
                  Map<Sector, Order> orders) {
        super(variant, phase, year, adjustmentCount, supplyCenters, units, orders);
        this.dislodges = dislodges;
    }

    @Override
    public OrderParser getOrderParser() {
        return new RetreatOrderParser(variant);
    }

    @Override
    public Season resolve() {
        return new RetreatAdjudicator(variant, this).getNextSeason();
    }

    @Override
    public int getUnitCount(Country country) {
        int count = super.getUnitCount(country);
        for (Dislodge dislodge : dislodges) {
            if (country == dislodge.unit.country) {
                ++count;
            }
        }
        return count;
    }

    @Override
    public List<Dislodge> getAllRetreats() {
        return Collections.unmodifiableList(dislodges);
    }

    @Override
    public Dislodge getRetreat(Sector sector) {
        Assertions.notNull(sector);
        for (Dislodge dislodge : dislodges) {
            if (dislodge.unit.location.sector == sector) {
                return dislodge;
            }
        }
        return null;
    }
}
