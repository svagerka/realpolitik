package com.realpolitik.rp.reader;

import java.io.IOException;
import java.io.LineNumberReader;

class CopyingParser extends Parser {
    private final StringBuilder builder;

    public CopyingParser(LineNumberReader reader) {
        super(reader);
        this.builder = new StringBuilder();
    }

    public String toString() {
        return builder.toString();
    }

    @Override
    public String readLine() throws IOException {
        String line = super.readLine();
        if (line != null) {
            builder.append(line).append('\n');
        }
        return line;
    }

}
