package com.realpolitik.controller;

import android.graphics.Point;
import android.graphics.PointF;

public class PanZoomHelper {
    public interface Listener {
        @SuppressWarnings("UnusedParameters")
        void onPanZoomChanged(float scale, PointF pan);
    }

    private static final double MIN_ZOOM_DELTA_TO_REDRAW = 0.02;
    private static final int MIN_PAN_DELTA_TO_REDRAW = 1;
    private static final float MAX_ZOOM = 6f;

    private final Listener listener;

    private float zoom = 0f;
    private final PointF pan = new PointF();
    private float lastPaintedZoom;
    private final PointF lastPaintedPan = new PointF();

    private float zoomFocusX = 0f;
    private float zoomFocusY = 0f;

    private int viewWidth = 0;
    private int viewHeight = 0;
    private int mapWidth = 0;
    private int mapHeight = 0;

    public PanZoomHelper(Listener listener) {
        this.listener = listener;
    }

    public float getZoom() {
        return zoom;
    }

    public PointF getPan() {
        return pan;
    }

    public void setMapSize(int width, int height) {
        if (mapWidth != width || mapHeight != height) {
            zoom = 0f;
            pan.set(0f, 0f);
            mapWidth = width;
            mapHeight = height;
            constraintPanZoom();
        }
    }

    public void setViewSize(int width, int height) {
        viewWidth = width;
        viewHeight = height;
        constraintPanZoom();
    }

    public boolean pan(float distanceX, float distanceY) {
        pan.offset(distanceX, distanceY);
        return maybeNotify();
    }

    public boolean scale(float scaleFactor, float focusX, float focusY) {
        this.zoom *= scaleFactor;
        zoomFocusX = focusX;
        zoomFocusY = focusY;
        return maybeNotify();
    }

    public Point convertToMapCoords(PointF viewPos) {
        return new Point((int) ((pan.x + viewPos.x) / zoom), (int) ((pan.y + viewPos.y) / zoom));
    }

    private boolean maybeNotify() {
        if (viewWidth == 0 || viewHeight == 0 || mapWidth == 0 || mapHeight == 0) {
            return false;
        }

        constraintPanZoom();
        if (hasPanOrZoomChanged()) {
            storePanZoomState();
            listener.onPanZoomChanged(zoom, pan);
            return true;
        } else {
            return false;
        }
    }

    private void constraintPanZoom() {
        if (viewWidth == 0 || viewHeight == 0 || mapWidth == 0 || mapHeight == 0) {
            return;
        }

        float minZoom = Math.min(
                (float) viewWidth / mapWidth,
                (float) viewHeight / mapHeight
        );
        zoom = Math.max(minZoom, Math.min(zoom, MAX_ZOOM));

        if (Math.abs(zoom - lastPaintedZoom) > MIN_ZOOM_DELTA_TO_REDRAW && lastPaintedZoom != 0) {
            float zoomFactor = zoom / lastPaintedZoom;
            pan.x = (zoomFocusX * (zoomFactor - 1)) + (zoomFactor * pan.x);
            pan.y = (zoomFocusY * (zoomFactor - 1)) + (zoomFactor * pan.y);
        }

        float maxX = mapWidth * zoom - viewWidth;
        float maxY = mapHeight * zoom - viewHeight;
        pan.x = (maxX <= 0) ? (maxX / 2) : Math.max(0, Math.min(pan.x, maxX));
        pan.y = (maxY <= 0) ? (maxY / 2) : Math.max(0, Math.min(pan.y, maxY));
    }

    private boolean hasPanOrZoomChanged() {
        return (Math.abs(zoom - lastPaintedZoom) > MIN_ZOOM_DELTA_TO_REDRAW
                || Math.abs(pan.x - lastPaintedPan.x) > MIN_PAN_DELTA_TO_REDRAW
                || Math.abs(pan.y - lastPaintedPan.y) > MIN_PAN_DELTA_TO_REDRAW);
    }

    private void storePanZoomState() {
        lastPaintedZoom = zoom;
        lastPaintedPan.x = pan.x;
        lastPaintedPan.y = pan.y;
    }
}
