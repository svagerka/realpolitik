package com.realpolitik.rp.adjudicator;

import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.reader.ReadException;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.SeasonBuilder;
import com.realpolitik.test.AdjudicatorTest;
import com.realpolitik.test.RobolectricGradleTestRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.realpolitik.rp.common.Order.Resolution.DISBANDED;
import static com.realpolitik.rp.common.Order.Resolution.DISLODGED;
import static com.realpolitik.rp.common.Order.Resolution.FAILS;
import static com.realpolitik.rp.common.Order.Resolution.INVALID_SECTOR;
import static com.realpolitik.rp.common.Order.Resolution.SUCCESS;

@RunWith(RobolectricGradleTestRunner.class)
public class ConvoyTest extends AdjudicatorTest {

    public ConvoyTest() throws ReadException {
    }

    @Test
    public void testArmyCanBeConvoyedAcrossSeaSector() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "cxd", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxe - dxd");
        addOrder("One: F cxd Convoys A bxe - dxd");
        season.resolve();
        assertOrderResolution("bxe", SUCCESS);
        assertOrderResolution("cxd", SUCCESS);
    }

    @Test
    public void testFleetInCoastalProvinceCannotConvoy() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "bxd", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxe - bxc");
        addOrder("One: F bxd Convoys A bxe - bxc");
        season.resolve();
        assertOrderResolution("bxe", FAILS);
        assertOrderResolution("bxd", INVALID_SECTOR);
    }


    @Test
    public void testConvoyFailsIfConvoysDoNotFormPath() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "dxe", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxe - dxd");
        addOrder("One: F dxe Convoys A bxe - dxd");
        season.resolve();
        assertOrderResolution("bxe", FAILS);
        assertOrderResolution("dxe", SUCCESS);
    }

    @Test
    public void testArmyCanBeConvoyedAcrossMultipleSeaSectors() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "cxe", 0)
                .addUnit(Unit.Type.FLEET, "dxe", 0);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxe - dxd");
        addOrder("One: F cxe Convoys A bxe - dxd");
        addOrder("One: F dxe Convoys A bxe - dxd");
        season.resolve();
        assertOrderResolution("bxe", SUCCESS);
        assertOrderResolution("cxe", SUCCESS);
        assertOrderResolution("dxe", SUCCESS);
    }

    @Test
    public void testArmyIsNotConvoyedWhenFleetIsDislodged() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "cxe", 0)
                .addUnit(Unit.Type.FLEET, "dxe", 0)
                .addUnit(Unit.Type.FLEET, "exd", 1)
                .addUnit(Unit.Type.FLEET, "exe", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxe - dxd");
        addOrder("One: F cxe Convoys A bxe - dxd");
        addOrder("One: F dxe Convoys A bxe - dxd");
        addOrder("Two: F exd - dxe");
        addOrder("Two: F exe Supports F exd - dxe");
        season.resolve();
        assertOrderResolution("exd", SUCCESS);
        assertOrderResolution("exe", SUCCESS);
        assertOrderResolution("dxe", DISLODGED);
        assertOrderResolution("bxe", FAILS);
        assertOrderResolution("cxe", SUCCESS);
    }

    @Test
    public void testArmyIsConvoyedIfSuperfluousFleetIsDislodged() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxe", 0)
                .addUnit(Unit.Type.FLEET, "cxe", 0)
                .addUnit(Unit.Type.FLEET, "dxe", 0)
                .addUnit(Unit.Type.FLEET, "cxd", 0)
                .addUnit(Unit.Type.FLEET, "exd", 1)
                .addUnit(Unit.Type.FLEET, "exe", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxe - dxd");
        addOrder("One: F cxe Convoys A bxe - dxd");
        addOrder("One: F cxd Convoys A bxe - dxd");
        addOrder("One: F dxe Convoys A bxe - dxd");
        addOrder("Two: F exd - dxe");
        addOrder("Two: F exe Supports F exd - dxe");
        season.resolve();
        assertOrderResolution("exd", SUCCESS);
        assertOrderResolution("exe", SUCCESS);
        assertOrderResolution("dxe", DISBANDED);
        assertOrderResolution("bxe", SUCCESS);
        assertOrderResolution("cxe", SUCCESS);
        assertOrderResolution("cxd", SUCCESS);
    }

    @Test
    public void testConvoyedAttackDoesNotProtectConvoyingFleet() throws ReadException {
        SeasonBuilder seasonBuilder = new SeasonBuilder(variant, Phase.SPRING, 1)
                .addUnit(Unit.Type.ARMY, "bxd", 0)
                .addUnit(Unit.Type.FLEET, "cxc", 0)
                .addUnit(Unit.Type.FLEET, "cxb", 0)
                .addUnit(Unit.Type.FLEET, "dxc/wc", 1)
                .addUnit(Unit.Type.FLEET, "dxd", 1);
        season = seasonBuilder.build();
        parser = season.getOrderParser();
        addOrder("One: A bxd - dxc");
        addOrder("One: F cxb Supports A bxd - dxc");
        addOrder("One: F cxc Convoys A bxd - dxc");
        addOrder("Two: F dxc(wc) Supports F dxd - cxc");
        addOrder("Two: F dxd - cxc");
        season.resolve();
        assertOrderResolution("cxb", SUCCESS);
        assertOrderResolution("dxd", SUCCESS);
        assertOrderResolution("dxc", SUCCESS);
        assertOrderResolution("cxc", DISLODGED);
        assertOrderResolution("bxd", FAILS);

    }

}
