package com.realpolitik.rp.common;

import android.graphics.Point;

import com.realpolitik.util.Assertions;

import java.util.List;

public class Location {
    public final Sector.Coast coast;
    public final Sector sector;

    public Location(Sector sector, Sector.Coast coast) {
        this.coast = Assertions.notNull(coast);
        this.sector = Assertions.notNull(sector);
    }

    public Point getUnitPosition() {
        return sector.getUnitPosition(coast);
    }

    public int getX() {
        return getUnitPosition().x;
    }

    public int getY() {
        return getUnitPosition().y;
    }

    public boolean isReachable(Location location) {
        return sector.isReachable(coast, location);
    }

    public boolean isReachable(Sector sector) {
        return this.sector.isReachable(coast, sector);
    }

    public List<Location> getAllAdjacencies() {
        return sector.getAllAdjacencies(coast);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Location)) {
            return false;
        }

        Location location = (Location) o;
        return (location.coast == coast) && location.sector.equals(sector);
    }

    @Override
    public int hashCode() {
        return sector.hashCode();
    }

    @Override
    public String toString() {
        switch (coast) {
            case ARMY:
                return sector.toString() + "/mv";
            case FLEET:
                return sector.toString() + "/xc";
            case SOUTH:
                return sector.toString() + "/sc";
            case NORTH:
                return sector.toString() + "/nc";
            case WEST:
                return sector.toString() + "/wc";
            case EAST:
                return sector.toString() + "/ec";
            default:
                throw new IllegalStateException();
        }
    }
}