package com.realpolitik.rp.reader.orders;

import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;

import java.util.regex.Matcher;

public class AdjustmentOrderParser extends OrderParser {
    public AdjustmentOrderParser(Variant variant) {
        super(variant, new OrderMatcher[]{new EmptyMatcher(), new BuildMatcher(),
                new RemoveMatcher(), new DefaultRemoveMatcher(), new NoMoveReceivedMatcher()});
    }

    static class BuildMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return "Build\\s+" + UNIT_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.BUILD);
        }
    }

    static class RemoveMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return "Remove\\s+" + UNIT_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.REMOVE);
        }
    }

    static class DefaultRemoveMatcher extends OrderMatcher {
        @Override
        protected String getRegexp() {
            return "Defaults, removing\\s+" + UNIT_REGEXP;
        }

        @Override
        protected Order parseOrder(Variant variant, Unit unit, Matcher matcher) {
            return new Order(unit, Order.Type.DEFAULT_REMOVE);
        }
    }
}
