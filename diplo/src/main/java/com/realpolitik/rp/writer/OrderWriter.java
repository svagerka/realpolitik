package com.realpolitik.rp.writer;

import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Location;
import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.common.Unit;
import com.realpolitik.rp.common.Variant;
import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;

import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

public class OrderWriter {
    public static final int FLAGS_DEFAULT = 0;
    public static final int FLAG_USE_ABBREVS = 1;
    public static final int FLAG_PREPEND_COUNTRY = 2;
    public static final int FLAG_USE_HTML = 4;

    public static void writeOrders(PrintWriter writer, Season season, Variant variant,
                                   boolean[] countriesToShare, int flags) {

        List<Order> orders = season.getAllOrders();
        Collections.sort(orders);

        List<Country> countries = variant.getCountries();

        int orderId = 0;

        for (Country country : countries) {
            int totalAdjustments = season.getAdjustmentCount(country);
            if (!season.hasOrders(country)) {
                // no orders for this country
                continue;
            }

            if (countriesToShare != null && !countriesToShare[country.id]) {
                // don't print any orders
                while (orderId < orders.size() && country == orders.get(orderId).unit.country) {
                    ++orderId;
                }
                continue;
            }

            // print country name
            if ((flags & FLAG_PREPEND_COUNTRY) == 0) {
                if ((flags & FLAG_USE_HTML) == FLAG_USE_HTML) {
                    writer.print("<b>");
                }
                writer.print(country.name);
                writer.print((flags & FLAG_USE_HTML) == FLAG_USE_HTML ? ":</b><br />" : ":\n");
            }

            int printedOrders = 0;
            while (orderId < orders.size() && country == orders.get(orderId).unit.country) {
                ++printedOrders;
                writeOrder(writer, orders.get(orderId++), flags);
                writer.print((flags & FLAG_USE_HTML) == FLAG_USE_HTML ? "<br />" : "\n");
            }
            while (season.getPhaseType() == Phase.PhaseType.ADJUSTMENT
                    && printedOrders++ < Math.abs(totalAdjustments)) {
                writer.print(totalAdjustments > 0 ? "Build\n" : "Remove\n");
                if ((flags & FLAG_USE_HTML) == FLAG_USE_HTML) {
                    writer.print("<br />");
                }
            }
            if ((flags & FLAG_PREPEND_COUNTRY) == 0) {
                writer.print((flags & FLAG_USE_HTML) == FLAG_USE_HTML ? "<br />" : "\n");
            }
        }
    }

    public static void writeOrder(PrintWriter writer, Order order, int flags) {
        if ((flags & FLAG_PREPEND_COUNTRY) == FLAG_PREPEND_COUNTRY) {
            writer.print(order.unit.country.name);
            writer.print(": ");
        }

        switch (order.type) {
            case NO_MOVE_RECEIVED:
                if ((flags & FLAG_USE_HTML) == FLAG_USE_HTML) {
                    writer.print("<i>");
                }
                writeUnit(writer, order.unit, flags);
                writer.print(", no move received");
                if ((flags & FLAG_USE_HTML) == FLAG_USE_HTML) {
                    writer.print("</i>");
                }
                break;
            case HOLD:
                writeUnit(writer, order.unit, flags);
                writer.print(" Hold");
                break;
            case RETREAT:
            case MOVE:
                writeUnit(writer, order.unit, flags);
                writer.print(" - ");
                writeLocation(writer, order.targetLocation, flags);
                break;
            case SUPPORT_MOVE:
                writeUnit(writer, order.unit, flags);
                writer.print(" Supports ");
                writeUnit(writer, order.targetUnit, flags);
                writer.print(" - ");
                writeLocation(writer, order.targetLocation, flags);
                break;
            case SUPPORT_HOLD:
                writeUnit(writer, order.unit, flags);
                writer.print(" Supports ");
                writeUnit(writer, order.targetUnit, flags);
                break;
            case CONVOY:
                writeUnit(writer, order.unit, flags);
                writer.print(" Convoys ");
                writeUnit(writer, order.targetUnit, flags);
                writer.print(" - ");
                writeLocation(writer, order.targetLocation, flags);
                break;
            case BUILD:
                writer.print("Build ");
                writeUnit(writer, order.unit, flags);
                break;
            case REMOVE:
                writer.print("Remove ");
                writeUnit(writer, order.unit, flags);
                break;
            case DEFAULT_REMOVE:
                writer.print("Defaults, removing ");
                writeUnit(writer, order.unit, flags);
                break;
            case DISBAND:
                writer.print("Disband ");
                writeUnit(writer, order.unit, flags);
                break;
            default:
                throw new IllegalArgumentException();
        }

        String resolution = order.getResolution().label;
        if (resolution != null) {
            if ((flags & FLAG_USE_HTML) == FLAG_USE_HTML) {
                writer.print("<b>");
            }
            writer.print(" (*");
            writer.print(resolution);
            writer.print("*)");
            if ((flags & FLAG_USE_HTML) == FLAG_USE_HTML) {
                writer.print("</b>");
            }
        }
    }

    public static void writeUnit(PrintWriter writer, Unit unit, int flags) {
        writer.print((unit.type == Unit.Type.ARMY) ? "A " : "F ");
        writeLocation(writer, unit.location, flags);
    }

    public static void writeLocation(PrintWriter writer, Location location, int flags) {
        writer.print(((flags & FLAG_USE_ABBREVS) == FLAG_USE_ABBREVS)
                ? location.sector.abbrev : location.sector.name);
        switch (location.coast) {
            case ARMY:
            case FLEET:
                break;
            case SOUTH:
                writer.print("(sc)");
                break;
            case NORTH:
                writer.print("(nc)");
                break;
            case WEST:
                writer.print("(wc)");
                break;
            case EAST:
                writer.print("(ec)");
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
}
