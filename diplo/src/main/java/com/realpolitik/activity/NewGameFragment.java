package com.realpolitik.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;

import com.realpolitik.R;
import com.realpolitik.rp.common.GameInfo;

public class NewGameFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ArrayAdapter adapter;
    private CheckBox hotSeatCheckbox;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_game_fragment, container, false);
        hotSeatCheckbox = (CheckBox) view.findViewById(R.id.hot_seat);

        adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.variantNames));
        ListView variantView = (ListView) view.findViewById(R.id.list_view);
        variantView.setAdapter(adapter);
        variantView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), GameActivity.class);
        intent.putExtra(GameActivity.EXTRA_VARIANT_NAME, getInternalVariantName(position));
        intent.putExtra(GameActivity.EXTRA_MODE,
                hotSeatCheckbox.isChecked() ? GameInfo.MODE_HOT_SEAT : GameInfo.MODE_LOCAL);
        startActivity(intent);
    }

    private String getInternalVariantName(int position) {
        String variantName = (String) adapter.getItem(position);
        String[] variants = getResources().getStringArray(R.array.variantNames);
        for (int i = 0; i < variants.length; ++i) {
            if (variants[i].equals(variantName)) {
                return getResources().getStringArray(R.array.internalVariantNames)[i];
            }
        }
        throw new IllegalArgumentException("Variant " + variantName + " does not exist");
    }
}
