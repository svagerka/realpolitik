package com.realpolitik.test;

import com.realpolitik.rp.common.Order;
import com.realpolitik.rp.reader.ReadException;
import com.realpolitik.rp.reader.orders.OrderParser;
import com.realpolitik.rp.season.Season;

import org.junit.Ignore;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(RobolectricGradleTestRunner.class)
@Ignore
abstract public class AdjudicatorTest extends DiploTest {
    protected OrderParser parser;
    protected Season season;

    public AdjudicatorTest() throws ReadException {
    }

    protected void addOrder(String order) {
        season.addOrder(parser.parseOrder(order));
    }

    protected void assertOrderResolution(String abbrev, Order.Resolution resolution) {
        assertEquals(resolution,
                season.getOrder(variant.getSectorByAbbrev(abbrev)).getResolution());
    }

}
