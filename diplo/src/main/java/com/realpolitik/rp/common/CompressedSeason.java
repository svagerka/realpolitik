package com.realpolitik.rp.common;

import com.realpolitik.rp.season.Phase;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.writer.DpyFileWriter;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

public class CompressedSeason {
    public final Phase phase;
    public final int year;
    public final String data;

    public CompressedSeason(Phase phase, int year, String data) {
        this.phase = phase;
        this.year = year;
        this.data = data;
    }

    public CompressedSeason(Season season, Variant variant) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream);
        DpyFileWriter.writePastSeasonInfo(writer, variant, season);
        writer.flush();
        this.data = stream.toString();
        this.phase = season.phase;
        this.year = season.year;
        writer.close();
    }

}
