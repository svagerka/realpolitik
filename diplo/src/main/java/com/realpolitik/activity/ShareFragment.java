package com.realpolitik.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.realpolitik.R;
import com.realpolitik.rp.common.Country;
import com.realpolitik.rp.common.Game;
import com.realpolitik.rp.common.GameInfo;
import com.realpolitik.rp.season.Season;
import com.realpolitik.rp.writer.DpyFileWriter;
import com.realpolitik.rp.writer.OrderWriter;
import com.realpolitik.ui.MapRenderer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ShareFragment extends GameFragment {

    private ViewGroup sharedCountriesParent;
    private CheckBox shareDpyCheckbox;
    private CheckBox shareCurrentPngCheckbox;
    private CheckBox sharePreviousPngCheckbox;
    private CheckBox shareCountryCheckboxes[];
    private Button shareButton;
    private final CompoundButton.OnCheckedChangeListener checkBoxListener
            = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            updateButtonEnabled();
        }
    };
    private String[] seasonsArray;
    private Resources resources;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resources = getResources();
        seasonsArray = resources.getStringArray(R.array.phases);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.share_fragment, container, false);
        sharedCountriesParent = (ViewGroup) view.findViewById(R.id.shared_countries);
        shareButton = (Button) view.findViewById(R.id.button_share);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendShareIntent();
            }
        });

        shareDpyCheckbox = (CheckBox) view.findViewById(R.id.shared_dpy);
        shareDpyCheckbox.setOnCheckedChangeListener(checkBoxListener);

        shareCurrentPngCheckbox = (CheckBox) view.findViewById(R.id.shared_current_png);
        shareCurrentPngCheckbox.setOnCheckedChangeListener(checkBoxListener);

        sharePreviousPngCheckbox = (CheckBox) view.findViewById(R.id.shared_previous_png);
        sharePreviousPngCheckbox.setOnCheckedChangeListener(checkBoxListener);

        view.findViewById(R.id.action_share_countries_all).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        for (CheckBox shareCountryCheckbox : shareCountryCheckboxes) {
                            shareCountryCheckbox.setChecked(true);
                        }
                        updateButtonEnabled();
                    }
                }
        );

        view.findViewById(R.id.action_share_countries_none).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        for (CheckBox shareCountryCheckbox : shareCountryCheckboxes) {
                            shareCountryCheckbox.setChecked(false);
                        }
                        updateButtonEnabled();
                    }
                }
        );

        return view;
    }

    @Override
    protected void performUpdate() {
        if (getGame() != null) {
            if (sharedCountriesParent.getChildCount() == 0) {
                List<Country> countries = getGame().variant.getCountries();
                shareCountryCheckboxes = new CheckBox[countries.size()];
                sharedCountriesParent.removeAllViews();
                for (int i = 0; i < countries.size(); ++i) {
                    CheckBox checkBox = new CheckBox(getActivity());
                    checkBox.setText(countries.get(i).name);
                    checkBox.setChecked(true);
                    checkBox.setOnCheckedChangeListener(checkBoxListener);
                    sharedCountriesParent.addView(checkBox);
                    shareCountryCheckboxes[i] = checkBox;
                }
            }

            // disable sharing in the last season of hot seat, as it would show the orders
            shareButton.setEnabled(getGame().gameInfo.mode != GameInfo.MODE_HOT_SEAT
                    || !getGame().isCurrentSeasonLast());

            updateSeasonCheckBox(sharePreviousPngCheckbox, R.string.share_previous_png_file,
                    getGame().getPreviousSeason());
            updateSeasonCheckBox(shareCurrentPngCheckbox, R.string.share_current_png_file,
                    getGame().getCurrentSeason());
        }
    }

    private void updateSeasonCheckBox(CheckBox checkBox, int resourceId, Season season) {
        if (season == null) {
            checkBox.setVisibility(View.GONE);
        } else {
            checkBox.setVisibility(View.VISIBLE);
            String phase = seasonsArray[season.phase.id - 1];
            String title = resources.getString(resourceId, phase, season.year);
            checkBox.setText(title);
        }
    }

    private void updateButtonEnabled() {
        boolean enabled = false;
        for (CheckBox shareCountryCheckbox : shareCountryCheckboxes) {
            enabled |= shareCountryCheckbox.isChecked();
        }
        enabled |= shareDpyCheckbox.isChecked();
        enabled |= shareCurrentPngCheckbox.isChecked();
        shareButton.setEnabled(enabled);
    }

    private void sendShareIntent() {
        ArrayList<Uri> uris = new ArrayList<>();
        try {
            if (shareDpyCheckbox.isChecked()) {
                uris.add(new DpyExport().generate());
            }
            if (shareCurrentPngCheckbox.isChecked()) {
                uris.add(new CurrentPngExport().generate());
            }
            if (!getGame().isCurrentSeasonFirst() && sharePreviousPngCheckbox.isChecked()) {
                uris.add(new PreviousPngExport().generate());
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        Intent sendIntent = new Intent();
        sendIntent.setAction(uris.size() >= 2 ? Intent.ACTION_SEND_MULTIPLE : Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                serializeOrders() + getString(R.string.shared_by));
        sendIntent.putExtra(Intent.EXTRA_SUBJECT,
                getSubject());

        if (uris.size() >= 2) {
            sendIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else if (uris.size() == 1) {
            sendIntent.putExtra(Intent.EXTRA_STREAM, uris.get(0));
            sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        startActivity(Intent.createChooser(sendIntent, resources.getText(R.string.share_command)));
    }

    private String getSubject() {
        Game game = getGame();
        Season season = game.getCurrentSeason();
        return resources.getString(R.string.share_subject, game.gameInfo.gameName,
                seasonsArray[season.phase.id - 1], season.year);
    }

    private String serializeOrders() {
        boolean[] countriesToShare = new boolean[shareCountryCheckboxes.length];
        boolean anyCountry = false;
        for (int i = 0; i < shareCountryCheckboxes.length; ++i) {
            countriesToShare[i] = shareCountryCheckboxes[i].isChecked();
            anyCountry |= countriesToShare[i];
        }

        if (!anyCountry) {
            return "";
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream);
        OrderWriter.writeOrders(writer, getGame().getCurrentSeason(), getGame().variant,
                countriesToShare, OrderWriter.FLAGS_DEFAULT);
        writer.flush();
        String orders = stream.toString();
        writer.close();
        return orders;
    }

    private abstract class Export {
        private static final String AUTHORITY = "com.android.provider.DataSharing";
        private static final String SHARE_FOLDER = "share";

        abstract protected String getSuffix();

        abstract protected void doGenerate(FileOutputStream fileOutputStream) throws ParseException;

        final Uri generate() throws IOException, ParseException {
            File sharePath = new File(getActivity().getFilesDir(), SHARE_FOLDER);
            sharePath.mkdirs();
            File file = new File(sharePath, getGame().gameInfo.gameName + getSuffix());
            FileOutputStream fos = new FileOutputStream(file);
            doGenerate(fos);
            fos.flush();
            fos.close();
            return FileProvider.getUriForFile(getActivity(), AUTHORITY, file);
        }
    }

    private abstract class PngExport extends Export {
        @Override
        protected void doGenerate(FileOutputStream fileOutputStream) throws ParseException {
            Bitmap map = getGame().variant.colorMap;
            Bitmap.Config conf = Bitmap.Config.ARGB_8888;
            Bitmap output = Bitmap.createBitmap(map.getWidth(), map.getHeight(), conf);
            Canvas canvas = new Canvas(output);
            MapRenderer renderer = MapRenderer.getInstance(getActivity());
            renderer.render(canvas, getGame(), null);
            output.compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream);
        }
    }

    private class CurrentPngExport extends PngExport {
        @Override
        protected String getSuffix() {
            return "-current.png";
        }
    }

    private class PreviousPngExport extends PngExport {
        @Override
        protected String getSuffix() {
            return "-previous.png";
        }

        @Override
        protected void doGenerate(FileOutputStream fileOutputStream) throws ParseException {
            getGame().moveToPrevious();
            super.doGenerate(fileOutputStream);
            getGame().moveToNext();
        }
    }

    private class DpyExport extends Export {
        @Override
        protected String getSuffix() {
            return ".dpy";
        }

        @Override
        protected void doGenerate(FileOutputStream fileOutputStream) {
            PrintWriter printWriter = new PrintWriter(fileOutputStream);
            DpyFileWriter.write(getDatabaseController(), printWriter, getGame());
            printWriter.flush();
        }
    }

}